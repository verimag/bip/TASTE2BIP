This projects tackles the TASTE to BIP model transformation.

It consists of the following folders:
1) sources: contains the source code and executables of the transformation
  - sdlrev. The SDL parser executable should be copied under /home/taste/tool-src/ellidiss-GUI/TASTE-linux/bin 
  - taste2bip.tcl. This launching script should be copied under /home/taste/tool-src/ellidiss-GUI/TASTE-linux/config/externalTools
  - taste2bip.sbp. This binary should be copied under /home/taste/tool-src/ellidiss-GUI/TASTE-linux/config/plugins. The binary can be obtained from the sources following the instructions from the prolog_src folder.
  - prolog_src. Contains the Prolog source code of the transformation.
  - prolog_comp. Contains the sbprolog environment adapted to compile the Prolog source files into binaries.
  
2) examples: contains multiple TASTE examples and the corresponding BIP code.

4) techrep: contains the latex sources of the transformation rules.

5) presentations: contains the latex sources of a presentation describing the transformation and its current status.

Current status of the tool:
- Generation of BIP code from IV completed.
- Implementation of the SDL compiler completed.
- Generation of BIP state machine code from SDL state machines (procedure translation under implementation).

Future steps:
- Translation of ASN.1 data types.
- Structured components implemented as an option.
- Hierarchical IV.
