The toy example of the producer-consumer from the technical report:
- Two functions Prod and Cons 
- Prod communicates with Cons via interface comp of the latter
- Prod sends at each activation (cyclic interface step) an integer value and updates the value (by incrementing it)
- Cons prints the received value (triggered by comp interface)



