-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package cons is
    --  Provided interface "comp"
    procedure comp(val: access asn1SccT_Int32);
    pragma Export(C, comp, "cons_comp");
end cons;