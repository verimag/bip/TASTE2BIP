#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['prod'] = {
    'name_with_case' : 'Prod',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['prod']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'prod',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['prod']['interfaces']['step']['paramsInOrdered'] = []

functions['prod']['interfaces']['step']['paramsOutOrdered'] = []

functions['prod']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'prod',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'cons',
    'calling_threads': {},
    'distant_name': 'comp',
    'queue_size': 1
}

functions['prod']['interfaces']['comp']['paramsInOrdered'] = ['val']

functions['prod']['interfaces']['comp']['paramsOutOrdered'] = []

functions['prod']['interfaces']['comp']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example01/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['cons'] = {
    'name_with_case' : 'Cons',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['cons']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'cons',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 200,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 2
}

functions['cons']['interfaces']['comp']['paramsInOrdered'] = ['val']

functions['cons']['interfaces']['comp']['paramsOutOrdered'] = []

functions['cons']['interfaces']['comp']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example01/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}
