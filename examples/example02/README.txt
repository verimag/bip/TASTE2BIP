The example consists of:
- Two functions E1 et E2
- E1 and E2 communicate via 2 interfaces: P12 of E2 and P21 of E1
- E1 sends to E2 at each activation (cyclic interface step) an integer value (via interface P12) 
- E2 receives the value from E1, updates it by adding 5 and sends its back to E1 (via interface P21)
- E1 receives the result and sends the value to E2 at next activation
