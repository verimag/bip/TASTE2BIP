-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body e1 is
    type States is (l0, l1);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        val : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when l0 =>
                    runTransition(1);
                when l1 =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure P21(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when l0 =>
                    runTransition(CS_Only);
                when l1 =>
                    ctxt.val := val.all;
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end P21;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE L0 (11,18) at 200, 60
                        trId := -1;
                        ctxt.state := L0;
                        goto next_transition;
                    when 1 =>
                        -- writeln('In E1:  value sent to E2 is ',val) (17,17)
                        Put("In E1:  value sent to E2 is ");
                        Put(Asn1Int'Image(ctxt.val));
                        New_Line;
                        -- P12(val) (19,19)
                        RI�P12(ctxt.val'Access);
                        -- NEXT_STATE L1 (21,22) at 310, 283
                        trId := -1;
                        ctxt.state := L1;
                        goto next_transition;
                    when 2 =>
                        -- writeln('In E1: value received  from E2 is ', val) (28,17)
                        Put("In E1: value received  from E2 is ");
                        Put(Asn1Int'Image(ctxt.val));
                        New_Line;
                        -- NEXT_STATE L0 (30,22) at 414, 448
                        trId := -1;
                        ctxt.state := L0;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end e1;