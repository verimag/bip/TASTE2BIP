-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package e2 is
    --  Provided interface "P12"
    procedure P12(val: access asn1SccT_Int32);
    pragma Export(C, P12, "e2_P12");
    --  Required interface "P21"
    procedure RI�P21(val: access asn1SccT_Int32);
    pragma import(C, RI�P21, "e2_RI_P21");
end e2;