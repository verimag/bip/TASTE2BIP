The example (similar to example02) consists of:
- Two functions E1 et E2
- E1 and E2 communicate via 2 interfaces: P12 of E2 and P21 of E1
- E1 sends to E2 at each activation (cyclic interface step) an integer value (via interface P12) and updates its value by incrementing it
- E2 receives from E1 via interface P12 an integer value that it prints, updates by multiplying it by 2 and sends it to E1 via interface P21
- E1 receives from E2 via interface P21 an integer that it prints and goes back to the initial state where wait for the cyclic activation
