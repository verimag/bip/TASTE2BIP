-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package e1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "e1_step");
    --  Provided interface "P21"
    procedure P21(val: access asn1SccT_Int32);
    pragma Export(C, P21, "e1_P21");
    --  Required interface "P12"
    procedure RI�P12(val: access asn1SccT_Int32);
    pragma import(C, RI�P12, "e1_RI_P12");
end e1;