-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body e2 is
    type States is (l0);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        val : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 2;
    procedure runTransition(Id: Integer);
    procedure P12(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when l0 =>
                    ctxt.val := val.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end P12;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE L0 (11,18) at 30, 50
                        trId := -1;
                        ctxt.state := L0;
                        goto next_transition;
                    when 1 =>
                        -- writeln('In E2: received value from E1 is ', val) (17,17)
                        Put("In E2: received value from E1 is ");
                        Put(Asn1Int'Image(ctxt.val));
                        New_Line;
                        -- val := val * 2 (19,17)
                        ctxt.val := Asn1Int((ctxt.val * 2));
                        -- P21(val) (21,19)
                        RI�P21(ctxt.val'Access);
                        -- NEXT_STATE L0 (23,22) at 144, 265
                        trId := -1;
                        ctxt.state := L0;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end e2;