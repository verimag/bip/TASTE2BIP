The example consists of:
- Two functions E1 et E2
- E1 and E2 communicate through 3 interfaces: P121 and P122 of E2 and P21 of E1
- E1 sends to E2 at each activation (cyclic interface step) an integer value either via interface P121 if the value is less or equal that 18 or via interface P122 if the value is greater than 18
- E2 receives from E1 the integer value which it updates and send it back to E1 via interface P21: if the value comes via interface P121 it updates it by adding 5, if the value comes via interface P122 it updates it by decrementing 1
- E1 prints the value received through P21 and goes back to the initial state waiting for the cyclic activation
