-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package e1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "e1_step");
    --  Provided interface "P21"
    procedure P21(val: access asn1SccT_Int32);
    pragma Export(C, P21, "e1_P21");
    --  Required interface "P121"
    procedure RI�P121(val: access asn1SccT_Int32);
    pragma import(C, RI�P121, "e1_RI_P121");
    --  Required interface "P122"
    procedure RI�P122(val: access asn1SccT_Int32);
    pragma import(C, RI�P122, "e1_RI_P122");
end e1;