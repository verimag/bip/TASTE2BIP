#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['e1'] = {
    'name_with_case' : 'E1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['e1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'e1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['e1']['interfaces']['step']['paramsInOrdered'] = []

functions['e1']['interfaces']['step']['paramsOutOrdered'] = []

functions['e1']['interfaces']['P21'] = {
    'port_name': 'P21',
    'parent_fv': 'e1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 200,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['e1']['interfaces']['P21']['paramsInOrdered'] = ['val']

functions['e1']['interfaces']['P21']['paramsOutOrdered'] = []

functions['e1']['interfaces']['P21']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example04/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P21',
    'param_direction': param_in
}

functions['e1']['interfaces']['P121'] = {
    'port_name': 'P121',
    'parent_fv': 'e1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'e2',
    'calling_threads': {},
    'distant_name': 'P121',
    'queue_size': 1
}

functions['e1']['interfaces']['P121']['paramsInOrdered'] = ['val']

functions['e1']['interfaces']['P121']['paramsOutOrdered'] = []

functions['e1']['interfaces']['P121']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example04/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P121',
    'param_direction': param_in
}

functions['e1']['interfaces']['P122'] = {
    'port_name': 'P122',
    'parent_fv': 'e1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'e2',
    'calling_threads': {},
    'distant_name': 'P122',
    'queue_size': 1
}

functions['e1']['interfaces']['P122']['paramsInOrdered'] = ['val']

functions['e1']['interfaces']['P122']['paramsOutOrdered'] = []

functions['e1']['interfaces']['P122']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example04/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P122',
    'param_direction': param_in
}

functions['e2'] = {
    'name_with_case' : 'E2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['e2']['interfaces']['P121'] = {
    'port_name': 'P121',
    'parent_fv': 'e2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 200,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['e2']['interfaces']['P121']['paramsInOrdered'] = ['val']

functions['e2']['interfaces']['P121']['paramsOutOrdered'] = []

functions['e2']['interfaces']['P121']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example04/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P121',
    'param_direction': param_in
}

functions['e2']['interfaces']['P122'] = {
    'port_name': 'P122',
    'parent_fv': 'e2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 200,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['e2']['interfaces']['P122']['paramsInOrdered'] = ['val']

functions['e2']['interfaces']['P122']['paramsOutOrdered'] = []

functions['e2']['interfaces']['P122']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example04/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P122',
    'param_direction': param_in
}

functions['e2']['interfaces']['P21'] = {
    'port_name': 'P21',
    'parent_fv': 'e2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'e1',
    'calling_threads': {},
    'distant_name': 'P21',
    'queue_size': 1
}

functions['e2']['interfaces']['P21']['paramsInOrdered'] = ['val']

functions['e2']['interfaces']['P21']['paramsOutOrdered'] = []

functions['e2']['interfaces']['P21']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example04/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P21',
    'param_direction': param_in
}
