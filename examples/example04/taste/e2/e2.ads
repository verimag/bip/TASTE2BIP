-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package e2 is
    --  Provided interface "P121"
    procedure P121(val: access asn1SccT_Int32);
    pragma Export(C, P121, "e2_P121");
    --  Provided interface "P122"
    procedure P122(val: access asn1SccT_Int32);
    pragma Export(C, P122, "e2_P122");
    --  Required interface "P21"
    procedure RI�P21(val: access asn1SccT_Int32);
    pragma import(C, RI�P21, "e2_RI_P21");
end e2;