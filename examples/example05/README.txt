The example consists of:
- Three functions P, C1, et C2
- P communicates with C1 and C2 via interface send, denoted send and send1 respectively as required interfaces
- P is activated by cyclic interface step) and sets a Timer t to 1sec
- When the Timer expires P sends an integer value to C1 or C2 via interface send or send1; the values are sent at each activation to one of Ci starting with C1, their choice being modeled by a boolean value; the value is updated for next step by incrementing it and the Timer is reset to 1sec
- C1 receives basically every odd multiple of seconds an integer value via interface send and prints it
- C2 receives basically every even mutiple of seconds an integer value via interface send and prints it

