-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package c1 is
    --  Provided interface "send"
    procedure send(val: access asn1SccT_Int32);
    pragma Export(C, send, "c1_send");
end c1;