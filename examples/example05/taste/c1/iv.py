#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['c1'] = {
    'name_with_case' : 'C1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c1']['interfaces']['send'] = {
    'port_name': 'send',
    'parent_fv': 'c1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 10,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['c1']['interfaces']['send']['paramsInOrdered'] = ['val']

functions['c1']['interfaces']['send']['paramsOutOrdered'] = []

functions['c1']['interfaces']['send']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example05/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'send',
    'param_direction': param_in
}

functions['c2'] = {
    'name_with_case' : 'C2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c2']['interfaces']['send'] = {
    'port_name': 'send',
    'parent_fv': 'c2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 10,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['c2']['interfaces']['send']['paramsInOrdered'] = ['val']

functions['c2']['interfaces']['send']['paramsOutOrdered'] = []

functions['c2']['interfaces']['send']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example05/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'send',
    'param_direction': param_in
}

functions['p'] = {
    'name_with_case' : 'P',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['p']['functional_states']['t'] = {
    'fullFsName' : 't',
    'typeName' : 'Timer',
    'moduleName' : 'TASTE-Directives',
    'asn1FileName' : 'dummy'
}

functions['p']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'p',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1000,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['p']['interfaces']['step']['paramsInOrdered'] = []

functions['p']['interfaces']['step']['paramsOutOrdered'] = []

functions['p']['interfaces']['send'] = {
    'port_name': 'send',
    'parent_fv': 'p',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c1',
    'calling_threads': {},
    'distant_name': 'send',
    'queue_size': 1
}

functions['p']['interfaces']['send']['paramsInOrdered'] = ['val']

functions['p']['interfaces']['send']['paramsOutOrdered'] = []

functions['p']['interfaces']['send']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example05/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'send',
    'param_direction': param_in
}

functions['p']['interfaces']['send1'] = {
    'port_name': 'send1',
    'parent_fv': 'p',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c2',
    'calling_threads': {},
    'distant_name': 'send',
    'queue_size': 1
}

functions['p']['interfaces']['send1']['paramsInOrdered'] = ['val']

functions['p']['interfaces']['send1']['paramsOutOrdered'] = []

functions['p']['interfaces']['send1']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example05/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'send1',
    'param_direction': param_in
}
