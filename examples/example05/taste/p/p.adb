-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body p is
    type States is (check, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        b : aliased asn1SccT_Boolean := true;
        val : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when check =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure t is
        begin
            case ctxt.state is
                when check =>
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end t;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp20 : aliased asn1SccT_UInt32;
        tmp38 : aliased asn1SccT_UInt32;
        tmp42 : aliased asn1SccT_UInt32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (13,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- DECISION b (-1,-1)
                        -- ANSWER true (21,17)
                        if (ctxt.b) = true then
                            -- send(val) (23,27)
                            RI�send(ctxt.val'Access);
                            -- writeln('P sent C1 value ', val) (25,25)
                            Put("P sent C1 value ");
                            Put(Asn1Int'Image(ctxt.val));
                            New_Line;
                            -- val:=val+1 (27,25)
                            ctxt.val := Asn1Int((ctxt.val + 1));
                            -- b:= not b (29,25)
                            ctxt.b := (not ctxt.b);
                            -- SET_TIMER(1000,t) (31,25)
                            tmp20 := 1000;
                            SET_t(tmp20'access);
                            -- NEXT_STATE Check (33,30) at 436, 660
                            trId := -1;
                            ctxt.state := Check;
                            goto next_transition;
                            -- ANSWER false (35,17)
                        elsif (ctxt.b) = false then
                            -- send1(val) (37,27)
                            RI�send1(ctxt.val'Access);
                            -- writeln('P sent C2 value ', val) (39,25)
                            Put("P sent C2 value ");
                            Put(Asn1Int'Image(ctxt.val));
                            New_Line;
                            -- val:=val+1 (41,25)
                            ctxt.val := Asn1Int((ctxt.val + 1));
                            -- b := not b (43,25)
                            ctxt.b := (not ctxt.b);
                            -- SET_TIMER(1000,t) (45,25)
                            tmp38 := 1000;
                            SET_t(tmp38'access);
                            -- NEXT_STATE Check (47,30) at 672, 660
                            trId := -1;
                            ctxt.state := Check;
                            goto next_transition;
                        end if;
                    when 2 =>
                        -- SET_TIMER(1000,t) (55,17)
                        tmp42 := 1000;
                        SET_t(tmp42'access);
                        -- NEXT_STATE Check (57,22) at 557, 171
                        trId := -1;
                        ctxt.state := Check;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end p;