-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package p is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "p_step");
    --  Provided interface "t"
    procedure t;
    pragma Export(C, t, "p_t");
    --  Required interface "send"
    procedure RI�send(val: access asn1SccT_Int32);
    pragma import(C, RI�send, "p_RI_send");
    --  Required interface "send1"
    procedure RI�send1(val: access asn1SccT_Int32);
    pragma import(C, RI�send1, "p_RI_send1");
    --  Timer t SET and RESET functions
    procedure SET_t(val: access asn1SccT_UInt32);
    pragma import(C, SET_t, "p_RI_set_t");
    procedure RESET_t;
    pragma import(C, RESET_t, "p_RI_reset_t");
end p;