This example consists of:
- Three functions A, B and C
- A and B communicate via the interfaces s of B and r1 of A
- A and C communicate via the interfaces s of C and r2 of A
- A sends at each activation (cyclic interface) a request s1 or s2 to B or C respectively; it starts by sending s1 to B via interface s, then at next step s2 to C via interface s; this sequence is modelled with a boolean value
- B receives a request s (without parameter) from A and replies with a request r (without parameter) via interface r1
- C receives a request s (without parameter) from A and replies with a request r (without parameter) via interface r2
- After A sends s1 wait in a state for reply r1 and prints a message upon reception; if r2 is received an error message is printed and goes into an error state where no behavior is modelled
- The same behavior is modelled for A and the pair s2 - r2
