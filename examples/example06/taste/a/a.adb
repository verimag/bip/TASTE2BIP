-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body a is
    type States is (error, waitr2, waitr1, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        tc : aliased asn1SccT_Boolean := false;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 6;
    procedure runTransition(Id: Integer);
    procedure r1 is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(CS_Only);
                when waitr2 =>
                    runTransition(2);
                when waitr1 =>
                    runTransition(4);
                when error =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end r1;
        

    procedure r2 is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(CS_Only);
                when waitr2 =>
                    runTransition(3);
                when waitr1 =>
                    runTransition(5);
                when error =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end r2;
        

    procedure step is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(1);
                when waitr2 =>
                    runTransition(CS_Only);
                when waitr1 =>
                    runTransition(CS_Only);
                when error =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (11,18) at 14, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- DECISION tc (-1,-1)
                        -- ANSWER false (19,17)
                        if (ctxt.tc) = false then
                            -- s1 (21,27)
                            RI�s1;
                            -- writeln('Message sent: s1') (23,25)
                            Put("Message sent: s1");
                            New_Line;
                            -- tc := not tc (25,25)
                            ctxt.tc := (not ctxt.tc);
                            -- NEXT_STATE Waitr1 (27,30) at 155, 399
                            trId := -1;
                            ctxt.state := Waitr1;
                            goto next_transition;
                            -- ANSWER true (29,17)
                        elsif (ctxt.tc) = true then
                            -- s2 (31,27)
                            RI�s2;
                            -- writeln('Message sent: s2') (33,25)
                            Put("Message sent: s2");
                            New_Line;
                            -- tc := not tc (35,25)
                            ctxt.tc := (not ctxt.tc);
                            -- NEXT_STATE Waitr2 (37,30) at 355, 393
                            trId := -1;
                            ctxt.state := Waitr2;
                            goto next_transition;
                        end if;
                    when 2 =>
                        -- writeln('Error! Message received: r1') (48,17)
                        Put("Error! Message received: r1");
                        New_Line;
                        -- NEXT_STATE Error (50,22) at 643, 426
                        trId := -1;
                        ctxt.state := Error;
                        goto next_transition;
                    when 3 =>
                        -- writeln('Message received: r2') (54,17)
                        Put("Message received: r2");
                        New_Line;
                        -- NEXT_STATE Wait (56,22) at 895, 422
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 4 =>
                        -- writeln('Message received: r1') (63,17)
                        Put("Message received: r1");
                        New_Line;
                        -- NEXT_STATE Wait (65,22) at 638, 176
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 5 =>
                        -- writeln('Error! Message received: r2') (69,17)
                        Put("Error! Message received: r2");
                        New_Line;
                        -- NEXT_STATE Error (71,22) at 897, 180
                        trId := -1;
                        ctxt.state := Error;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end a;