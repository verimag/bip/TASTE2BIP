-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package a is
    --  Provided interface "r1"
    procedure r1;
    pragma Export(C, r1, "a_r1");
    --  Provided interface "r2"
    procedure r2;
    pragma Export(C, r2, "a_r2");
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "a_step");
    --  Paramless required interface "s1"
    procedure RI�s1;
    pragma import(C, RI�s1, "a_RI_s1");
    --  Paramless required interface "s2"
    procedure RI�s2;
    pragma import(C, RI�s2, "a_RI_s2");
end a;