#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['a'] = {
    'name_with_case' : 'A',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['a']['interfaces']['r1'] = {
    'port_name': 'r1',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 500,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['r1']['paramsInOrdered'] = []

functions['a']['interfaces']['r1']['paramsOutOrdered'] = []

functions['a']['interfaces']['r2'] = {
    'port_name': 'r2',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 500,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['r2']['paramsInOrdered'] = []

functions['a']['interfaces']['r2']['paramsOutOrdered'] = []

functions['a']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['step']['paramsInOrdered'] = []

functions['a']['interfaces']['step']['paramsOutOrdered'] = []

functions['a']['interfaces']['s1'] = {
    'port_name': 's1',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 's',
    'queue_size': 1
}

functions['a']['interfaces']['s1']['paramsInOrdered'] = []

functions['a']['interfaces']['s1']['paramsOutOrdered'] = []

functions['a']['interfaces']['s2'] = {
    'port_name': 's2',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c',
    'calling_threads': {},
    'distant_name': 's',
    'queue_size': 1
}

functions['a']['interfaces']['s2']['paramsInOrdered'] = []

functions['a']['interfaces']['s2']['paramsOutOrdered'] = []

functions['b'] = {
    'name_with_case' : 'B',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['b']['interfaces']['s'] = {
    'port_name': 's',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['s']['paramsInOrdered'] = []

functions['b']['interfaces']['s']['paramsOutOrdered'] = []

functions['b']['interfaces']['r'] = {
    'port_name': 'r',
    'parent_fv': 'b',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'a',
    'calling_threads': {},
    'distant_name': 'r1',
    'queue_size': 1
}

functions['b']['interfaces']['r']['paramsInOrdered'] = []

functions['b']['interfaces']['r']['paramsOutOrdered'] = []

functions['c'] = {
    'name_with_case' : 'C',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c']['interfaces']['s'] = {
    'port_name': 's',
    'parent_fv': 'c',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['c']['interfaces']['s']['paramsInOrdered'] = []

functions['c']['interfaces']['s']['paramsOutOrdered'] = []

functions['c']['interfaces']['r'] = {
    'port_name': 'r',
    'parent_fv': 'c',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'a',
    'calling_threads': {},
    'distant_name': 'r2',
    'queue_size': 1
}

functions['c']['interfaces']['r']['paramsInOrdered'] = []

functions['c']['interfaces']['r']['paramsOutOrdered'] = []
