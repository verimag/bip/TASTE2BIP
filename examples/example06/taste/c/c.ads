-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package c is
    --  Provided interface "s"
    procedure s;
    pragma Export(C, s, "c_s");
    --  Paramless required interface "r"
    procedure RI�r;
    pragma import(C, RI�r, "c_RI_r");
end c;