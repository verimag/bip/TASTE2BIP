The example consists of:
- Two functions A and B
- A communicates with B via interfaces P1 and P2 of B
- A sends at each activation (cyclic interface step) either a signal P1 without parameter or a signal P2 with a parameter of type bool; the modeling of whether to request P1 or P2 is done with a boolean value which is the one sent to B; for each request to P1 or P2 a message is printed
- B activates periodically (cicly interface step) and prints a message; no behavior is modelled for the provided interfaces P1 and P2
- The MIAT of P2 (of B) is of 5000ms while the period of step of A is of 2000ms. The queue length of P1 is of 1, which makes the queue full and an a queue full message is printed.
