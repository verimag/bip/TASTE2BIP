#include "Example07.hpp"

// Methods for (queue of) Bool

int get_size(const queueBool q){
  return q.size();
}

void push_back(queueBool &q, bool &val, const int &size){
  if (get_size(q) < size) {
    q.push(val);
    cout<<"Queuing: "<<val<<endl;
  }
  else {
    cout<<"Queue full!"<<endl;
  }
}

bool pop_front(queueBool &q){
  bool val;

  if (get_size(q) > 0) {
    val = q.front();
    q.pop();
    cout<<"Popping: "<<val<<endl;
  }
  else {
    val = false;
    cout<<"Queue empty!"<<endl;
  }

  return val;
}

ostream &operator<<(ostream &os, queueBool q){
  queueBool tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}

void write(const bool i){
  cout<<i;
}

void writeln(const bool i){
  cout<<i<<endl;
}


// Methods for (queue of) Int

int get_size(const queueInt q){
  return q.size();
}

void push_back(queueInt &q, int &val, const int &size){
  if (get_size(q) < size) {
    q.push(val);
    cout<<"Queuing: "<<val<<endl;
  }
  else {
    cout<<"Queue full!"<<endl;
  }
}

int pop_front(queueInt &q){
  int val;

  if (get_size(q) > 0) {
    val = q.front();
    q.pop();
    cout<<"Popping: "<<val<<endl;
  }
  else {
    val = 0;
    cout<<"Queue empty!"<<endl;
  }

  return val;
}

ostream &operator<<(ostream &os, queueInt q){
  queueInt tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}

void write(const int i){
  cout<<i;
}

void writeln(const int i){
  cout<<i<<endl;
}


// Methods for (queue of) Float

int get_size(const queueFloat q){
  return q.size();
}

void push_back(queueFloat &q, double &val, const int &size){
  if (get_size(q) < size) {
    q.push(val);
    cout<<"Queuing: "<<val<<endl;
  }
  else {
    cout<<"Queue full!"<<endl;
  }
}

double pop_front(queueFloat &q){
  double val;

  if (get_size(q) > 0) {
    val = q.front();
    q.pop();
    cout<<"Popping: "<<val<<endl;
  }
  else {
    val = 0;
    cout<<"Queue empty!"<<endl;
  }

  return val;
}

ostream &operator<<(ostream &os, queueFloat q){
  queueFloat tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}

void write(const double i){
  cout<<i;
}

void writeln(const double i){
  cout<<i<<endl;
}


// Methods for (queue of) String

int get_size(const queueString q){
  return q.size();
}

void push_back(queueString &q, string &val, const int &size){
  if (get_size(q) < size) {
    q.push(val);
    cout<<"Queuing: "<<val<<endl;
  }
  else {
    cout<<"Queue full!"<<endl;
  }
}

string pop_front(queueString &q){
  string val;

  if (get_size(q) > 0) {
    val = q.front();
    q.pop();
    cout<<"Popping: "<<val<<endl;
  }
  else {
    val = "";
    cout<<"Queue empty!"<<endl;
  }

  return val;
}

ostream &operator<<(ostream &os, queueString q){
  queueString tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}

void write(const string i){
  cout<<i;
}

void writeln(const string i){
  cout<<i<<endl;
}

void write(const char* s){
  cout<<s;
}

void writeln(const char* s){
  cout<<s<<endl;
}


