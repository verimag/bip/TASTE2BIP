-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body a is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v : aliased asn1SccT_Boolean := false;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 2;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (11,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('Activation of A (periodic)') (17,17)
                        Put("Activation of A (periodic)");
                        New_Line;
                        -- DECISION v (-1,-1)
                        -- ANSWER false (21,17)
                        if (ctxt.v) = false then
                            -- p1 (23,27)
                            RI�p1;
                            -- writeln('A sent B: p1') (25,25)
                            Put("A sent B: p1");
                            New_Line;
                            -- v := not v (27,25)
                            ctxt.v := (not ctxt.v);
                            -- ANSWER true (29,17)
                        elsif (ctxt.v) = true then
                            -- p2(v) (31,27)
                            RI�p2(ctxt.v'Access);
                            -- writeln('A sent B: p2') (33,25)
                            Put("A sent B: p2");
                            New_Line;
                            -- v := not v (35,25)
                            ctxt.v := (not ctxt.v);
                        end if;
                        -- NEXT_STATE Wait (38,22) at 450, 446
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end a;