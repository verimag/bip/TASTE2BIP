-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package a is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "a_step");
    --  Paramless required interface "P1"
    procedure RI�P1;
    pragma import(C, RI�P1, "a_RI_P1");
    --  Required interface "P2"
    procedure RI�P2(val: access asn1SccT_Boolean);
    pragma import(C, RI�P2, "a_RI_P2");
end a;