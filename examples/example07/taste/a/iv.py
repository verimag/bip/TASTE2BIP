#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['a'] = {
    'name_with_case' : 'A',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['a']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['step']['paramsInOrdered'] = []

functions['a']['interfaces']['step']['paramsOutOrdered'] = []

functions['a']['interfaces']['P1'] = {
    'port_name': 'P1',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'P1',
    'queue_size': 1
}

functions['a']['interfaces']['P1']['paramsInOrdered'] = []

functions['a']['interfaces']['P1']['paramsOutOrdered'] = []

functions['a']['interfaces']['P2'] = {
    'port_name': 'P2',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'P2',
    'queue_size': 1
}

functions['a']['interfaces']['P2']['paramsInOrdered'] = ['val']

functions['a']['interfaces']['P2']['paramsOutOrdered'] = []

functions['a']['interfaces']['P2']['in']['val'] = {
    'type': 'T_Boolean',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': boolean,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example07/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P2',
    'param_direction': param_in
}

functions['b'] = {
    'name_with_case' : 'B',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['b']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 3000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['step']['paramsInOrdered'] = []

functions['b']['interfaces']['step']['paramsOutOrdered'] = []

functions['b']['interfaces']['P1'] = {
    'port_name': 'P1',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['P1']['paramsInOrdered'] = []

functions['b']['interfaces']['P1']['paramsOutOrdered'] = []

functions['b']['interfaces']['P2'] = {
    'port_name': 'P2',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 5000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['P2']['paramsInOrdered'] = ['val']

functions['b']['interfaces']['P2']['paramsOutOrdered'] = []

functions['b']['interfaces']['P2']['in']['val'] = {
    'type': 'T_Boolean',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': boolean,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example07/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'P2',
    'param_direction': param_in
}
