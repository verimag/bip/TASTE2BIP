-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package b is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "b_step");
    --  Provided interface "P1"
    procedure P1;
    pragma Export(C, P1, "b_P1");
    --  Provided interface "P2"
    procedure P2(val: access asn1SccT_Boolean);
    pragma Export(C, P2, "b_P2");
end b;