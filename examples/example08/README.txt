The example consists of:
- Three functions X, SQ, and Y
- X communicates with SQ via interface comp of SQ 
- SQ communicates with Y via interface print_res of Y
- X send at each activation (cyclic interface step) to SQ via interface comp an integer value; the function defines a state variable v initialized to 1 that is updates by incrementing with 1 at each activation; the value sent is the state variable value divided by 2; once the request is sent a message is printed
- SQ receives a computation request from X via interface comp with an integer value; then a request is sent to Y via interface print_res with an integer value representing the square of the received value
- Y receives a print request from SQ via interface print_res with an integer value and it prints a message with the value

