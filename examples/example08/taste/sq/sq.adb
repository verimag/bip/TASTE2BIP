-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body sq is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v : aliased asn1SccT_Int32;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 2;
    procedure runTransition(Id: Integer);
    procedure comp(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.v := val.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp3 : aliased asn1SccT_Int32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (11,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- print_res(v*v) (17,19)
                        tmp3 := Asn1Int((ctxt.v * ctxt.v));
                        RI�print_res(tmp3'Access);
                        -- writeln('Sq sent for printing value ',v*v) (19,17)
                        Put("Sq sent for printing value ");
                        Put(Asn1Int'Image((ctxt.v * ctxt.v)));
                        New_Line;
                        -- NEXT_STATE Wait (21,22) at 450, 230
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end sq;