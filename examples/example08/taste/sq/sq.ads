-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package sq is
    --  Provided interface "comp"
    procedure comp(val: access asn1SccT_Int32);
    pragma Export(C, comp, "sq_comp");
    --  Required interface "print_res"
    procedure RI�print_res(val: access asn1SccT_Int32);
    pragma import(C, RI�print_res, "sq_RI_print_res");
end sq;