-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package x is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "x_step");
    --  Required interface "comp"
    procedure RI�comp(val: access asn1SccT_Int32);
    pragma import(C, RI�comp, "x_RI_comp");
end x;