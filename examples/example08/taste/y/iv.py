#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['x'] = {
    'name_with_case' : 'X',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['x']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'x',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['x']['interfaces']['step']['paramsInOrdered'] = []

functions['x']['interfaces']['step']['paramsOutOrdered'] = []

functions['x']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'x',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'sq',
    'calling_threads': {},
    'distant_name': 'comp',
    'queue_size': 1
}

functions['x']['interfaces']['comp']['paramsInOrdered'] = ['val']

functions['x']['interfaces']['comp']['paramsOutOrdered'] = []

functions['x']['interfaces']['comp']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example08/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['sq'] = {
    'name_with_case' : 'SQ',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['sq']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'sq',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['sq']['interfaces']['comp']['paramsInOrdered'] = ['val']

functions['sq']['interfaces']['comp']['paramsOutOrdered'] = []

functions['sq']['interfaces']['comp']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example08/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['sq']['interfaces']['print_res'] = {
    'port_name': 'print_res',
    'parent_fv': 'sq',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'y',
    'calling_threads': {},
    'distant_name': 'print_res',
    'queue_size': 1
}

functions['sq']['interfaces']['print_res']['paramsInOrdered'] = ['val']

functions['sq']['interfaces']['print_res']['paramsOutOrdered'] = []

functions['sq']['interfaces']['print_res']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example08/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print_res',
    'param_direction': param_in
}

functions['y'] = {
    'name_with_case' : 'Y',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['y']['interfaces']['print_res'] = {
    'port_name': 'print_res',
    'parent_fv': 'y',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['y']['interfaces']['print_res']['paramsInOrdered'] = ['val']

functions['y']['interfaces']['print_res']['paramsOutOrdered'] = []

functions['y']['interfaces']['print_res']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example08/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print_res',
    'param_direction': param_in
}
