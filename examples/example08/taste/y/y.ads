-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package y is
    --  Provided interface "print_res"
    procedure print_res(val: access asn1SccT_Int32);
    pragma Export(C, print_res, "y_print_res");
end y;