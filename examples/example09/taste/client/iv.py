#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['client'] = {
    'name_with_case' : 'Client',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['client']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'client',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['client']['interfaces']['step']['paramsInOrdered'] = []

functions['client']['interfaces']['step']['paramsOutOrdered'] = []

functions['client']['interfaces']['reply'] = {
    'port_name': 'reply',
    'parent_fv': 'client',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['client']['interfaces']['reply']['paramsInOrdered'] = []

functions['client']['interfaces']['reply']['paramsOutOrdered'] = []

functions['client']['interfaces']['req'] = {
    'port_name': 'req',
    'parent_fv': 'client',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'srv',
    'calling_threads': {},
    'distant_name': 'req',
    'queue_size': 1
}

functions['client']['interfaces']['req']['paramsInOrdered'] = []

functions['client']['interfaces']['req']['paramsOutOrdered'] = []

functions['srv'] = {
    'name_with_case' : 'Srv',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['srv']['functional_states']['t'] = {
    'fullFsName' : 't',
    'typeName' : 'Timer',
    'moduleName' : 'TASTE-Directives',
    'asn1FileName' : 'dummy'
}

functions['srv']['interfaces']['req'] = {
    'port_name': 'req',
    'parent_fv': 'srv',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['srv']['interfaces']['req']['paramsInOrdered'] = []

functions['srv']['interfaces']['req']['paramsOutOrdered'] = []

functions['srv']['interfaces']['reply'] = {
    'port_name': 'reply',
    'parent_fv': 'srv',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'client',
    'calling_threads': {},
    'distant_name': 'reply',
    'queue_size': 1
}

functions['srv']['interfaces']['reply']['paramsInOrdered'] = []

functions['srv']['interfaces']['reply']['paramsOutOrdered'] = []
