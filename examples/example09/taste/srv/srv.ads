-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package srv is
    --  Provided interface "req"
    procedure req;
    pragma Export(C, req, "srv_req");
    --  Provided interface "t"
    procedure t;
    pragma Export(C, t, "srv_t");
    --  Paramless required interface "reply"
    procedure RI�reply;
    pragma import(C, RI�reply, "srv_RI_reply");
    --  Timer t SET and RESET functions
    procedure SET_t(val: access asn1SccT_UInt32);
    pragma import(C, SET_t, "srv_RI_set_t");
    procedure RESET_t;
    pragma import(C, RESET_t, "srv_RI_reset_t");
end srv;