This example consists of:
- Three functions P1, P2 and C1
- P1 communicates with C1 via interface comp1 of the latter, and P2 communicates with C2 via interface comp2 of the latter
- P1 sends C1 via interface comp1 at each activation step (cyclic interface step) an integer value, prints a message and updates the value by incrementing it
- P2 sends C1 via interface comp2 at each activation step (cyclic interface step) an integer value, prints a message and updated the value by adding 2
- C1 receives via comp1 an integer value which it prints and increments an counter
- C2 receives via comp2 an integer valuw which it prints whenever the counter is even and restarts the counter, otherwise the request is ignored

