-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body c1 is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        val : aliased asn1SccT_Int32;
        choice : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure comp1(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.val := val.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp1;
        

    procedure comp2(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.val := val.all;
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp2;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 229, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('      P1 ? ', val) (18,17)
                        Put("      P1 ? ");
                        Put(Asn1Int'Image(ctxt.val));
                        New_Line;
                        -- choice := choice + 1 (20,17)
                        ctxt.choice := Asn1Int((ctxt.choice + 1));
                        -- NEXT_STATE Wait (22,22) at 348, 230
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- DECISION (choice mod 2) = 0 (26,36)
                        -- ANSWER true (28,17)
                        if (((ctxt.choice mod 2) = 0)) = true then
                            -- writeln('      P2 ? ', val) (30,25)
                            Put("      P2 ? ");
                            Put(Asn1Int'Image(ctxt.val));
                            New_Line;
                            -- choice := 0 (32,25)
                            ctxt.choice := Asn1Int(0);
                            -- NEXT_STATE Wait (34,30) at 530, 346
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                            -- ANSWER false (36,17)
                        elsif (((ctxt.choice mod 2) = 0)) = false then
                            -- NEXT_STATE Wait (38,30) at 723, 235
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                        end if;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end c1;