-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package c1 is
    --  Provided interface "comp1"
    procedure comp1(val: access asn1SccT_Int32);
    pragma Export(C, comp1, "c1_comp1");
    --  Provided interface "comp2"
    procedure comp2(val: access asn1SccT_Int32);
    pragma Export(C, comp2, "c1_comp2");
end c1;