-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package p2 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "p2_step");
    --  Required interface "comp2"
    procedure RI�comp2(val: access asn1SccT_Int32);
    pragma import(C, RI�comp2, "p2_RI_comp2");
end p2;