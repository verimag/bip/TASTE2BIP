  This example consists of:
- Two functions Client and Srv
- The two functions communicate via the interface req of Srv and reply of Client
- Client sends a request req at each activation (cyclic interface step) to Srv via interface req and prints a message
- Srv receives a request from Client via interface req, sets q timer of 1500 milliseconds and upon expiration of the timer sends a reply request to Client and prints a message
- Client receives the answer reply and prints a message, and restarts its behavior with respect to the cyclic activation 

