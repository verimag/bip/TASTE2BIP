-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package client is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "client_step");
    --  Provided interface "reply"
    procedure reply;
    pragma Export(C, reply, "client_reply");
    --  Paramless required interface "req"
    procedure RI�req;
    pragma import(C, RI�req, "client_RI_req");
end client;