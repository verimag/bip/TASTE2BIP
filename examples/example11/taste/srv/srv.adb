-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body srv is
    type States is (idle, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure req is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end req;
        

    procedure t is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end t;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp6 : aliased asn1SccT_UInt32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (11,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- reply (17,19)
                        RI�reply;
                        -- writeln('Reply sent!') (19,17)
                        Put("Reply sent!");
                        New_Line;
                        -- NEXT_STATE Wait (21,22) at 450, 440
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('Request received!') (28,17)
                        Put("Request received!");
                        New_Line;
                        -- set_timer(1500,t) (30,17)
                        tmp6 := 1500;
                        SET_t(tmp6'access);
                        -- NEXT_STATE Idle (32,22) at 450, 225
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end srv;