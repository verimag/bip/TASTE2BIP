This example consists of:
- Four functions P, C1, C2, and C3
- P communicates with C1 via the interface req of the latter (call req1 from P)
- P communicates with C2 via the interface req of the latter (call req2 from P)
- P communicates with C3 via the interface req of the latter (call req3 from P)
- P sends a request reqi at almost each activation (cyclic interface req) as follows: the request to be send in modeled by an integer variable; if the variable modulo 4 is 0 req1 is sent, if it is 1 req2 is sent, if it is 2 req3 is sent, otherwise the variable is reinitialized and P waits next activation; if req1 is sent a timer is set to 1000 milliseconds and P enters a specific state Idle1; for req2 the timer is set to 2000 milliseconds and P goes to Idle2; for req3 the timer is set to 3000 milliseconds and P goes to Idle3; in each state Idlei, P waits for the time elapse and then v is reset accordingly - addition by 2 for req1 and req2 and subtraction by 1 for req3 - and goes back to the initial state waiting for next activation
- Ci (i=1,2,3) prints a message upon the reception of req


