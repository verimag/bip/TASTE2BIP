-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package c1 is
    --  Provided interface "req"
    procedure req;
    pragma Export(C, req, "c1_req");
end c1;