-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body p is
    type States is (idle3, idle2, idle1, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v : aliased asn1SccT_UInt8 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 5;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when idle3 =>
                    runTransition(CS_Only);
                when idle2 =>
                    runTransition(CS_Only);
                when idle1 =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(4);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure t is
        begin
            case ctxt.state is
                when idle3 =>
                    runTransition(1);
                when idle2 =>
                    runTransition(2);
                when idle1 =>
                    runTransition(3);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end t;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp37 : aliased asn1SccT_UInt32;
        tmp26 : aliased asn1SccT_UInt32;
        tmp48 : aliased asn1SccT_UInt32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- v := v - 1 (18,17)
                        ctxt.v := Asn1Int((ctxt.v - 1));
                        -- NEXT_STATE Wait (20,22) at 647, 563
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- v := v + 2 (27,17)
                        ctxt.v := Asn1Int((ctxt.v + 2));
                        -- NEXT_STATE Wait (29,22) at 436, 563
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 3 =>
                        -- v := v + 2 (36,17)
                        ctxt.v := Asn1Int((ctxt.v + 2));
                        -- NEXT_STATE Wait (38,22) at 225, 563
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 4 =>
                        -- DECISION v MOD 4 (45,23)
                        -- ANSWER 0 (47,17)
                        if ((ctxt.v mod 4)) = 0 then
                            -- req1 (49,27)
                            RI�req1;
                            -- writeln('Request sent to C1') (51,25)
                            Put("Request sent to C1");
                            New_Line;
                            -- set_timer((v+1)*1000,t) (53,25)
                            tmp26 := ((ctxt.v + 1) * 1000);
                            SET_t(tmp26'access);
                            -- NEXT_STATE Idle1 (55,30) at 225, 398
                            trId := -1;
                            ctxt.state := Idle1;
                            goto next_transition;
                            -- ANSWER 1 (57,17)
                        elsif ((ctxt.v mod 4)) = 1 then
                            -- req2 (59,27)
                            RI�req2;
                            -- writeln('Request sent to C2') (61,25)
                            Put("Request sent to C2");
                            New_Line;
                            -- set_timer((v+1)*1000,t) (63,25)
                            tmp37 := ((ctxt.v + 1) * 1000);
                            SET_t(tmp37'access);
                            -- NEXT_STATE Idle2 (65,30) at 436, 398
                            trId := -1;
                            ctxt.state := Idle2;
                            goto next_transition;
                            -- ANSWER 2 (67,17)
                        elsif ((ctxt.v mod 4)) = 2 then
                            -- req3 (69,27)
                            RI�req3;
                            -- writeln('Request sent to C3') (71,25)
                            Put("Request sent to C3");
                            New_Line;
                            -- set_timer((v+1)*1000,t) (73,25)
                            tmp48 := ((ctxt.v + 1) * 1000);
                            SET_t(tmp48'access);
                            -- NEXT_STATE Idle3 (75,30) at 647, 398
                            trId := -1;
                            ctxt.state := Idle3;
                            goto next_transition;
                            -- ANSWER else (None,None)
                        else
                            -- writeln('Reinitializing') (79,25)
                            Put("Reinitializing");
                            New_Line;
                            -- v := 0 (81,25)
                            ctxt.v := Asn1Int(0);
                            -- NEXT_STATE Wait (83,30) at 838, 338
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                        end if;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end p;