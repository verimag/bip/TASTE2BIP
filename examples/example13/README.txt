This example (a variant of example12) consists of:
- Four functions P, C1, C2, and C3
- P communicates with C1 via the interface req of the latter (call req1 from P)
- P communicates with C2 via the interface req of the latter (call req2 from P)
- P communicates with C3 via the interface req of the latter (call req3 from P)
- P sends a request reqi at almost each activation (cyclic interface req) as follows: the request to be send in modeled by an integer variable; if the variable modulo 4 is 0 req1 is sent, if it is 1 req2 is sent, if it is 2 req3 is sent, otherwise the variable is reinitialized and P waits next activation; if req1 is sent a timer is set to 1000 milliseconds and P enters a specific state Idle; for req2 the timer is set to 2000 milliseconds and P goes to the same Idle; after req3 P goes to Wait; v is update before going to the new location acoordingly - addition by 3 for req1 and subtraction by 1 for all the other cases; in state Idle, P waits for the time elapse and goes back to the initial state waiting for next activation
- Ci (i=1,2,3) prints a message upon the reception of req

