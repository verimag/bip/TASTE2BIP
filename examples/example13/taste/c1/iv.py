#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['p'] = {
    'name_with_case' : 'P',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['p']['functional_states']['t'] = {
    'fullFsName' : 't',
    'typeName' : 'Timer',
    'moduleName' : 'TASTE-Directives',
    'asn1FileName' : 'dummy'
}

functions['p']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'p',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['p']['interfaces']['step']['paramsInOrdered'] = []

functions['p']['interfaces']['step']['paramsOutOrdered'] = []

functions['p']['interfaces']['req1'] = {
    'port_name': 'req1',
    'parent_fv': 'p',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c1',
    'calling_threads': {},
    'distant_name': 'req',
    'queue_size': 1
}

functions['p']['interfaces']['req1']['paramsInOrdered'] = []

functions['p']['interfaces']['req1']['paramsOutOrdered'] = []

functions['p']['interfaces']['req2'] = {
    'port_name': 'req2',
    'parent_fv': 'p',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c2',
    'calling_threads': {},
    'distant_name': 'req',
    'queue_size': 1
}

functions['p']['interfaces']['req2']['paramsInOrdered'] = []

functions['p']['interfaces']['req2']['paramsOutOrdered'] = []

functions['p']['interfaces']['req3'] = {
    'port_name': 'req3',
    'parent_fv': 'p',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c3',
    'calling_threads': {},
    'distant_name': 'req',
    'queue_size': 1
}

functions['p']['interfaces']['req3']['paramsInOrdered'] = []

functions['p']['interfaces']['req3']['paramsOutOrdered'] = []

functions['c1'] = {
    'name_with_case' : 'C1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c1']['interfaces']['req'] = {
    'port_name': 'req',
    'parent_fv': 'c1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['c1']['interfaces']['req']['paramsInOrdered'] = []

functions['c1']['interfaces']['req']['paramsOutOrdered'] = []

functions['c2'] = {
    'name_with_case' : 'C2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c2']['interfaces']['req'] = {
    'port_name': 'req',
    'parent_fv': 'c2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['c2']['interfaces']['req']['paramsInOrdered'] = []

functions['c2']['interfaces']['req']['paramsOutOrdered'] = []

functions['c3'] = {
    'name_with_case' : 'C3',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c3']['interfaces']['req'] = {
    'port_name': 'req',
    'parent_fv': 'c3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['c3']['interfaces']['req']['paramsInOrdered'] = []

functions['c3']['interfaces']['req']['paramsOutOrdered'] = []
