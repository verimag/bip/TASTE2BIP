-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body p is
    type States is (idle, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v : aliased asn1SccT_UInt8 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure t is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end t;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp21 : aliased asn1SccT_UInt32;
        tmp10 : aliased asn1SccT_UInt32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (15,18) at 402, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- NEXT_STATE Wait (21,22) at 426, 566
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- DECISION v mod 4 (28,23)
                        -- ANSWER 0 (30,17)
                        if ((ctxt.v mod 4)) = 0 then
                            -- req1 (32,27)
                            RI�req1;
                            -- writeln('Request sent to C1') (34,25)
                            Put("Request sent to C1");
                            New_Line;
                            -- set_timer(1000,t) (36,25)
                            tmp10 := 1000;
                            SET_t(tmp10'access);
                            -- v := v+3 (38,25)
                            ctxt.v := Asn1Int((ctxt.v + 3));
                            -- NEXT_STATE Idle (40,30) at 426, 456
                            trId := -1;
                            ctxt.state := Idle;
                            goto next_transition;
                            -- ANSWER 1 (42,17)
                        elsif ((ctxt.v mod 4)) = 1 then
                            -- req2 (44,27)
                            RI�req2;
                            -- writeln('Request sent to C2') (46,25)
                            Put("Request sent to C2");
                            New_Line;
                            -- set_timer(2000, t) (48,25)
                            tmp21 := 2000;
                            SET_t(tmp21'access);
                            -- v := v - 1 (50,25)
                            ctxt.v := Asn1Int((ctxt.v - 1));
                            -- NEXT_STATE Idle (52,30) at 629, 470
                            trId := -1;
                            ctxt.state := Idle;
                            goto next_transition;
                            -- ANSWER 2 (54,17)
                        elsif ((ctxt.v mod 4)) = 2 then
                            -- req3 (56,27)
                            RI�req3;
                            -- writeln('Request sent to C3') (58,25)
                            Put("Request sent to C3");
                            New_Line;
                            -- v := v - 1 (60,25)
                            ctxt.v := Asn1Int((ctxt.v - 1));
                            -- ANSWER else (None,None)
                        else
                            -- writeln('Reinitializing') (64,25)
                            Put("Reinitializing");
                            New_Line;
                            -- v := v - 1 (66,25)
                            ctxt.v := Asn1Int((ctxt.v - 1));
                        end if;
                        -- NEXT_STATE Wait (69,22) at 791, 616
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end p;