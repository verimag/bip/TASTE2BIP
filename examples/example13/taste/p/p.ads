-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package p is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "p_step");
    --  Provided interface "t"
    procedure t;
    pragma Export(C, t, "p_t");
    --  Paramless required interface "req1"
    procedure RI�req1;
    pragma import(C, RI�req1, "p_RI_req1");
    --  Paramless required interface "req2"
    procedure RI�req2;
    pragma import(C, RI�req2, "p_RI_req2");
    --  Paramless required interface "req3"
    procedure RI�req3;
    pragma import(C, RI�req3, "p_RI_req3");
    --  Timer t SET and RESET functions
    procedure SET_t(val: access asn1SccT_UInt32);
    pragma import(C, SET_t, "p_RI_set_t");
    procedure RESET_t;
    pragma import(C, RESET_t, "p_RI_reset_t");
end p;