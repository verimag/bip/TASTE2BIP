-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body b is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        x : aliased asn1SccT_Int32 := 0;
        aux : aliased asn1SccT_Int32;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 4;
    procedure runTransition(Id: Integer);
    procedure up is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end up;
        

    procedure print(x: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.aux := x.all;
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end print;
        

    procedure sync is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(3);
                when others =>
                    runTransition(CS_Only);
            end case;
        end sync;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 46, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- x := x + 1 (18,17)
                        ctxt.x := Asn1Int((ctxt.x + 1));
                        -- NEXT_STATE Wait (20,22) at 127, 175
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('The received value is ',aux) (24,17)
                        Put("The received value is ");
                        Put(Asn1Int'Image(ctxt.aux));
                        New_Line;
                        -- NEXT_STATE Wait (26,22) at 294, 175
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 3 =>
                        -- writeln('The computed value is ', x) (30,17)
                        Put("The computed value is ");
                        Put(Asn1Int'Image(ctxt.x));
                        New_Line;
                        -- NEXT_STATE Wait (32,22) at 545, 175
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end b;