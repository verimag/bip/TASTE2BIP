-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package b is
    --  Provided interface "up"
    procedure up;
    pragma Export(C, up, "b_up");
    --  Provided interface "print"
    procedure print(x: access asn1SccT_Int32);
    pragma Export(C, print, "b_print");
    --  Provided interface "sync"
    procedure sync;
    pragma Export(C, sync, "b_sync");
end b;