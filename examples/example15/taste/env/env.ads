-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package env is
    --  Provided interface "a"
    procedure a;
    pragma Export(C, a, "env_a");
    --  Provided interface "b"
    procedure b;
    pragma Export(C, b, "env_b");
end env;