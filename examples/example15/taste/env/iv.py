#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['env'] = {
    'name_with_case' : 'Env',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['env']['interfaces']['a'] = {
    'port_name': 'a',
    'parent_fv': 'env',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['env']['interfaces']['a']['paramsInOrdered'] = []

functions['env']['interfaces']['a']['paramsOutOrdered'] = []

functions['env']['interfaces']['b'] = {
    'port_name': 'b',
    'parent_fv': 'env',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['env']['interfaces']['b']['paramsInOrdered'] = []

functions['env']['interfaces']['b']['paramsOutOrdered'] = []

functions['k1'] = {
    'name_with_case' : 'K1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['k1']['functional_states']['ta'] = {
    'fullFsName' : 'ta',
    'typeName' : 'Timer',
    'moduleName' : 'TASTE-Directives',
    'asn1FileName' : 'dummy'
}

functions['k1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'k1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 2000,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k1']['interfaces']['step']['paramsInOrdered'] = []

functions['k1']['interfaces']['step']['paramsOutOrdered'] = []

functions['k1']['interfaces']['q'] = {
    'port_name': 'q',
    'parent_fv': 'k1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k1']['interfaces']['q']['paramsInOrdered'] = []

functions['k1']['interfaces']['q']['paramsOutOrdered'] = []

functions['k1']['interfaces']['m'] = {
    'port_name': 'm',
    'parent_fv': 'k1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k1']['interfaces']['m']['paramsInOrdered'] = []

functions['k1']['interfaces']['m']['paramsOutOrdered'] = []

functions['k1']['interfaces']['a'] = {
    'port_name': 'a',
    'parent_fv': 'k1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'env',
    'calling_threads': {},
    'distant_name': 'a',
    'queue_size': 1
}

functions['k1']['interfaces']['a']['paramsInOrdered'] = []

functions['k1']['interfaces']['a']['paramsOutOrdered'] = []

functions['k1']['interfaces']['p'] = {
    'port_name': 'p',
    'parent_fv': 'k1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k2',
    'calling_threads': {},
    'distant_name': 'p',
    'queue_size': 1
}

functions['k1']['interfaces']['p']['paramsInOrdered'] = []

functions['k1']['interfaces']['p']['paramsOutOrdered'] = []

functions['k1']['interfaces']['n'] = {
    'port_name': 'n',
    'parent_fv': 'k1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k3',
    'calling_threads': {},
    'distant_name': 'n',
    'queue_size': 1
}

functions['k1']['interfaces']['n']['paramsInOrdered'] = ['val']

functions['k1']['interfaces']['n']['paramsOutOrdered'] = []

functions['k1']['interfaces']['n']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example14/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'n',
    'param_direction': param_in
}

functions['k2'] = {
    'name_with_case' : 'K2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['k2']['functional_states']['tb'] = {
    'fullFsName' : 'tb',
    'typeName' : 'Timer',
    'moduleName' : 'TASTE-Directives',
    'asn1FileName' : 'dummy'
}

functions['k2']['interfaces']['p'] = {
    'port_name': 'p',
    'parent_fv': 'k2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k2']['interfaces']['p']['paramsInOrdered'] = []

functions['k2']['interfaces']['p']['paramsOutOrdered'] = []

functions['k2']['interfaces']['u'] = {
    'port_name': 'u',
    'parent_fv': 'k2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k2']['interfaces']['u']['paramsInOrdered'] = []

functions['k2']['interfaces']['u']['paramsOutOrdered'] = []

functions['k2']['interfaces']['b'] = {
    'port_name': 'b',
    'parent_fv': 'k2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'env',
    'calling_threads': {},
    'distant_name': 'b',
    'queue_size': 1
}

functions['k2']['interfaces']['b']['paramsInOrdered'] = []

functions['k2']['interfaces']['b']['paramsOutOrdered'] = []

functions['k2']['interfaces']['q'] = {
    'port_name': 'q',
    'parent_fv': 'k2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k1',
    'calling_threads': {},
    'distant_name': 'q',
    'queue_size': 1
}

functions['k2']['interfaces']['q']['paramsInOrdered'] = []

functions['k2']['interfaces']['q']['paramsOutOrdered'] = []

functions['k2']['interfaces']['v'] = {
    'port_name': 'v',
    'parent_fv': 'k2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k3',
    'calling_threads': {},
    'distant_name': 'v',
    'queue_size': 1
}

functions['k2']['interfaces']['v']['paramsInOrdered'] = ['val']

functions['k2']['interfaces']['v']['paramsOutOrdered'] = []

functions['k2']['interfaces']['v']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example14/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'v',
    'param_direction': param_in
}

functions['k3'] = {
    'name_with_case' : 'K3',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['k3']['interfaces']['n'] = {
    'port_name': 'n',
    'parent_fv': 'k3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 3000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k3']['interfaces']['n']['paramsInOrdered'] = ['val']

functions['k3']['interfaces']['n']['paramsOutOrdered'] = []

functions['k3']['interfaces']['n']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example14/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'n',
    'param_direction': param_in
}

functions['k3']['interfaces']['v'] = {
    'port_name': 'v',
    'parent_fv': 'k3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 3000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k3']['interfaces']['v']['paramsInOrdered'] = ['val']

functions['k3']['interfaces']['v']['paramsOutOrdered'] = []

functions['k3']['interfaces']['v']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example14/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'v',
    'param_direction': param_in
}

functions['k3']['interfaces']['req'] = {
    'port_name': 'req',
    'parent_fv': 'k3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 3000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 3000,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k3']['interfaces']['req']['paramsInOrdered'] = []

functions['k3']['interfaces']['req']['paramsOutOrdered'] = []

functions['k3']['interfaces']['m'] = {
    'port_name': 'm',
    'parent_fv': 'k3',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k1',
    'calling_threads': {},
    'distant_name': 'm',
    'queue_size': 1
}

functions['k3']['interfaces']['m']['paramsInOrdered'] = []

functions['k3']['interfaces']['m']['paramsOutOrdered'] = []

functions['k3']['interfaces']['u'] = {
    'port_name': 'u',
    'parent_fv': 'k3',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k2',
    'calling_threads': {},
    'distant_name': 'u',
    'queue_size': 1
}

functions['k3']['interfaces']['u']['paramsInOrdered'] = []

functions['k3']['interfaces']['u']['paramsOutOrdered'] = []
