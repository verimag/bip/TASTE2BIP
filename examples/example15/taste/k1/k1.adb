-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body k1 is
    type States is (idle, testval, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        i : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 8;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(CS_Only);
                when testval =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(7);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure q is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(4);
                when testval =>
                    runTransition(2);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end q;
        

    procedure m is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(3);
                when testval =>
                    runTransition(1);
                when wait =>
                    runTransition(6);
                when others =>
                    runTransition(CS_Only);
            end case;
        end m;
        

    procedure ta is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(5);
                when testval =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end ta;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp33 : aliased asn1SccT_UInt32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- n(i) (18,19)
                        RI�n(ctxt.i'Access);
                        -- writeln('!n') (20,17)
                        Put("!n");
                        New_Line;
                        -- NEXT_STATE TestVal (22,22) at 1058, 226
                        trId := -1;
                        ctxt.state := TestVal;
                        goto next_transition;
                    when 2 =>
                        -- NEXT_STATE Wait (26,22) at 1148, 116
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 3 =>
                        -- n(i) (33,19)
                        RI�n(ctxt.i'Access);
                        -- writeln('!n') (35,17)
                        Put("!n");
                        New_Line;
                        -- NEXT_STATE Idle (37,22) at 660, 229
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when 4 =>
                        -- a (41,19)
                        RI�a;
                        -- writeln('!a') (43,17)
                        Put("!a");
                        New_Line;
                        -- i := i + 1 (45,17)
                        ctxt.i := Asn1Int((ctxt.i + 1));
                        -- NEXT_STATE Idle (47,22) at 765, 279
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when 5 =>
                        -- RESET_TIMER(ta) (51,17)
                        RESET_ta;
                        -- writeln('K1 timer reset') (53,17)
                        Put("K1 timer reset");
                        New_Line;
                        -- NEXT_STATE TestVal (55,22) at 908, 224
                        trId := -1;
                        ctxt.state := TestVal;
                        goto next_transition;
                    when 6 =>
                        -- n(i) (62,19)
                        RI�n(ctxt.i'Access);
                        -- writeln('!n') (64,17)
                        Put("!n");
                        New_Line;
                        -- NEXT_STATE Wait (66,22) at 408, 230
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 7 =>
                        -- a (70,19)
                        RI�a;
                        -- writeln('!a') (72,17)
                        Put("!a");
                        New_Line;
                        -- i := i + 1 (74,17)
                        ctxt.i := Asn1Int((ctxt.i + 1));
                        -- p (76,19)
                        RI�p;
                        -- writeln('!p') (78,17)
                        Put("!p");
                        New_Line;
                        -- SET_TIMER(1000,ta) (80,17)
                        tmp33 := 1000;
                        SET_ta(tmp33'access);
                        -- writeln('K1 timer set') (82,17)
                        Put("K1 timer set");
                        New_Line;
                        -- NEXT_STATE Idle (84,22) at 540, 495
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end k1;