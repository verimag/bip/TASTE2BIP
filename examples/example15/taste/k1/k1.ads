-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package k1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "k1_step");
    --  Provided interface "q"
    procedure q;
    pragma Export(C, q, "k1_q");
    --  Provided interface "m"
    procedure m;
    pragma Export(C, m, "k1_m");
    --  Provided interface "ta"
    procedure ta;
    pragma Export(C, ta, "k1_ta");
    --  Paramless required interface "a"
    procedure RI�a;
    pragma import(C, RI�a, "k1_RI_a");
    --  Paramless required interface "p"
    procedure RI�p;
    pragma import(C, RI�p, "k1_RI_p");
    --  Required interface "n"
    procedure RI�n(val: access asn1SccT_Int32);
    pragma import(C, RI�n, "k1_RI_n");
    --  Timer ta SET and RESET functions
    procedure SET_ta(val: access asn1SccT_UInt32);
    pragma import(C, SET_ta, "k1_RI_set_ta");
    procedure RESET_ta;
    pragma import(C, RESET_ta, "k1_RI_reset_ta");
end k1;