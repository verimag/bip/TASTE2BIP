-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body k2 is
    type States is (idle, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        j : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 6;
    procedure runTransition(Id: Integer);
    procedure p is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(4);
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end p;
        

    procedure u is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(3);
                when wait =>
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end u;
        

    procedure tb is
        begin
            case ctxt.state is
                when idle =>
                    runTransition(5);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end tb;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp13 : aliased asn1SccT_UInt32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- v(j) (18,19)
                        RI�v(ctxt.j'Access);
                        -- writeln('  !v') (20,17)
                        Put("  !v");
                        New_Line;
                        -- NEXT_STATE Wait (22,22) at 500, 231
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- b (26,19)
                        RI�b;
                        -- writeln('  !b') (28,17)
                        Put("  !b");
                        New_Line;
                        -- j := j + 1 (30,17)
                        ctxt.j := Asn1Int((ctxt.j + 1));
                        -- SET_TIMER(2000,tb) (32,17)
                        tmp13 := 2000;
                        SET_tb(tmp13'access);
                        -- writeln('  K2 timer set') (34,17)
                        Put("  K2 timer set");
                        New_Line;
                        -- NEXT_STATE Idle (36,22) at 681, 390
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when 3 =>
                        -- v(j) (43,19)
                        RI�v(ctxt.j'Access);
                        -- writeln('  !v') (45,17)
                        Put("  !v");
                        New_Line;
                        -- NEXT_STATE Idle (47,22) at 868, 227
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when 4 =>
                        -- b (51,19)
                        RI�b;
                        -- writeln('  !b') (53,17)
                        Put("  !b");
                        New_Line;
                        -- j := j + 1 (55,17)
                        ctxt.j := Asn1Int((ctxt.j + 1));
                        -- NEXT_STATE Idle (57,22) at 979, 282
                        trId := -1;
                        ctxt.state := Idle;
                        goto next_transition;
                    when 5 =>
                        -- RESET_TIMER(tb) (61,17)
                        RESET_tb;
                        -- writeln('  K2 timer reset') (63,17)
                        Put("  K2 timer reset");
                        New_Line;
                        -- q (65,19)
                        RI�q;
                        -- writeln('  !q') (67,17)
                        Put("  !q");
                        New_Line;
                        -- NEXT_STATE Wait (69,22) at 1129, 332
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end k2;