-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package k2 is
    --  Provided interface "p"
    procedure p;
    pragma Export(C, p, "k2_p");
    --  Provided interface "u"
    procedure u;
    pragma Export(C, u, "k2_u");
    --  Provided interface "tb"
    procedure tb;
    pragma Export(C, tb, "k2_tb");
    --  Paramless required interface "b"
    procedure RI�b;
    pragma import(C, RI�b, "k2_RI_b");
    --  Paramless required interface "q"
    procedure RI�q;
    pragma import(C, RI�q, "k2_RI_q");
    --  Required interface "v"
    procedure RI�v(val: access asn1SccT_Int32);
    pragma import(C, RI�v, "k2_RI_v");
    --  Timer tb SET and RESET functions
    procedure SET_tb(val: access asn1SccT_UInt32);
    pragma import(C, SET_tb, "k2_RI_set_tb");
    procedure RESET_tb;
    pragma import(C, RESET_tb, "k2_RI_reset_tb");
end k2;