-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body k3 is
    type States is (wait1, wait2, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        i : aliased asn1SccT_Int32;
        c : aliased asn1SccT_Boolean := true;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 4;
    procedure runTransition(Id: Integer);
    procedure n(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait1 =>
                    ctxt.i := val.all;
                    runTransition(2);
                when wait2 =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end n;
        

    procedure v(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait1 =>
                    runTransition(CS_Only);
                when wait2 =>
                    ctxt.i := val.all;
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end v;
        

    procedure req is
        begin
            case ctxt.state is
                when wait1 =>
                    runTransition(CS_Only);
                when wait2 =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(3);
                when others =>
                    runTransition(CS_Only);
            end case;
        end req;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- NEXT_STATE Wait (18,22) at 584, 508
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- NEXT_STATE Wait (25,22) at 465, 508
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 3 =>
                        -- DECISION c (-1,-1)
                        -- ANSWER true (34,17)
                        if (ctxt.c) = true then
                            -- m (36,27)
                            RI�m;
                            -- writeln('    !m') (38,25)
                            Put("    !m");
                            New_Line;
                            -- c := not c (40,25)
                            ctxt.c := (not ctxt.c);
                            -- NEXT_STATE Wait1 (42,30) at 465, 398
                            trId := -1;
                            ctxt.state := Wait1;
                            goto next_transition;
                            -- ANSWER false (44,17)
                        elsif (ctxt.c) = false then
                            -- u (46,27)
                            RI�u;
                            -- writeln('    !u') (48,25)
                            Put("    !u");
                            New_Line;
                            -- c := not c (50,25)
                            ctxt.c := (not ctxt.c);
                            -- NEXT_STATE Wait2 (52,30) at 584, 398
                            trId := -1;
                            ctxt.state := Wait2;
                            goto next_transition;
                        end if;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end k3;