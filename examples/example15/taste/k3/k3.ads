-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package k3 is
    --  Provided interface "n"
    procedure n(val: access asn1SccT_Int32);
    pragma Export(C, n, "k3_n");
    --  Provided interface "v"
    procedure v(val: access asn1SccT_Int32);
    pragma Export(C, v, "k3_v");
    --  Provided interface "req"
    procedure req;
    pragma Export(C, req, "k3_req");
    --  Paramless required interface "m"
    procedure RI�m;
    pragma import(C, RI�m, "k3_RI_m");
    --  Paramless required interface "u"
    procedure RI�u;
    pragma import(C, RI�u, "k3_RI_u");
end k3;