-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Provided interface "rF2F1"
    procedure rF2F1(v: access asn1SccT_Int32);
    pragma Export(C, rF2F1, "f1_rF2F1");
    --  Provided interface "aF1F2"
    procedure aF1F2(v: access asn1SccT_Int32);
    pragma Export(C, aF1F2, "f1_aF1F2");
    --  Required interface "aF2F1"
    procedure RI�aF2F1(v: access asn1SccT_Int32);
    pragma import(C, RI�aF2F1, "f1_RI_aF2F1");
    --  Required interface "rF1F2"
    procedure RI�rF1F2(v: access asn1SccT_Int32);
    pragma import(C, RI�rF1F2, "f1_RI_rF1F2");
end f1;