#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['f1'] = {
    'name_with_case' : 'F1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'f1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 4000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 40,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f1']['interfaces']['step']['paramsInOrdered'] = []

functions['f1']['interfaces']['step']['paramsOutOrdered'] = []

functions['f1']['interfaces']['rF2F1'] = {
    'port_name': 'rF2F1',
    'parent_fv': 'f1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f1']['interfaces']['rF2F1']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['rF2F1']['paramsOutOrdered'] = []

functions['f1']['interfaces']['rF2F1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'rF2F1',
    'param_direction': param_in
}

functions['f1']['interfaces']['aF1F2'] = {
    'port_name': 'aF1F2',
    'parent_fv': 'f1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f1']['interfaces']['aF1F2']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['aF1F2']['paramsOutOrdered'] = []

functions['f1']['interfaces']['aF1F2']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'aF1F2',
    'param_direction': param_in
}

functions['f1']['interfaces']['aF2F1'] = {
    'port_name': 'aF2F1',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f2',
    'calling_threads': {},
    'distant_name': 'aF2F1',
    'queue_size': 1
}

functions['f1']['interfaces']['aF2F1']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['aF2F1']['paramsOutOrdered'] = []

functions['f1']['interfaces']['aF2F1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'aF2F1',
    'param_direction': param_in
}

functions['f1']['interfaces']['rF1F2'] = {
    'port_name': 'rF1F2',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f2',
    'calling_threads': {},
    'distant_name': 'rF1F2',
    'queue_size': 1
}

functions['f1']['interfaces']['rF1F2']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['rF1F2']['paramsOutOrdered'] = []

functions['f1']['interfaces']['rF1F2']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'rF1F2',
    'param_direction': param_in
}

functions['f2'] = {
    'name_with_case' : 'F2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f2']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 3000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 30,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f2']['interfaces']['step']['paramsInOrdered'] = []

functions['f2']['interfaces']['step']['paramsOutOrdered'] = []

functions['f2']['interfaces']['aF2F1'] = {
    'port_name': 'aF2F1',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f2']['interfaces']['aF2F1']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['aF2F1']['paramsOutOrdered'] = []

functions['f2']['interfaces']['aF2F1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'aF2F1',
    'param_direction': param_in
}

functions['f2']['interfaces']['rF1F2'] = {
    'port_name': 'rF1F2',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f2']['interfaces']['rF1F2']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['rF1F2']['paramsOutOrdered'] = []

functions['f2']['interfaces']['rF1F2']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'rF1F2',
    'param_direction': param_in
}

functions['f2']['interfaces']['rF2F1'] = {
    'port_name': 'rF2F1',
    'parent_fv': 'f2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f1',
    'calling_threads': {},
    'distant_name': 'rF2F1',
    'queue_size': 1
}

functions['f2']['interfaces']['rF2F1']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['rF2F1']['paramsOutOrdered'] = []

functions['f2']['interfaces']['rF2F1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'rF2F1',
    'param_direction': param_in
}

functions['f2']['interfaces']['aF1F2'] = {
    'port_name': 'aF1F2',
    'parent_fv': 'f2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f1',
    'calling_threads': {},
    'distant_name': 'aF1F2',
    'queue_size': 1
}

functions['f2']['interfaces']['aF1F2']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['aF1F2']['paramsOutOrdered'] = []

functions['f2']['interfaces']['aF1F2']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example15/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'aF1F2',
    'param_direction': param_in
}
