-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f2 is
    type States is (waitanswer, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v1 : aliased asn1SccT_Int32 := 0;
        v2 : aliased asn1SccT_Int32;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 4;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when waitanswer =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure aF2F1(v: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when waitanswer =>
                    ctxt.v1 := v.all;
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end aF2F1;
        

    procedure rF1F2(v: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when waitanswer =>
                    runTransition(CS_Only);
                when wait =>
                    ctxt.v2 := v.all;
                    runTransition(3);
                when others =>
                    runTransition(CS_Only);
            end case;
        end rF1F2;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 178, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- NEXT_STATE Wait (18,22) at 327, 347
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('F2 request with val ', v1) (25,17)
                        Put("F2 request with val ");
                        Put(Asn1Int'Image(ctxt.v1));
                        New_Line;
                        -- rF2F1(v1) (27,19)
                        RI�rF2F1(ctxt.v1'Access);
                        -- NEXT_STATE WaitAnswer (29,22) at 315, 237
                        trId := -1;
                        ctxt.state := WaitAnswer;
                        goto next_transition;
                    when 3 =>
                        -- v2:=v2+1 (33,17)
                        ctxt.v2 := Asn1Int((ctxt.v2 + 1));
                        -- writeln('F2 answer with val ', v2) (35,17)
                        Put("F2 answer with val ");
                        Put(Asn1Int'Image(ctxt.v2));
                        New_Line;
                        -- aF1F2(v2) (37,19)
                        RI�aF1F2(ctxt.v2'Access);
                        -- NEXT_STATE Wait (39,22) at 569, 293
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f2;