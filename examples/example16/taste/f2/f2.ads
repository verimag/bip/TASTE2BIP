-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f2 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f2_step");
    --  Provided interface "aF2F1"
    procedure aF2F1(v: access asn1SccT_Int32);
    pragma Export(C, aF2F1, "f2_aF2F1");
    --  Provided interface "rF1F2"
    procedure rF1F2(v: access asn1SccT_Int32);
    pragma Export(C, rF1F2, "f2_rF1F2");
    --  Required interface "rF2F1"
    procedure RI�rF2F1(v: access asn1SccT_Int32);
    pragma import(C, RI�rF2F1, "f2_RI_rF2F1");
    --  Required interface "aF1F2"
    procedure RI�aF1F2(v: access asn1SccT_Int32);
    pragma import(C, RI�aF1F2, "f2_RI_aF1F2");
end f2;