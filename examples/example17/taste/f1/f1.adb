-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f1 is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v1 : aliased asn1SccT_Int32 := 0;
        v2 : aliased asn1SccT_Boolean := false;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        msgPending : aliased Asn1Boolean := True;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 275, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- mesA(v1) (18,19)
                        RI�mesA(ctxt.v1'Access);
                        -- writeln('Sent A') (20,17)
                        Put("Sent A");
                        New_Line;
                        -- v2 := not v2 (22,17)
                        ctxt.v2 := (not ctxt.v2);
                        -- writeln('    val v2 ', v2) (24,17)
                        Put("    val v2 ");
                        Put(Boolean'Image(ctxt.v2));
                        New_Line;
                        -- NEXT_STATE Wait (26,22) at 354, 333
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('    val v2 ', v2) (30,17)
                        Put("    val v2 ");
                        Put(Boolean'Image(ctxt.v2));
                        New_Line;
                        -- mesB(v2) (32,19)
                        RI�mesB(ctxt.v2'Access);
                        -- writeln('Sent B') (34,17)
                        Put("Sent B");
                        New_Line;
                        -- v2 := not v2 (36,17)
                        ctxt.v2 := (not ctxt.v2);
                        -- NEXT_STATE Wait (38,22) at 518, 325
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                --  Process continuous signals
                if ctxt.initDone then
                    Check_Queue(msgPending'access);
                end if;
                if not msgPending and trId = -1 and ctxt.state = wait then
                    -- Priority 1
                    -- DECISION not v2 (28,17)
                    -- ANSWER true (None,None)
                    if ((not ctxt.v2)) = true then
                        trId := 2;
                    end if;
                end if;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f1;