-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Required interface "mesA"
    procedure RI�mesA(val: access asn1SccT_Int32);
    pragma import(C, RI�mesA, "f1_RI_mesA");
    --  Required interface "mesB"
    procedure RI�mesB(val: access asn1SccT_Boolean);
    pragma import(C, RI�mesB, "f1_RI_mesB");
    procedure Check_Queue(res: access Asn1Boolean);
    pragma import(C, Check_Queue, "f1_check_queue");
end f1;