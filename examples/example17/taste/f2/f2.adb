-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f2 is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        v1 : aliased asn1SccT_Int32;
        v2 : aliased asn1SccT_Boolean;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure mesA(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.v1 := val.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end mesA;
        

    procedure mesB(val: access asn1SccT_Boolean) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.v2 := val.all;
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end mesB;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 241, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('  Received A') (18,17)
                        Put("  Received A");
                        New_Line;
                        -- NEXT_STATE Wait (20,22) at 364, 178
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('  Received B') (24,17)
                        Put("  Received B");
                        New_Line;
                        -- NEXT_STATE Wait (26,22) at 533, 175
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f2;