-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package f2 is
    --  Provided interface "mesA"
    procedure mesA(val: access asn1SccT_Int32);
    pragma Export(C, mesA, "f2_mesA");
    --  Provided interface "mesB"
    procedure mesB(val: access asn1SccT_Boolean);
    pragma Export(C, mesB, "f2_mesB");
end f2;