-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body c1 is
    type States is (handlep2, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        i : aliased asn1SccT_Int32 := 0;
        c : aliased asn1SccT_Int32 := 0;
        v : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure comp1(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when handlep2 =>
                    runTransition(CS_Only);
                when wait =>
                    ctxt.v := val.all;
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp1;
        

    procedure comp2(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when handlep2 =>
                    ctxt.v := val.all;
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp2;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (13,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- c := c + 2 * v (19,17)
                        ctxt.c := Asn1Int((ctxt.c + (2 * ctxt.v)));
                        -- writeln('Received from P2 ', v) (21,17)
                        Put("Received from P2 ");
                        Put(Asn1Int'Image(ctxt.v));
                        New_Line;
                        -- writeln('After P2 the value is ', c) (23,17)
                        Put("After P2 the value is ");
                        Put(Asn1Int'Image(ctxt.c));
                        New_Line;
                        -- NEXT_STATE Wait (25,22) at 780, 519
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- c := c + v (32,17)
                        ctxt.c := Asn1Int((ctxt.c + ctxt.v));
                        -- i := i + 1 (33,0)
                        ctxt.i := Asn1Int((ctxt.i + 1));
                        -- writeln('Received from P1 ', v) (35,17)
                        Put("Received from P1 ");
                        Put(Asn1Int'Image(ctxt.v));
                        New_Line;
                        -- writeln('After P1 the value is ', c) (37,17)
                        Put("After P1 the value is ");
                        Put(Asn1Int'Image(ctxt.c));
                        New_Line;
                        -- DECISION (i mod 2) = 0 (39,31)
                        -- ANSWER false (41,17)
                        if (((ctxt.i mod 2) = 0)) = false then
                            null;
                            -- ANSWER true (43,17)
                        elsif (((ctxt.i mod 2) = 0)) = true then
                            -- i := 0 (45,25)
                            ctxt.i := Asn1Int(0);
                            -- NEXT_STATE HandleP2 (47,30) at 442, 497
                            trId := -1;
                            ctxt.state := HandleP2;
                            goto next_transition;
                        end if;
                        -- NEXT_STATE Wait (50,22) at 403, 547
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end c1;