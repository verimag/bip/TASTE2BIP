#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['p1'] = {
    'name_with_case' : 'P1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['p1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'p1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 5000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 500,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['p1']['interfaces']['step']['paramsInOrdered'] = []

functions['p1']['interfaces']['step']['paramsOutOrdered'] = []

functions['p1']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'p1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c1',
    'calling_threads': {},
    'distant_name': 'comp1',
    'queue_size': 1
}

functions['p1']['interfaces']['comp']['paramsInOrdered'] = ['val']

functions['p1']['interfaces']['comp']['paramsOutOrdered'] = []

functions['p1']['interfaces']['comp']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example17/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['p2'] = {
    'name_with_case' : 'P2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['p2']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'p2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 5000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 500,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['p2']['interfaces']['step']['paramsInOrdered'] = []

functions['p2']['interfaces']['step']['paramsOutOrdered'] = []

functions['p2']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'p2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'c1',
    'calling_threads': {},
    'distant_name': 'comp2',
    'queue_size': 1
}

functions['p2']['interfaces']['comp']['paramsInOrdered'] = ['val']

functions['p2']['interfaces']['comp']['paramsOutOrdered'] = []

functions['p2']['interfaces']['comp']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example17/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['c1'] = {
    'name_with_case' : 'C1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['c1']['interfaces']['comp1'] = {
    'port_name': 'comp1',
    'parent_fv': 'c1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 2
}

functions['c1']['interfaces']['comp1']['paramsInOrdered'] = ['val']

functions['c1']['interfaces']['comp1']['paramsOutOrdered'] = []

functions['c1']['interfaces']['comp1']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example17/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp1',
    'param_direction': param_in
}

functions['c1']['interfaces']['comp2'] = {
    'port_name': 'comp2',
    'parent_fv': 'c1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 7000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 2
}

functions['c1']['interfaces']['comp2']['paramsInOrdered'] = ['val']

functions['c1']['interfaces']['comp2']['paramsOutOrdered'] = []

functions['c1']['interfaces']['comp2']['in']['val'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example17/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp2',
    'param_direction': param_in
}
