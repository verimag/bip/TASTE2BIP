@cpp(src="ext-cpp/ExternExample19Fcns.cpp",include="ExternExample19Fcns.hpp")
package Example19

  // Extern declarations
  extern data type queueBool
  extern function int get_size(const queueBool)
  extern function push_back(queueBool, bool, int)
  extern function bool pop_front(queueBool)
  extern function write(const bool)
  extern function writeln(const bool)

  extern data type queueInt
  extern function int get_size(const queueInt)
  extern function push_back(queueInt, int, int)
  extern function int pop_front(queueInt)
  extern function write(const int)
  extern function writeln(const int)

  extern data type queueFloat
  extern function int get_size(const queueFloat)
  extern function push_back(queueFloat, float, int)
  extern function float pop_front(queueFloat)
  extern function write(const float)
  extern function writeln(const float)

  extern data type queueString
  extern function int get_size(const queueString)
  extern function push_back(queueString, string, int)
  extern function string pop_front(queueString)
  extern function write(const string)
  extern function writeln(const string)

  // Port types
  port type Port()

  port type portNil()
  port type queuePortNil(int current)
  port type portBool(bool v)
  port type queuePortBool(queueBool q)
  port type portInt(int v)
  port type queuePortInt(queueInt q)
  port type portFloat(float v)
  port type queuePortFloat(queueFloat q)
  port type portString(string v)
  port type queuePortString(queueString q)

  // Connector types
  connector type Sync(Port s, Port r)
    define s r
  end

  connector type SendRecNil(portNil s, portNil r)
    define s r
  end

  connector type SendRecQueueNil(queuePortNil s, portNil r)
    define s r
    on s r
      down { if (s.current > 0) then s.current = s.current - 1; else writeln("Queue empty!"); fi }
  end

  connector type SendRecBool(portBool s, portBool r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueBool(queuePortBool s, portBool r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecInt(portInt s, portInt r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueInt(queuePortInt s, portInt r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecFloat(portFloat s, portFloat r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueFloat(queuePortFloat s, portFloat r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecString(portString s, portString r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueString(queuePortString s, portString r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end


  // Generic TASTE cyclic interface type
  atom type TASTE_PI_Cyclic(int period, int deadline)
    clock t unit millisecond

    export port Port sig_out()
    export port Port sig_return()
 
    place l0, l1, l2

    initial to l0

    on sig_out
      from l0 to l1
      eager
      do { t = 0; }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l2 to l1
      provided (t == period)
      do { t = 0; }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= period)
  end

  // Generic TASTE sporadic interfaces

  // Sporadic interface type with no parameter
  atom type TASTE_PI_Sporadic_Nil(int miat, int deadline, int size)
    clock t unit millisecond
    data int current

    export port portNil sig_out()
    export port Port sig_return()
    export port queuePortNil sig_in(current)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { current = 0; }

    on sig_in
      from l0 to l1
      provided(current>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of bool type
  atom type TASTE_PI_Sporadic_Bool(int miat, int deadline, int size)
    clock t unit millisecond
    data bool val
    data queueBool q

    export port portBool sig_out(val)
    export port Port sig_return()
    export port queuePortBool sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = false; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end
  
  // Sporadic interface type with parameter of int type
  atom type TASTE_PI_Sporadic_Int(int miat, int deadline, int size)
    clock t unit millisecond
    data int val
    data queueInt q

    export port portInt sig_out(val)
    export port Port sig_return()
    export port queuePortInt sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of float type
  atom type TASTE_PI_Sporadic_Float(int miat, int deadline, int size)
    clock t unit millisecond
    data float val
    data queueFloat q

    export port portFloat sig_out(val)
    export port Port sig_return()
    export port queuePortFloat sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of string type
  atom type TASTE_PI_Sporadic_String(int miat, int deadline, int size)
    clock t unit millisecond
    data string val
    data queueString q

    export port portString sig_out(val)
    export port Port sig_return()
    export port queuePortString sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = ""; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // TASTE terminal functions
  atom type F1()
    data int v
    data bool ctrl
    data int F1_aF2_val
    data int F1_aF3_val

    export port Port F1_step_in()
    export port Port F1_step_return()
    export port portInt F1_aF2_in(F1_aF2_val)
    export port Port F1_aF2_return()
    export port portInt F1_aF3_in(F1_aF3_val)
    export port Port F1_aF3_return()
    export port portNil F1_r2F2_out()
    export port portNil F1_r2F3_out()

    port Port silent()

    place WaitAnswer, Wait, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15, l16, l17

    initial to Wait
      do { ctrl = true; }

    // Definition of state WaitAnswer
    
    on F1_aF2_in
      from WaitAnswer to l2
      eager

    internal
      from l2 to l3
      do { v = F1_aF2_val; }

    on F1_aF2_return
      from l3 to Wait
      eager

    on F1_aF3_in
      from WaitAnswer to l4
      eager

    internal
      from l4 to l5
      do { v = F1_aF3_val; }

    on F1_aF3_return
      from l5 to Wait
      eager

    on F1_step_in
      from WaitAnswer to l6
      eager

    on F1_step_return
      from l6 to WaitAnswer
      eager

    // Definition of state Wait

    on silent
      from Wait to l7
      provided (ctrl)
      eager

    on silent
      from l7 to l8
      eager
      do { writeln(" F2 request"); }

    on F1_r2F2_out
      from l8 to l9
      eager

    on silent
      from l9 to l10
      eager
      do { ctrl = ! ctrl; }

    internal
      from l10 to WaitAnswer

    on silent
      from Wait to l11
      provided (! ctrl)
      eager

    on silent
      from l11 to l12
      eager
      do { writeln(" F3 request"); }

    on F1_r2F3_out
      from l12 to l13
      eager

    on silent
      from l13 to l14
      eager
      do { ctrl = ! ctrl; }

    internal
      from l14 to WaitAnswer

    on F1_aF2_in
      from Wait to l15
      eager

    on F1_aF2_return
      from l15 to Wait
      eager

    on F1_aF3_in
      from Wait to l16
      eager

    on F1_aF3_return
      from l16 to Wait
      eager

    on F1_step_in
      from Wait to l17
      eager

    on F1_step_return
      from l17 to Wait
      eager

  end

  atom type F2()
    data int v
    data int F2_aF2_val

    export port portNil F2_r2F2_in()
    export port Port F2_r2F2_return()
    export port portInt F2_aF2_out(F2_aF2_val)

    port Port silent()

    place Wait, l1, l2, l3, l4, l5

    initial to Wait
      do { v = 0; }

    // Definition of state Wait
    
    on F2_r2F2_in
      from Wait to l1
      eager

    on silent
      from l1 to l2
      eager
      do { write("F2 answer "); writeln(v); }

    internal
      from l2 to l3
      do { F2_aF2_val = v; }

    on F2_aF2_out
      from l3 to l4
      eager

    on silent
      from l4 to l5
      eager
      do { v = v + 2; }

    on F2_r2F2_return
      from l5 to Wait
      eager
    
  end

  atom type F3()
    data int v
    data int F3_aF3_val

    export port portNil F3_r2F3_in()
    export port Port F3_r2F3_return()
    export port portInt F3_aF3_out(F3_aF3_val)

    port Port silent()

    place Wait, l1, l2, l3, l4, l5

    initial to Wait
      do { v = 1; }

    // Definition of state Wait
    
    on F3_r2F3_in
      from Wait to l1
      eager

    on silent
      from l1 to l2
      eager
      do { write("F3 answer val "); writeln(v); }

    internal
      from l2 to l3
      do { F3_aF3_val = v; }

    on F3_aF3_out
      from l3 to l4
      eager

    on silent
      from l4 to l5
      eager
      do { v = v + 2; }

    on F3_r2F3_return
      from l5 to Wait
      eager

  end

  // Compound types
  compound type Syst()
    component F1 F1()
    component TASTE_PI_Sporadic_Int F1_aF2(1000,0,1)
    component TASTE_PI_Sporadic_Int F1_aF3(0,0,1)
    component TASTE_PI_Cyclic F1_step(2000,1000)
    component F2 F2()
    component TASTE_PI_Sporadic_Nil F2_r2F2(10000,0,1)
    component F3 F3()
    component TASTE_PI_Sporadic_Nil F3_r2F3(0,0,1)

    connector Sync F1_step2F1(F1_step.sig_out,F1.F1_step_in)
    connector Sync F12F1_step(F1.F1_step_return,F1_step.sig_return)
    connector Sync F12F1_aF2(F1.F1_aF2_return,F1_aF2.sig_return)
    connector Sync F12F1_aF3(F1.F1_aF3_return,F1_aF3.sig_return)
    connector Sync F22F2_r2F2(F2.F2_r2F2_return,F2_r2F2.sig_return)
    connector Sync F32F3_r2F3(F3.F3_r2F3_return,F3_r2F3.sig_return)
    connector SendRecInt F22F1_aF2(F2.F2_aF2_out,F1_aF2.sig_out)
    connector SendRecQueueInt F1_aF22F1(F1_aF2.sig_in,F1.F1_aF2_in)
    connector SendRecInt F32F1_aF3(F3.F3_aF3_out,F1_aF3.sig_out)
    connector SendRecQueueInt F1_aF32F1(F1_aF3.sig_in,F1.F1_aF3_in)
    connector SendRecNil F12F2_r2F2(F1.F1_r2F2_out,F2_r2F2.sig_out)
    connector SendRecQueueNil F2_r2F22F2(F2_r2F2.sig_in,F2.F2_r2F2_in)
    connector SendRecNil F12F3_r2F3(F1.F1_r2F3_out,F3_r2F3.sig_out)
    connector SendRecQueueNil F3_r2F32F3(F3_r2F3.sig_in,F3.F3_r2F3_in)
  end
end
