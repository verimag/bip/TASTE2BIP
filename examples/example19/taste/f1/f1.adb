-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f1 is
    type States is (waitanswer, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        ctrl : aliased asn1SccT_Boolean := true;
        v : aliased asn1SccT_Int32;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 5;
    procedure runTransition(Id: Integer);
    procedure aF2(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when waitanswer =>
                    ctxt.v := val.all;
                    runTransition(1);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end aF2;
        

    procedure aF3(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when waitanswer =>
                    ctxt.v := val.all;
                    runTransition(2);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end aF3;
        

    procedure step is
        begin
            case ctxt.state is
                when waitanswer =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(CS_Only);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        msgPending : aliased Asn1Boolean := True;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 239, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- NEXT_STATE Wait (18,22) at 359, 419
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- NEXT_STATE Wait (22,22) at 449, 419
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 3 =>
                        -- writeln(' F2 request') (29,17)
                        Put(" F2 request");
                        New_Line;
                        -- r2F2 (31,19)
                        RI�r2F2;
                        -- ctrl := not ctrl (33,17)
                        ctxt.ctrl := (not ctxt.ctrl);
                        -- NEXT_STATE WaitAnswer (35,22) at 392, 309
                        trId := -1;
                        ctxt.state := WaitAnswer;
                        goto next_transition;
                    when 4 =>
                        -- writeln(' F3 request') (39,17)
                        Put(" F3 request");
                        New_Line;
                        -- r2F3 (41,19)
                        RI�r2F3;
                        -- ctrl := not ctrl (43,17)
                        ctxt.ctrl := (not ctxt.ctrl);
                        -- NEXT_STATE WaitAnswer (45,22) at 611, 324
                        trId := -1;
                        ctxt.state := WaitAnswer;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                --  Process continuous signals
                if ctxt.initDone then
                    Check_Queue(msgPending'access);
                end if;
                if not msgPending and trId = -1 and ctxt.state = wait then
                    -- Priority 1
                    -- DECISION ctrl (-1,-1)
                    -- ANSWER true (None,None)
                    if (ctxt.ctrl) = true then
                        trId := 3;
                        -- Priority 1
                        -- DECISION not ctrl (37,17)
                        -- ANSWER true (None,None)
                    elsif ((not ctxt.ctrl)) = true then
                        trId := 4;
                    end if;
                end if;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f1;