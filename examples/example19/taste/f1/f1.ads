-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "aF2"
    procedure aF2(val: access asn1SccT_Int32);
    pragma Export(C, aF2, "f1_aF2");
    --  Provided interface "aF3"
    procedure aF3(val: access asn1SccT_Int32);
    pragma Export(C, aF3, "f1_aF3");
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Paramless required interface "r2F2"
    procedure RI�r2F2;
    pragma import(C, RI�r2F2, "f1_RI_r2F2");
    --  Paramless required interface "r2F3"
    procedure RI�r2F3;
    pragma import(C, RI�r2F3, "f1_RI_r2F3");
    procedure Check_Queue(res: access Asn1Boolean);
    pragma import(C, Check_Queue, "f1_check_queue");
end f1;