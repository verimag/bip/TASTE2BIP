-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package f2 is
    --  Provided interface "r2F2"
    procedure r2F2;
    pragma Export(C, r2F2, "f2_r2F2");
    --  Required interface "aF2"
    procedure RI�aF2(val: access asn1SccT_Int32);
    pragma import(C, RI�aF2, "f2_RI_aF2");
end f2;