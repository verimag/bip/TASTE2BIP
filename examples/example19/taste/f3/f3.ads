-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package f3 is
    --  Provided interface "r2F3"
    procedure r2F3;
    pragma Export(C, r2F3, "f3_r2F3");
    --  Required interface "aF3"
    procedure RI�aF3(val: access asn1SccT_Int32);
    pragma import(C, RI�aF3, "f3_RI_aF3");
end f3;