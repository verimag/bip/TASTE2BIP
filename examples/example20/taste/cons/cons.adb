-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body cons is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        val : aliased asn1SccT_Int32;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 2;
    procedure runTransition(Id: Integer);
    procedure comp(val: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.val := val.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (11,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- DECISION val mod 2 =0 (17,31)
                        -- ANSWER true (19,17)
                        if (((ctxt.val mod 2) = 0)) = true then
                            -- writeln('Received from ProdA value ', val) (21,25)
                            Put("Received from ProdA value ");
                            Put(Asn1Int'Image(ctxt.val));
                            New_Line;
                            -- NEXT_STATE Wait (23,30) at 275, 288
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                            -- ANSWER false (25,17)
                        elsif (((ctxt.val mod 2) = 0)) = false then
                            -- writeln('Received from ProdB value ', val) (27,25)
                            Put("Received from ProdB value ");
                            Put(Asn1Int'Image(ctxt.val));
                            New_Line;
                            -- NEXT_STATE Wait (29,30) at 575, 288
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                        end if;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end cons;