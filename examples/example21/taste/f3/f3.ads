-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f3 is
    --  Provided interface "comp1"
    procedure comp1;
    pragma Export(C, comp1, "f3_comp1");
    --  Provided interface "comp2"
    procedure comp2;
    pragma Export(C, comp2, "f3_comp2");
    --  Required interface "print"
    procedure RI�print(val: access asn1SccT_Int32);
    pragma import(C, RI�print, "f3_RI_print");
end f3;