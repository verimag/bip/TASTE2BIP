@cpp(src="ext-cpp/ExternExample22Fcns.cpp",include="ExternExample22Fcns.hpp")
package Example22

  // Extern declarations
  extern data type queueBool
  extern function int get_size(const queueBool)
  extern function push_back(queueBool, bool, int)
  extern function bool pop_front(queueBool)
  extern function write(const bool)
  extern function writeln(const bool)

  extern data type queueInt
  extern function int get_size(const queueInt)
  extern function push_back(queueInt, int, int)
  extern function int pop_front(queueInt)
  extern function write(const int)
  extern function writeln(const int)

  extern data type queueFloat
  extern function int get_size(const queueFloat)
  extern function push_back(queueFloat, float, int)
  extern function float pop_front(queueFloat)
  extern function write(const float)
  extern function writeln(const float)

  extern data type queueString
  extern function int get_size(const queueString)
  extern function push_back(queueString, string, int)
  extern function string pop_front(queueString)
  extern function write(const string)
  extern function writeln(const string)

  // Port types
  port type Port()

  port type portNil()
  port type queuePortNil(int current)
  port type portBool(bool v)
  port type queuePortBool(queueBool q)
  port type portInt(int v)
  port type queuePortInt(queueInt q)
  port type portFloat(float v)
  port type queuePortFloat(queueFloat q)
  port type portString(string v)
  port type queuePortString(queueString q)

  // Connector types
  connector type Sync(Port s, Port r)
    define s r
  end

  connector type SendRecNil(portNil s, portNil r)
    define s r
  end

  connector type SendRecQueueNil(queuePortNil s, portNil r)
    define s r
    on s r
      down { if (s.current > 0) then s.current = s.current - 1; else writeln("Queue empty!"); fi }
  end

  connector type SendRecBool(portBool s, portBool r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueBool(queuePortBool s, portBool r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecInt(portInt s, portInt r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueInt(queuePortInt s, portInt r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecFloat(portFloat s, portFloat r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueFloat(queuePortFloat s, portFloat r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecString(portString s, portString r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueString(queuePortString s, portString r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end


  // Generic TASTE cyclic interface type
  atom type TASTE_PI_Cyclic(int period, int deadline)
    clock t unit millisecond

    export port Port sig_out()
    export port Port sig_return()
 
    place l0, l1, l2

    initial to l0

    on sig_out
      from l0 to l1
      eager
      do { t = 0; }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l2 to l1
      provided (t == period)
      do { t = 0; }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= period)
  end

  // Generic TASTE sporadic interfaces

  // Sporadic interface type with no parameter
  atom type TASTE_PI_Sporadic_Nil(int miat, int deadline, int size)
    clock t unit millisecond
    data int current

    export port portNil sig_out()
    export port Port sig_return()
    export port queuePortNil sig_in(current)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { current = 0; }

    on sig_in
      from l0 to l1
      provided(current>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of bool type
  atom type TASTE_PI_Sporadic_Bool(int miat, int deadline, int size)
    clock t unit millisecond
    data bool val
    data queueBool q

    export port portBool sig_out(val)
    export port Port sig_return()
    export port queuePortBool sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = false; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end
  
  // Sporadic interface type with parameter of int type
  atom type TASTE_PI_Sporadic_Int(int miat, int deadline, int size)
    clock t unit millisecond
    data int val
    data queueInt q

    export port portInt sig_out(val)
    export port Port sig_return()
    export port queuePortInt sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of float type
  atom type TASTE_PI_Sporadic_Float(int miat, int deadline, int size)
    clock t unit millisecond
    data float val
    data queueFloat q

    export port portFloat sig_out(val)
    export port Port sig_return()
    export port queuePortFloat sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of string type
  atom type TASTE_PI_Sporadic_String(int miat, int deadline, int size)
    clock t unit millisecond
    data string val
    data queueString q

    export port portString sig_out(val)
    export port Port sig_return()
    export port queuePortString sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = ""; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // TASTE terminal functions
  atom type F1()
    data int i
    data int F1_comp_v

    export port Port F1_step_in()
    export port Port F1_step_return()
    export port portInt F1_comp_out(F1_comp_v)

    port Port silent()

    place Wait, l1, l2, l3, l4, l5

    initial to Wait
      do { }

    // Definition of state Wait

    on F1_step_in
      from Wait to l1
      eager

    internal
      from l1 to l2
      do { F1_comp_v = i; }

    on F1_comp_out
      from l2 to l3
      eager

    on silent
      from l3 to l4
      eager
      do { writeln("F1 sent request"); }

    on silent
      from l4 to l5
      eager
      do { i = i + 1; }

    on F1_step_return
      from l5 to Wait
      eager

  end

  atom type F2()

    export port Port F2_step_in()
    export port Port F2_step_return()
    export port portNil F2_comp_out()

    port Port silent()

    place Wait, l1, l2, l3

    initial to Wait
      do { }

    // Definition of state Wait

    on F2_step_in
      from Wait to l1
      eager

    on F2_comp_out
      from l1 to l2
      eager

    on silent
      from l2 to l3
      eager
      do { writeln("F2 sent request"); }

    on F2_step_return
      from l3 to Wait
      eager

  end

  atom type F3()
    data int v
    data int a
    data int b
    data int F3_comp1_v
    data int F3_print_val

    // Data variables corresponding to the procedure
    data int up_i
    data int up_a
    data int up_b

    export port portInt F3_comp1_in(F3_comp1_v)
    export port Port F3_comp1_return()
    export port portNil F3_comp2_in()
    export port Port F3_comp2_return()
    export port portInt F3_print_out(F3_print_val)

    port Port silent()

    place Wait, l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15, l16, l17, l18

    initial to Wait
      do { v = 0; a = 0; b = 0; }

    // Definition of state Wait

    on F3_comp1_in
      from Wait to l1
      eager

    internal
      from l1 to l2
      do { a = F3_comp1_v; }

    // Procedure up call

    // Setting the inputs fpar in (corresponding to START)

    internal
      from l2 to l3
      do { up_i = 0; up_a = a; }

    // Translating the procedure state machine 

    on silent
      from l3 to l4
      eager
      do { up_i = up_a * 2; }

    internal
      from l4 to l5
      do { F3_print_val = up_i; }

    on F3_print_out
      from l5 to l6
      eager

    on silent
      from l6 to l7
      eager
      do { up_b = up_a + 1; }

    // Setting the outputs fpar out (corresponding to return)

    internal
      from l7 to l8
      do { b = up_b; }

    // End procedure up call

    on silent
      from l8 to l9
      eager
      do { writeln("Request from comp1"); }

    on F3_comp1_return
      from l9 to Wait
      eager

    on F3_comp2_in
      from Wait to l10
      eager

    // Procedure up call

    // Setting the inputs fpar in

    internal
      from l10 to l11
      do { up_i = 0; up_a = v; }

    // Translating the procedure state machine

    on silent
      from l11 to l12
      eager
      do { up_i = up_a * 2; }

    internal
      from l12 to l13
      do { F3_print_val = up_i; }

    on F3_print_out
      from l13 to l14
      eager

    on silent
      from l14 to l15
      eager
      do { up_b = up_a + 1; }

    // Setting the outputs fpar out

    internal
      from l15 to l16
      do { b = up_b; }

    // End procedure up call

    on silent
      from l16 to l17
      eager
      do { v = b; }

    on silent
      from l17 to l18
      eager
      do { writeln("Request from comp2"); }

    on F3_comp2_return
      from l18 to Wait
      eager

  end

  atom type F4()
    data int v
    data int F4_print_val

    export port portInt F4_print_in(F4_print_val)
    export port Port F4_print_return()

    port Port silent()

    place Wait, l1, l2, l3

    initial to Wait
      do { }

    // Definition of state Wait

    on F4_print_in
      from Wait to l1
      eager

    internal
      from l1 to l2
      do { v = F4_print_val; }

    on silent
      from l2 to l3
      eager
      do { write("Value computed "); writeln(v); }

    on F4_print_return
      from l3 to Wait
      eager

  end

  // Compound types
  compound type Syst()
    component F1 F1()
    component TASTE_PI_Cyclic F1_step(2000,2000)
    component F2 F2()
    component TASTE_PI_Cyclic F2_step(2000,2000)
    component F3 F3()
    component TASTE_PI_Sporadic_Int F3_comp1(1000,1000,2)
    component TASTE_PI_Sporadic_Nil F3_comp2(1000,1000,2)
    component F4 F4()
    component TASTE_PI_Sporadic_Int F4_print(0,0,2)

    connector Sync F1_step2F1(F1_step.sig_out,F1.F1_step_in)
    connector Sync F12F1_step(F1.F1_step_return,F1_step.sig_return)
    connector Sync F2_step2F2(F2_step.sig_out,F2.F2_step_in)
    connector Sync F22F2_step(F2.F2_step_return,F2_step.sig_return)
    connector Sync F32F3_comp1(F3.F3_comp1_return,F3_comp1.sig_return)
    connector Sync F32F3_comp2(F3.F3_comp2_return,F3_comp2.sig_return)
    connector Sync F42F4_print(F4.F4_print_return,F4_print.sig_return)
    connector SendRecInt F12F3_comp1(F1.F1_comp_out,F3_comp1.sig_out)
    connector SendRecQueueInt F3_comp12F3(F3_comp1.sig_in,F3.F3_comp1_in)
    connector SendRecNil F22F3_comp2(F2.F2_comp_out,F3_comp2.sig_out)
    connector SendRecQueueNil F3_comp22F3(F3_comp2.sig_in,F3.F3_comp2_in)
    connector SendRecInt F32F4_print(F3.F3_print_out,F4_print.sig_out)
    connector SendRecQueueInt F4_print2F4(F4_print.sig_in,F4.F4_print_in)
  end
end
