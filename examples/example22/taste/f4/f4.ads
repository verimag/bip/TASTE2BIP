-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f4 is
    --  Provided interface "print"
    procedure print(val: access asn1SccT_Int32);
    pragma Export(C, print, "f4_print");
end f4;