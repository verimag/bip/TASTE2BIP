#!/bin/bash

rm -rf output

mkdir output

bipc.sh -I . -p Example23 -d "Syst()" --gencpp-output output --gencpp-ld-l rt --gencpp-cc-I $PWD/ext-cpp

mkdir output/build

cd output/build

cmake ..

make

./system -l 100 --silent
