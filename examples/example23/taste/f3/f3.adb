-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f3 is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        a : aliased asn1SccT_Int32 := 0;
        v : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure p�up(a: in asn1SccT_Int32);
    procedure p�up(a: in asn1SccT_Int32) is
        i : aliased asn1SccT_Int32 := 0;
        tmp16 : aliased asn1SccT_Int32;
        begin
            -- i := a * 2 (22,17)
            i := Asn1Int((a * 2));
            -- print(i) (24,19)
            tmp16 := Asn1Int(i);
            RI�print(tmp16'Access);
            -- v := v + 1 (26,17)
            ctxt.v := Asn1Int((ctxt.v + 1));
            -- RETURN  (None,None) at 211, 288
            return;
        end p�up;
        

    procedure comp1(v: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.a := v.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp1;
        

    procedure comp2 is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp2;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (33,18) at 182, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- up(a) (39,17)
                        p�up(ctxt.a);
                        -- writeln('Request from comp1') (41,17)
                        Put("Request from comp1");
                        New_Line;
                        -- NEXT_STATE Wait (43,22) at 332, 230
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- up(v) (47,17)
                        p�up(ctxt.v);
                        -- writeln('Request from comp2') (49,17)
                        Put("Request from comp2");
                        New_Line;
                        -- NEXT_STATE Wait (51,22) at 553, 225
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f3;