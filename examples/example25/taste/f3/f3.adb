-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f3 is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        a : aliased asn1SccT_Int32 := 0;
        b : aliased asn1SccT_Int32;
        d : aliased asn1SccT_Boolean := false;
        v : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure p�up1(a: in asn1SccT_Int32;c: in asn1SccT_Boolean;d: in out asn1SccT_Int32;b: in out asn1SccT_Boolean);
    procedure p�up2(a: in asn1SccT_Int32;b: in out asn1SccT_Int32);
    procedure p�up1(a: in asn1SccT_Int32;c: in asn1SccT_Boolean;d: in out asn1SccT_Int32;b: in out asn1SccT_Boolean) is
        tmp42 : aliased asn1SccT_Int32;
        tmp52 : aliased asn1SccT_Int32;
        begin
            -- DECISION c (-1,-1)
            -- ANSWER true (27,17)
            if (c) = true then
                -- d := a - 1 (29,25)
                d := Asn1Int((a - 1));
                -- b := not c (30,0)
                b := (not c);
                -- print(d) (32,27)
                tmp42 := Asn1Int(d);
                RI�print(tmp42'Access);
                -- ANSWER false (34,17)
            elsif (c) = false then
                -- d := a + 2 (36,25)
                d := Asn1Int((a + 2));
                -- b := not c (37,0)
                b := (not c);
                -- print(d) (39,27)
                tmp52 := Asn1Int(d);
                RI�print(tmp52'Access);
            end if;
            -- RETURN  (None,None) at 530, 352
            return;
        end p�up1;
        

    procedure p�up2(a: in asn1SccT_Int32;b: in out asn1SccT_Int32) is
        tmp58 : aliased asn1SccT_Int32;
        begin
            -- b := a - 1 (56,17)
            b := Asn1Int((a - 1));
            -- print(b) (58,19)
            tmp58 := Asn1Int(b);
            RI�print(tmp58'Access);
            -- RETURN  (None,None) at 271, 213
            return;
        end p�up2;
        

    procedure comp1(v: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.a := v.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp1;
        

    procedure comp2 is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp2;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        tmp12 : aliased asn1SccT_Int32;
        tmp30 : aliased asn1SccT_Int32;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (65,18) at 182, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('Request from comp1') (71,17)
                        Put("Request from comp1");
                        New_Line;
                        -- up2(a,b) (73,17)
                        p�up2(ctxt.a, ctxt.b);
                        -- print(2*b) (75,19)
                        tmp12 := Asn1Int((2 * ctxt.b));
                        RI�print(tmp12'Access);
                        -- NEXT_STATE Wait (77,22) at 332, 275
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('Request from comp2') (81,17)
                        Put("Request from comp2");
                        New_Line;
                        -- up1(v,d,b,d) (83,17)
                        p�up1(ctxt.v, ctxt.d, ctxt.b, ctxt.d);
                        -- v := v + 1 (85,17)
                        ctxt.v := Asn1Int((ctxt.v + 1));
                        -- print(3*b) (87,19)
                        tmp30 := Asn1Int((3 * ctxt.b));
                        RI�print(tmp30'Access);
                        -- NEXT_STATE Wait (89,22) at 553, 330
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f3;