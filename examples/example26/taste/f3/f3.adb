-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body f3 is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        a : aliased asn1SccT_Int32;
        v : aliased asn1SccT_Int32 := 0;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 3;
    procedure runTransition(Id: Integer);
    procedure p�up(x: in out asn1SccT_Int32);
    procedure p�up(x: in out asn1SccT_Int32) is
        tmp26 : aliased asn1SccT_Int32;
        begin
            -- x := x + 1 (22,17)
            x := Asn1Int((x + 1));
            -- print(x) (24,19)
            tmp26 := Asn1Int(x);
            RI�print(tmp26'Access);
            -- RETURN  (None,None) at 530, 236
            return;
        end p�up;
        

    procedure comp1(v: access asn1SccT_Int32) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.a := v.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp1;
        

    procedure comp2 is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end comp2;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (31,18) at 47, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('F1: variable value before proc ', a) (37,17)
                        Put("F1: variable value before proc ");
                        Put(Asn1Int'Image(ctxt.a));
                        New_Line;
                        -- up(a) (39,17)
                        p�up(ctxt.a);
                        -- writeln('F1: variable value after proc ', a) (41,17)
                        Put("F1: variable value after proc ");
                        Put(Asn1Int'Image(ctxt.a));
                        New_Line;
                        -- NEXT_STATE Wait (43,22) at 287, 275
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 2 =>
                        -- writeln('F2: variable value before proc ', v) (47,17)
                        Put("F2: variable value before proc ");
                        Put(Asn1Int'Image(ctxt.v));
                        New_Line;
                        -- up(v) (49,17)
                        p�up(ctxt.v);
                        -- writeln('F2: variable value after proc ', v) (51,17)
                        Put("F2: variable value after proc ");
                        Put(Asn1Int'Image(ctxt.v));
                        New_Line;
                        -- NEXT_STATE Wait (53,22) at 588, 275
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end f3;