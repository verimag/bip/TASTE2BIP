@cpp(src="ext-cpp/ExternExample27Fcns.cpp,../taste/f2/f2.c",include="ExternExample27Fcns.hpp,f2.h")
package Example27

  // Extern declarations
  extern data type queueBool
  extern function int get_size(const queueBool)
  extern function push_back(queueBool, bool, int)
  extern function bool pop_front(queueBool)
  extern function write(const bool)
  extern function writeln(const bool)

  extern data type queueInt
  extern function int get_size(const queueInt)
  extern function push_back(queueInt, int, int)
  extern function int pop_front(queueInt)
  extern function write(const int)
  extern function writeln(const int)

  extern data type queueFloat
  extern function int get_size(const queueFloat)
  extern function push_back(queueFloat, float, int)
  extern function float pop_front(queueFloat)
  extern function write(const float)
  extern function writeln(const float)

  extern data type queueString
  extern function int get_size(const queueString)
  extern function push_back(queueString, string, int)
  extern function string pop_front(queueString)
  extern function write(const string)
  extern function writeln(const string)

  // Integration of the C code
  extern function F2_startup_wrap()
  extern function float F2_print_p_wrap(const int)
  extern function float F2_print_s_wrap(const int)

  // Port types
  port type Port()

  port type portNil()
  port type queuePortNil(int current)
  port type portBool(bool v)
  port type queuePortBool(queueBool q)
  port type portInt(int v)
  port type queuePortInt(queueInt q)
  port type portFloat(float v)
  port type queuePortFloat(queueFloat q)
  port type portString(string v)
  port type queuePortString(queueString q)

  // Connector types
  connector type Sync(Port s, Port r)
    define s r
  end

  connector type SendRecNil(portNil s, portNil r)
    define s r
  end

  connector type SendRecQueueNil(queuePortNil s, portNil r)
    define s r
    on s r
      down { if (s.current > 0) then s.current = s.current - 1; else writeln("Queue empty!"); fi }
  end

  connector type SendRecBool(portBool s, portBool r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueBool(queuePortBool s, portBool r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecInt(portInt s, portInt r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueInt(queuePortInt s, portInt r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecFloat(portFloat s, portFloat r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueFloat(queuePortFloat s, portFloat r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecString(portString s, portString r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueString(queuePortString s, portString r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end


  // Generic TASTE cyclic interface type
  atom type TASTE_PI_Cyclic(int period, int deadline)
    clock t unit millisecond

    export port Port sig_out()
    export port Port sig_return()
 
    place l0, l1, l2

    initial to l0

    on sig_out
      from l0 to l1
      eager
      do { t = 0; }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l2 to l1
      provided (t == period)
      do { t = 0; }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= period)
  end

  // Generic TASTE sporadic interfaces

  // Sporadic interface type with no parameter
  atom type TASTE_PI_Sporadic_Nil(int miat, int deadline, int size)
    clock t unit millisecond
    data int current

    export port portNil sig_out()
    export port Port sig_return()
    export port queuePortNil sig_in(current)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { current = 0; }

    on sig_in
      from l0 to l1
      provided(current>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of bool type
  atom type TASTE_PI_Sporadic_Bool(int miat, int deadline, int size)
    clock t unit millisecond
    data bool val
    data queueBool q

    export port portBool sig_out(val)
    export port Port sig_return()
    export port queuePortBool sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = false; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end
  
  // Sporadic interface type with parameter of int type
  atom type TASTE_PI_Sporadic_Int(int miat, int deadline, int size)
    clock t unit millisecond
    data int val
    data queueInt q

    export port portInt sig_out(val)
    export port Port sig_return()
    export port queuePortInt sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of float type
  atom type TASTE_PI_Sporadic_Float(int miat, int deadline, int size)
    clock t unit millisecond
    data float val
    data queueFloat q

    export port portFloat sig_out(val)
    export port Port sig_return()
    export port queuePortFloat sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of string type
  atom type TASTE_PI_Sporadic_String(int miat, int deadline, int size)
    clock t unit millisecond
    data string val
    data queueString q

    export port portString sig_out(val)
    export port Port sig_return()
    export port queuePortString sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = ""; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end


  // Generic TASTE protected interface type
  atom type TASTE_PI_Protected()
    export port Port lock()
    export port Port release()
 
    place l0, l1

    initial to l0

    on lock
      from l0 to l1
      eager

    on release
      from l1 to l0
      eager
  end

  // TASTE terminal functions
  atom type F1(int print_p_deadline, int print_p_wcet)
    data int i
    data int F1_print_s_v

    // Clocks generated for the protected required interface
    clock F1_comp_wait unit millisecond   // clock enforcing the time of the operation call
    data float F1_comp_time               // variable containing the computed time of function execution
    clock F1_comp_t unit millisecond      // clock measuring the total time since lock is demanded

    export port Port F1_step_in()
    export port Port F1_step_return()

    export port portInt F1_print_s_out(F1_print_s_v)

    // Translation of the protected required interface
    export port Port F1_print_p_lock()
    export port Port F1_print_p_release()

    port Port silent()

    place Wait, l1, l2, l3, l4, l5, l6, l7, l8, l9

    // Definition of state Wait

    initial to Wait
      do { i = 0; }

    on F1_step_in
      from Wait to l1
      eager

    // Translation of the print_p call (required protected interface)

    // Set clock measuring the "total" time to 0
    internal
      from l1 to l2
      do { F1_comp_t = 0; }

    // Lock the "protected object"
    on F1_print_p_lock
      from l2 to l3
      eager

    // Call the implemented function wrapper (use the name of the provided protected interface to which it is connected)
    // This function is a wrapper of the implemented function and returns the execution time
    // Set the clock enforcing the computed execution time to 0
    on silent
      from l3 to l4
      eager
      do { F1_comp_wait = 0; F1_comp_time = F2_print_p_wrap(i); }

    // Wait for the amount of the computed execution time
    on silent
      from l4 to l5
      provided (F1_comp_wait == F1_comp_time)
      eager

    // Release the protected object and reset clock
    on F1_print_p_release
      from l5 to l6
      eager
      do { F1_comp_t = 0; }

    // End of translation of print_p call

    internal
      from l6 to l7
      do { F1_print_s_v = i; }

    on F1_print_s_out
      from l7 to l8
      eager

    on silent
      from l8 to l9
      eager
      do { i = i + 1; }

    on F1_step_return
      from l9 to Wait
      eager

    invariant inv2 at l2 provided (F1_comp_t <= print_p_deadline)
    invariant inv3 at l3 provided (F1_comp_t <= print_p_deadline)
    invariant inv4 at l4 provided (F1_comp_wait <= print_p_wcet && F1_comp_t <= print_p_deadline)
    invariant inv5 at l5 provided (F1_comp_t <= print_p_deadline)

  end

  // Translation of a TASTE function written in C
  // Several BIP components are generated: one component handling all sporadic interfaces and one component for each protected interface
  // Component handling the sporadic interfaces
  atom type F2()
    data int F2_print_s_v

    // Clocks generated for the sporadic interface
    clock F2_comp_wait unit millisecond
    data float F2_comp_time

    export port portInt F2_print_s_in(F2_print_s_v)
    export port Port F2_print_s_return()

    port Port silent()

    place Wait, l1, l2, l3

    // Initialization of C variables called here
    initial to Wait
      do { F2_startup_wrap(); }

    // For every sporadic interface
    // Retrieve an element from the queue
    on F2_print_s_in
      from Wait to l1
      eager

    // Call implemented function
    on silent
      from l1 to l2
      eager
      do { F2_comp_wait = 0; F2_comp_time = F2_print_s_wrap(F2_print_s_v); }

    on silent
      from l2 to l3
      provided (F2_comp_wait == F2_comp_time)
      eager

    // Check next element
    on F2_print_s_return
      from l3 to Wait
      eager

    // No invariants generated here, computation times are handled in the queue components

  end
  
  // The components for each protected interface are of type TASTE_PI_Protected, they are just to be initialized


  // Compound types
  compound type Syst()
    component F1 F1(10,0)
    component TASTE_PI_Cyclic F1_step(2000,2000)
    component F2 F2()
    component TASTE_PI_Sporadic_Int F2_print_s(4000,2500,2)
    component TASTE_PI_Protected F2_print_p()

    connector Sync F1_step2F1(F1_step.sig_out,F1.F1_step_in)
    connector Sync F12F1_step(F1.F1_step_return,F1_step.sig_return)
    connector Sync F22F2_print_s(F2.F2_print_s_return,F2_print_s.sig_return)
    connector Sync F12F2_print_p_lock(F1.F1_print_p_lock,F2_print_p.lock)
    connector Sync F12F2_print_p_release(F1.F1_print_p_release,F2_print_p.release)
    connector SendRecInt F12F2_print_s(F1.F1_print_s_out,F2_print_s.sig_out)
    connector SendRecQueueInt F2_print_s2F2(F2_print_s.sig_in,F2.F2_print_s_in)
  end
end
