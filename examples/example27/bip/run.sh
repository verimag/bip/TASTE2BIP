#!/bin/bash

rm -rf output

mkdir output

bipc.sh -I . -p Example27 -d "Syst()" --gencpp-output output --gencpp-ld-l rt --gencpp-cc-I $PWD/ext-cpp --gencpp-cc-I $PWD/../taste/f2/ --gencpp-cc-I $PWD/../taste/binary.c/GlueAndBuild/gluef2/ --gencpp-cc-I $PWD/../taste/f2/dataview/

mkdir output/build

cd output/build

cmake ..

make

./system -l 100 --silent
