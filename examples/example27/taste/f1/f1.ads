-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Required interface "print_s"
    procedure RI�print_s(v: access asn1SccT_Int32);
    pragma import(C, RI�print_s, "f1_RI_print_s");
    --  Sync required interface "print_p"
    procedure RI�print_p(v: access asn1SccT_Int32);
    pragma import(C, RI�print_p, "f1_RI_print_p");
end f1;