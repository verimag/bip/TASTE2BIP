#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['f1'] = {
    'name_with_case' : 'F1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'f1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1000,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f1']['interfaces']['step']['paramsInOrdered'] = []

functions['f1']['interfaces']['step']['paramsOutOrdered'] = []

functions['f1']['interfaces']['print_p'] = {
    'port_name': 'print_p',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f2',
    'calling_threads': {},
    'distant_name': 'print_p',
    'queue_size': 1
}

functions['f1']['interfaces']['print_p']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['print_p']['paramsOutOrdered'] = []

functions['f1']['interfaces']['print_p']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example27/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print_p',
    'param_direction': param_in
}

functions['f1']['interfaces']['print_s'] = {
    'port_name': 'print_s',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f2',
    'calling_threads': {},
    'distant_name': 'print_s',
    'queue_size': 1
}

functions['f1']['interfaces']['print_s']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['print_s']['paramsOutOrdered'] = []

functions['f1']['interfaces']['print_s']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example27/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print_s',
    'param_direction': param_in
}

functions['f2'] = {
    'name_with_case' : 'F2',
    'runtime_nature': thread,
    'language': C,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f2']['interfaces']['print_p'] = {
    'port_name': 'print_p',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 0,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f2']['interfaces']['print_p']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['print_p']['paramsOutOrdered'] = []

functions['f2']['interfaces']['print_p']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example27/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print_p',
    'param_direction': param_in
}

functions['f2']['interfaces']['print_s'] = {
    'port_name': 'print_s',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 4000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 20,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 2
}

functions['f2']['interfaces']['print_s']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['print_s']['paramsOutOrdered'] = []

functions['f2']['interfaces']['print_s']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example27/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print_s',
    'param_direction': param_in
}
