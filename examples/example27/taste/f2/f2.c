/* User code: This file will not be overwritten by TASTE. */

#include "f2.h"
#include <stdio.h>

void f2_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
}

void f2_PI_print_p(const asn1SccT_Int32 *IN_v){
    printf("Received value protected intf: %d\n", (int) *IN_v);
}

void f2_PI_print_s(const asn1SccT_Int32 *IN_v){
    int aux = ((int) *IN_v) * 2;
    printf("Received value sporadic intf: %d\n", aux);
    
}


