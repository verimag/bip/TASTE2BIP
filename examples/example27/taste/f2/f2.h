/* This file was generated automatically: DO NOT MODIFY IT ! */

/* Declaration of the functions that have to be provided by the user */

#ifndef __USER_CODE_H_f2__
#define __USER_CODE_H_f2__

#include "C_ASN1_Types.h"

#ifdef __cplusplus
extern "C" {
#endif

void f2_startup();

void f2_PI_print_p(const asn1SccT_Int32 *);

void f2_PI_print_s(const asn1SccT_Int32 *);

#ifdef __cplusplus
}
#endif


#endif
