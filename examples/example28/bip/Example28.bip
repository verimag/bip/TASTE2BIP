@cpp(src="ext-cpp/ExternExample28Fcns.cpp,../taste/f3/f3.c",include="ExternExample28Fcns.hpp,f3.h")
package Example28

  // Extern declarations
  extern data type queueBool
  extern function int get_size(const queueBool)
  extern function push_back(queueBool, bool, int)
  extern function bool pop_front(queueBool)
  extern function write(const bool)
  extern function writeln(const bool)

  extern data type queueInt
  extern function int get_size(const queueInt)
  extern function push_back(queueInt, int, int)
  extern function int pop_front(queueInt)
  extern function write(const int)
  extern function writeln(const int)

  extern data type queueFloat
  extern function int get_size(const queueFloat)
  extern function push_back(queueFloat, float, int)
  extern function float pop_front(queueFloat)
  extern function write(const float)
  extern function writeln(const float)

  extern data type queueString
  extern function int get_size(const queueString)
  extern function push_back(queueString, string, int)
  extern function string pop_front(queueString)
  extern function write(const string)
  extern function writeln(const string)

  // Integration of the C code
  extern function F3_startup_wrap()
  extern function float F3_comp_wrap(const int)

  // Port types
  port type Port()

  port type portNil()
  port type queuePortNil(int current)
  port type portBool(bool v)
  port type queuePortBool(queueBool q)
  port type portInt(int v)
  port type queuePortInt(queueInt q)
  port type portFloat(float v)
  port type queuePortFloat(queueFloat q)
  port type portString(string v)
  port type queuePortString(queueString q)

  // Connector types
  connector type Sync(Port s, Port r)
    define s r
  end

  connector type SendRecNil(portNil s, portNil r)
    define s r
  end

  connector type SendRecQueueNil(queuePortNil s, portNil r)
    define s r
    on s r
      down { if (s.current > 0) then s.current = s.current - 1; else writeln("Queue empty!"); fi }
  end

  connector type SendRecBool(portBool s, portBool r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueBool(queuePortBool s, portBool r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecInt(portInt s, portInt r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueInt(queuePortInt s, portInt r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecFloat(portFloat s, portFloat r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueFloat(queuePortFloat s, portFloat r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end

  connector type SendRecString(portString s, portString r)
    define s r
    on s r
      down { r.v = s.v; }
  end

  connector type SendRecQueueString(queuePortString s, portString r)
    define s r
    on s r
      down { r.v = pop_front(s.q); }
  end


  // Generic TASTE cyclic interface type
  atom type TASTE_PI_Cyclic(int period, int deadline)
    clock t unit millisecond

    export port Port sig_out()
    export port Port sig_return()
 
    place l0, l1, l2

    initial to l0

    on sig_out
      from l0 to l1
      eager
      do { t = 0; }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l2 to l1
      provided (t == period)
      do { t = 0; }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= period)
  end

  // Generic TASTE sporadic interfaces

  // Sporadic interface type with no parameter
  atom type TASTE_PI_Sporadic_Nil(int miat, int deadline, int size)
    clock t unit millisecond
    data int current

    export port portNil sig_out()
    export port Port sig_return()
    export port queuePortNil sig_in(current)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { current = 0; }

    on sig_in
      from l0 to l1
      provided(current>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { if (current < size) then current = current + 1; else writeln("Queue full!"); fi }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of bool type
  atom type TASTE_PI_Sporadic_Bool(int miat, int deadline, int size)
    clock t unit millisecond
    data bool val
    data queueBool q

    export port portBool sig_out(val)
    export port Port sig_return()
    export port queuePortBool sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = false; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end
  
  // Sporadic interface type with parameter of int type
  atom type TASTE_PI_Sporadic_Int(int miat, int deadline, int size)
    clock t unit millisecond
    data int val
    data queueInt q

    export port portInt sig_out(val)
    export port Port sig_return()
    export port queuePortInt sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of float type
  atom type TASTE_PI_Sporadic_Float(int miat, int deadline, int size)
    clock t unit millisecond
    data float val
    data queueFloat q

    export port portFloat sig_out(val)
    export port Port sig_return()
    export port queuePortFloat sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = 0; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end

  // Sporadic interface type with parameter of string type
  atom type TASTE_PI_Sporadic_String(int miat, int deadline, int size)
    clock t unit millisecond
    data string val
    data queueString q

    export port portString sig_out(val)
    export port Port sig_return()
    export port queuePortString sig_in(q)

    port Port silent()

    place l0, l1, l2

    initial to l0
      do { val = ""; }

    on sig_in
      from l0 to l1
      provided(get_size(q)>0)
      eager
      do { t = 0; }

    on sig_out
      from l0 to l0
      eager
      do { push_back(q, val, size); }

    on sig_return
      from l1 to l2
      eager

    on sig_out
      from l1 to l1
      eager
      do { push_back(q, val, size); }

    on silent
      from l2 to l0
      provided (t == miat)
      eager
      do { t = 0; }

    on sig_out
      from l2 to l2
      eager
      do { push_back(q, val, size); }

    invariant inv1 at l1 provided (t <= deadline)
    invariant inv2 at l2 provided (t <= miat)
  end


  // Generic TASTE protected interface type
  atom type TASTE_PI_Protected()
    export port Port lock()
    export port Port release()
 
    place l0, l1

    initial to l0

    on lock
      from l0 to l1
      eager

    on release
      from l1 to l0
      eager
  end

  // TASTE terminal functions
  atom type F1(int comp_deadline, int comp_wcet)
    data int v

    // Clocks generated for the protected required interface
    clock F1_comp_wait unit millisecond   // clock enforcing the time of the operation call
    data float F1_comp_time               // variable containing the computed time of function execution
    clock F1_comp_t unit millisecond      // clock measuring the total time since lock is demanded

    export port Port F1_step_in()
    export port Port F1_step_return()

    // Translation of the protected required interface
    export port Port F1_comp_lock()
    export port Port F1_comp_release()

    port Port silent()

    place Wait, l1, l2, l3, l4, l5, l6, l7

    // Definition of state Wait

    initial to Wait
      do { v = 0; }

    on F1_step_in
      from Wait to l1
      eager

    // Translation of the comp call (required protected interface)

    // Set clock measuring the "total" time to 0
    internal
      from l1 to l2
      do { F1_comp_t = 0; }

    // Lock the "protected object"
    on F1_comp_lock
      from l2 to l3
      eager

    // Call the implemented function wrapper (use the name of the provided protected interface to which it is connected)
    // This function is a wrapper of the implemented function and returns the execution time
    // Set the clock enforcing the computed execution time to 0
    on silent
      from l3 to l4
      eager
      do { F1_comp_wait = 0; F1_comp_time = F3_comp_wrap(v); }

    // Wait for the amount of the computed execution time
    on silent
      from l4 to l5
      provided (F1_comp_wait == F1_comp_time)
      eager

    // Release the protected object and reset clock
    on F1_comp_release
      from l5 to l6
      eager
      do { F1_comp_t = 0; }
  
    // End of translation of comp call

    on silent
      from l6 to l7
      eager
      do { v = v + 2; }

    on F1_step_return
      from l7 to Wait
      eager

    invariant inv2 at l2 provided (F1_comp_t <= comp_deadline)
    invariant inv3 at l3 provided (F1_comp_t <= comp_deadline)
    invariant inv4 at l4 provided (F1_comp_wait <= comp_wcet && F1_comp_t <= comp_deadline)
    invariant inv5 at l5 provided (F1_comp_t <= comp_deadline)

  end

  atom type F2(int comp_deadline, int comp_wcet)
    data int v

    // Clocks generated for the protected required interface
    clock F2_comp_wait unit millisecond   // clock enforcing the time of the operation call
    data float F2_comp_time               // variable containing the computed time of function execution
    clock F2_comp_t unit millisecond      // clock measuring the total time since lock is demanded

    export port Port F2_step_in()
    export port Port F2_step_return()

    // Translation of the protected required interface
    export port Port F2_comp_lock()
    export port Port F2_comp_release()

    port Port silent()

    place Wait, l1, l2, l3, l4, l5, l6, l7

    // Definition of state Wait

    initial to Wait
      do { v = 1; }

    on F2_step_in
      from Wait to l1
      eager

    // Translation of the comp call (required protected interface)

    // Set clock measuring the "total" time to 0
    internal
      from l1 to l2
      do { F2_comp_t = 0; }

    // Lock the "protected object"
    on F2_comp_lock
      from l2 to l3
      eager

    // Call the implemented function wrapper (use the name of the provided protected interface to which it is connected)
    // This function is a wrapper of the implemented function and returns the execution time
    // Set the clock enforcing the computed execution time to 0
    on silent
      from l3 to l4
      eager
      do { F2_comp_wait = 0; F2_comp_time = F3_comp_wrap(v); }

    // Wait for the amount of the computed execution time
    on silent
      from l4 to l5
      provided (F2_comp_wait == F2_comp_time)
      eager

    // Release the protected object and reset clock
    on F2_comp_release
      from l5 to l6
      eager
      do { F2_comp_t = 0; }
  
    // End of translation of comp call

    on silent
      from l6 to l7
      eager
      do { v = v + 2; }

    on F2_step_return
      from l7 to Wait
      eager

    invariant inv2 at l2 provided (F2_comp_t <= comp_deadline)
    invariant inv3 at l3 provided (F2_comp_t <= comp_deadline)
    invariant inv4 at l4 provided (F2_comp_wait <= comp_wcet && F2_comp_t <= comp_deadline)
    invariant inv5 at l5 provided (F2_comp_t <= comp_deadline)

  end

  // Translation of a TASTE function written in C
  // Several BIP components are generated: one component handling all sporadic interfaces and one component for each protected interface
  // Component handling the sporadic interfaces
  atom type F3()

    // The following attributes handle the execution time of the sporadic function implementation
    // They can be generated even if they are not used
    clock F3_comp_wait unit millisecond
    data float F3_comp_time

    port Port silent()

    place Wait

    // Initialization of C variables called here
    initial to Wait
      do { F3_startup_wrap(); }

  end
  
  // The components for each protected interface are of type TASTE_PI_Protected, they are just to be initialized


  // Compound types
  compound type Syst()
    component F1 F1(50,10)
    component TASTE_PI_Cyclic F1_step(2000,1000)

    component F2 F2(50,10)
    component TASTE_PI_Cyclic F2_step(2000,1000)

    component F3 F3()
    component TASTE_PI_Protected F3_comp()

    connector Sync F1_step2F1(F1_step.sig_out,F1.F1_step_in)
    connector Sync F12F1_step(F1.F1_step_return,F1_step.sig_return)
    connector Sync F2_step2F2(F2_step.sig_out,F2.F2_step_in)
    connector Sync F22F2_step(F2.F2_step_return,F2_step.sig_return)
    connector Sync F12F3_comp_lock(F1.F1_comp_lock,F3_comp.lock)
    connector Sync F12F3_comp_release(F1.F1_comp_release,F3_comp.release)
    connector Sync F22F3_comp_lock(F2.F2_comp_lock,F3_comp.lock)
    connector Sync F22F3_comp_release(F2.F2_comp_release,F3_comp.release)
  end
end
