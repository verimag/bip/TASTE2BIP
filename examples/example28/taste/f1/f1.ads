-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Sync required interface "comp"
    procedure RI�comp(val: access asn1SccT_Int32);
    pragma import(C, RI�comp, "f1_RI_comp");
end f1;