/* User code: This file will not be overwritten by TASTE. */

#include "f3.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>

void f3_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
}

void f3_PI_comp(const asn1SccT_Int32 *IN_val)
{
    /* Write your code here! */
    
    int x = (int) *IN_val;
    
    if (x % 2 == 0){
        printf("Value received from F1: %d\n", x);
        printf("Start sleep (for F1)\n");
        sleep(2);
        printf("End sleep (for F1)\n");
    }
    else{
        printf("Value received from F2: %d\n", x);
        printf("Start sleep (for F2)\n");
        sleep(3);
        printf("End sleep (for F2)\n");
    }
}

