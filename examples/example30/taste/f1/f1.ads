-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Sync required interface "comp1"
    procedure RI�comp1(v: access asn1SccT_Int32);
    pragma import(C, RI�comp1, "f1_RI_comp1");
end f1;