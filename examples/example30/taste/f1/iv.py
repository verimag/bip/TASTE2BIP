#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['f1'] = {
    'name_with_case' : 'F1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'f1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f1']['interfaces']['step']['paramsInOrdered'] = []

functions['f1']['interfaces']['step']['paramsOutOrdered'] = []

functions['f1']['interfaces']['comp1'] = {
    'port_name': 'comp1',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f3',
    'calling_threads': {},
    'distant_name': 'comp1',
    'queue_size': 1
}

functions['f1']['interfaces']['comp1']['paramsInOrdered'] = []

functions['f1']['interfaces']['comp1']['paramsOutOrdered'] = ['v']

functions['f1']['interfaces']['comp1']['out']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example29/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp1',
    'param_direction': param_out
}

functions['f2'] = {
    'name_with_case' : 'F2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f2']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 3000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 150,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f2']['interfaces']['step']['paramsInOrdered'] = []

functions['f2']['interfaces']['step']['paramsOutOrdered'] = []

functions['f2']['interfaces']['comp2'] = {
    'port_name': 'comp2',
    'parent_fv': 'f2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f3',
    'calling_threads': {},
    'distant_name': 'comp2',
    'queue_size': 1
}

functions['f2']['interfaces']['comp2']['paramsInOrdered'] = []

functions['f2']['interfaces']['comp2']['paramsOutOrdered'] = ['v']

functions['f2']['interfaces']['comp2']['out']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example29/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp2',
    'param_direction': param_out
}

functions['f3'] = {
    'name_with_case' : 'F3',
    'runtime_nature': passive,
    'language': C,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f3']['interfaces']['comp1'] = {
    'port_name': 'comp1',
    'parent_fv': 'f3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f3']['interfaces']['comp1']['paramsInOrdered'] = []

functions['f3']['interfaces']['comp1']['paramsOutOrdered'] = ['v']

functions['f3']['interfaces']['comp1']['out']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example29/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp1',
    'param_direction': param_out
}

functions['f3']['interfaces']['comp2'] = {
    'port_name': 'comp2',
    'parent_fv': 'f3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 200,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f3']['interfaces']['comp2']['paramsInOrdered'] = []

functions['f3']['interfaces']['comp2']['paramsOutOrdered'] = ['v']

functions['f3']['interfaces']['comp2']['out']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example29/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp2',
    'param_direction': param_out
}
