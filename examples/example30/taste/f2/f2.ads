-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f2 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f2_step");
    --  Sync required interface "comp2"
    procedure RI�comp2(v: access asn1SccT_Int32);
    pragma import(C, RI�comp2, "f2_RI_comp2");
end f2;