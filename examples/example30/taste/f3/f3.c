/* User code: This file will not be overwritten by TASTE. */

#include "f3.h"

asn1SccT_Int32 c;

void f3_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
    c = 1;
}

void f3_PI_comp1(asn1SccT_Int32 *OUT_v)
{
    /* Write your code here! */
    *OUT_v = c;
    c = c + 1;
}

void f3_PI_comp2(asn1SccT_Int32 *OUT_v)
{
    /* Write your code here! */
    *OUT_v = c;
    c = c * 2;
}

