This example consists of:
- Three functions F1, F2, and F3
- F1 and F2 "communicate" with F3 via the comp protected interface and the print sporadic interface
- F1 calls comp at each activation step (cyclic interface step) with the value of a local variable v starting at 1, retrieves in an auxiliary local variable the result of the computation, sends a print request for this variable, and updates the local variable v with the value of the auxiliary variable
- F2 calls comp at each activation step (cyclic interface step) with the value of a local variable v starting at 1, retrieves in an auxiliary local variable the result of the computation, sends a print request for this variable, and updates the local variable v with the value of the auxiliary variable
- F3 is implemented in C. Upon the call of comp computes and "returns" the multiplication of the input value by 2. Upon the call of print, prints on standard output the value received as argument. 

To generate C code from the BIP model, first generate C code from the TASTE model by calling ./build-script.sh in the taste directory. The bipc.sh command links to a folder that is generated by TASTE. E.g.,
  cd taste
  ./build-script.sh
  cd ../bip
  ./run.sh
To be able to run BIP make sure that it is properly "installed" in the working terminal (run souce setup.sh in the BIP folder).
