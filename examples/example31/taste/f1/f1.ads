-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "f1_step");
    --  Required interface "print"
    procedure RI�print(v: access asn1SccT_Int32);
    pragma import(C, RI�print, "f1_RI_print");
    --  Sync required interface "comp"
    procedure RI�comp(x: access asn1SccT_Int32; y: access asn1SccT_Int32);
    pragma import(C, RI�comp, "f1_RI_comp");
end f1;