#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['f1'] = {
    'name_with_case' : 'F1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'f1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 500,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f1']['interfaces']['step']['paramsInOrdered'] = []

functions['f1']['interfaces']['step']['paramsOutOrdered'] = []

functions['f1']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f3',
    'calling_threads': {},
    'distant_name': 'comp',
    'queue_size': 1
}

functions['f1']['interfaces']['comp']['paramsInOrdered'] = ['x']

functions['f1']['interfaces']['comp']['paramsOutOrdered'] = ['y']

functions['f1']['interfaces']['comp']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['f1']['interfaces']['comp']['out']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_out
}

functions['f1']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'f1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f3',
    'calling_threads': {},
    'distant_name': 'print',
    'queue_size': 1
}

functions['f1']['interfaces']['print']['paramsInOrdered'] = ['v']

functions['f1']['interfaces']['print']['paramsOutOrdered'] = []

functions['f1']['interfaces']['print']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}

functions['f2'] = {
    'name_with_case' : 'F2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f2']['interfaces']['sync'] = {
    'port_name': 'sync',
    'parent_fv': 'f2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 700,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f2']['interfaces']['sync']['paramsInOrdered'] = []

functions['f2']['interfaces']['sync']['paramsOutOrdered'] = []

functions['f2']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'f2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f3',
    'calling_threads': {},
    'distant_name': 'comp',
    'queue_size': 1
}

functions['f2']['interfaces']['comp']['paramsInOrdered'] = ['x']

functions['f2']['interfaces']['comp']['paramsOutOrdered'] = ['y']

functions['f2']['interfaces']['comp']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['f2']['interfaces']['comp']['out']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_out
}

functions['f2']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'f2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'f3',
    'calling_threads': {},
    'distant_name': 'print',
    'queue_size': 1
}

functions['f2']['interfaces']['print']['paramsInOrdered'] = ['v']

functions['f2']['interfaces']['print']['paramsOutOrdered'] = []

functions['f2']['interfaces']['print']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}

functions['f3'] = {
    'name_with_case' : 'F3',
    'runtime_nature': thread,
    'language': C,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['f3']['interfaces']['comp'] = {
    'port_name': 'comp',
    'parent_fv': 'f3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['f3']['interfaces']['comp']['paramsInOrdered'] = ['x']

functions['f3']['interfaces']['comp']['paramsOutOrdered'] = ['y']

functions['f3']['interfaces']['comp']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_in
}

functions['f3']['interfaces']['comp']['out']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp',
    'param_direction': param_out
}

functions['f3']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'f3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 500,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 2
}

functions['f3']['interfaces']['print']['paramsInOrdered'] = ['v']

functions['f3']['interfaces']['print']['paramsOutOrdered'] = []

functions['f3']['interfaces']['print']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example30/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}
