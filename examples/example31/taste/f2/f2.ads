-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package f2 is
    --  Provided interface "sync"
    procedure sync;
    pragma Export(C, sync, "f2_sync");
    --  Required interface "print"
    procedure RI�print(v: access asn1SccT_Int32);
    pragma import(C, RI�print, "f2_RI_print");
    --  Sync required interface "comp"
    procedure RI�comp(x: access asn1SccT_Int32; y: access asn1SccT_Int32);
    pragma import(C, RI�comp, "f2_RI_comp");
end f2;