/* User code: This file will not be overwritten by TASTE. */

#include "f3.h"
#include <stdio.h>

void f3_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
}

void f3_PI_comp(const asn1SccT_Int32 *IN_x,
                asn1SccT_Int32 *OUT_y)
{
    /* Write your code here! */
    *OUT_y = (*IN_x) * 2;
}

void f3_PI_print(const asn1SccT_Int32 *IN_v)
{
    /* Write your code here! */
    printf("Received value: %d\n", (int) (*IN_v));
}

