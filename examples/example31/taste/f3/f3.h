/* This file was generated automatically: DO NOT MODIFY IT ! */

/* Declaration of the functions that have to be provided by the user */

#ifndef __USER_CODE_H_f3__
#define __USER_CODE_H_f3__

#include "C_ASN1_Types.h"

#ifdef __cplusplus
extern "C" {
#endif

void f3_startup();

void f3_PI_comp(const asn1SccT_Int32 *,
                asn1SccT_Int32 *);

void f3_PI_print(const asn1SccT_Int32 *);

#ifdef __cplusplus
}
#endif


#endif
