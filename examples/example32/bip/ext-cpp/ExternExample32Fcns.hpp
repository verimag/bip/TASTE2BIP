#ifndef TE_HPP
#define TE_HPP

#include <iostream>
#include <queue>
#include <chrono>
#include "C_ASN1_Types.h"
#include "b.h"

using namespace std;

// Queue and methods for Bool values
typedef queue<bool> queueBool;

int get_size(const queueBool q);
void push_back(queueBool &q, bool &val, const int &size);
bool pop_front(queueBool &q);
ostream &operator<<(ostream &os, queueBool q);
void write(const bool i);
void writeln(const bool i);

// Queue and methods for Int values
typedef queue<int> queueInt;

int get_size(const queueInt q);
void push_back(queueInt &q, int &val, const int &size);
int pop_front(queueInt &q);
ostream &operator<<(ostream &os, queueInt q);
void write(const int i);
void writeln(const int i);

// Queue and methods for Float values
typedef queue<double> queueFloat;

int get_size(const queueFloat q);
void push_back(queueFloat &q, double &val, const int &size);
double pop_front(queueFloat &q);
ostream &operator<<(ostream &os, queueFloat q);
void write(const double i);
void writeln(const double i);

// Queue and methods for String values
typedef queue<string> queueString;

int get_size(const queueString q);
void push_back(queueString &q, string &val, const int &size);
string pop_front(queueString &q);
ostream &operator<<(ostream &os, queueString q);
void write(const string i);
void writeln(const string i);
void write(const char* s);
void writeln(const char* s);


// Methods for C functions
void B_startup_wrap();
double B_incr_wrap(const int &x, int &y);
double B_multiply_wrap(const int &x, const int &y, int &z);
double B_print_wrap(const int &x);

#endif
