#!/bin/bash

rm -rf output

mkdir output

bipc.sh -I . -p Example32 -d "Syst()" --gencpp-output output --gencpp-ld-l rt --gencpp-cc-I $PWD/ext-cpp --gencpp-cc-I $PWD/../taste/b/ --gencpp-cc-I $PWD/../taste/binary.c/GlueAndBuild/glueb/ --gencpp-cc-I $PWD/../taste/b/dataview/

mkdir output/build

cd output/build

cmake ..

make

./system -l 100 --silent
