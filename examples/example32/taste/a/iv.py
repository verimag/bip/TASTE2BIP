#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['a'] = {
    'name_with_case' : 'A',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['a']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['step']['paramsInOrdered'] = []

functions['a']['interfaces']['step']['paramsOutOrdered'] = []

functions['a']['interfaces']['incr'] = {
    'port_name': 'incr',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'incr',
    'queue_size': 1
}

functions['a']['interfaces']['incr']['paramsInOrdered'] = ['x']

functions['a']['interfaces']['incr']['paramsOutOrdered'] = ['y']

functions['a']['interfaces']['incr']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'incr',
    'param_direction': param_in
}

functions['a']['interfaces']['incr']['out']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'incr',
    'param_direction': param_out
}

functions['a']['interfaces']['multiply'] = {
    'port_name': 'multiply',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'multiply',
    'queue_size': 1
}

functions['a']['interfaces']['multiply']['paramsInOrdered'] = ['x', 'y']

functions['a']['interfaces']['multiply']['paramsOutOrdered'] = ['z']

functions['a']['interfaces']['multiply']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'multiply',
    'param_direction': param_in
}

functions['a']['interfaces']['multiply']['in']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'multiply',
    'param_direction': param_in
}

functions['a']['interfaces']['multiply']['out']['z'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'multiply',
    'param_direction': param_out
}

functions['a']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'print',
    'queue_size': 1
}

functions['a']['interfaces']['print']['paramsInOrdered'] = ['x']

functions['a']['interfaces']['print']['paramsOutOrdered'] = []

functions['a']['interfaces']['print']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}

functions['b'] = {
    'name_with_case' : 'B',
    'runtime_nature': passive,
    'language': C,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['b']['interfaces']['incr'] = {
    'port_name': 'incr',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['incr']['paramsInOrdered'] = ['x']

functions['b']['interfaces']['incr']['paramsOutOrdered'] = ['y']

functions['b']['interfaces']['incr']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'incr',
    'param_direction': param_in
}

functions['b']['interfaces']['incr']['out']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'incr',
    'param_direction': param_out
}

functions['b']['interfaces']['multiply'] = {
    'port_name': 'multiply',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 20,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['multiply']['paramsInOrdered'] = ['x', 'y']

functions['b']['interfaces']['multiply']['paramsOutOrdered'] = ['z']

functions['b']['interfaces']['multiply']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'multiply',
    'param_direction': param_in
}

functions['b']['interfaces']['multiply']['in']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'multiply',
    'param_direction': param_in
}

functions['b']['interfaces']['multiply']['out']['z'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'multiply',
    'param_direction': param_out
}

functions['b']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 5,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['print']['paramsInOrdered'] = ['x']

functions['b']['interfaces']['print']['paramsOutOrdered'] = []

functions['b']['interfaces']['print']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example31/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}
