/* User code: This file will not be overwritten by TASTE. */

#include "b.h"
#include <stdio.h>

void b_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
}

void add(const asn1SccT_Int32 *IN_x,
         const asn1SccT_Int32 *IN_y,
         asn1SccT_Int32 *OUT_z)
{
    *OUT_z = *IN_x + *IN_y;
}

void b_PI_incr(const asn1SccT_Int32 *IN_x,
               asn1SccT_Int32 *OUT_y)
{
    /* Write your code here! */
    asn1SccT_Int32 factor = (asn1SccT_Int32) 1;
    add(IN_x, &factor, OUT_y);
}

void b_PI_multiply(const asn1SccT_Int32 *IN_x,
                   const asn1SccT_Int32 *IN_y,
                   asn1SccT_Int32 *OUT_z)
{
    /* Write your code here! */
    *OUT_z = (*IN_x) * (*IN_y);
}

void b_PI_print(const asn1SccT_Int32 *IN_x)
{
    /* Write your code here! */
    printf("The computed value is %d\n", (int) (*IN_x));
}

