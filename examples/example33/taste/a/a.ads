-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package a is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "a_step");
    --  Paramless required interface "up"
    procedure RI�up;
    pragma import(C, RI�up, "a_RI_up");
    --  Required interface "print"
    procedure RI�print(x: access asn1SccT_Int32);
    pragma import(C, RI�print, "a_RI_print");
end a;