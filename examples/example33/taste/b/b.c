/* User code: This file will not be overwritten by TASTE. */

#include "b.h"
#include <stdio.h>

asn1SccT_Int32 v;

void b_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
    v = 0;
}

void b_PI_step()
{
    /* Write your code here! */
    b_PI_print(&v);
}

void b_PI_up()
{
    /* Write your code here! */
    v = v + 1;
}

void b_PI_print(const asn1SccT_Int32 *IN_x)
{
    /* Write your code here! */
    printf("The value is %d\n", (int) (*IN_x));
}

