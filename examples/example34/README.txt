This example consists of:
- Three functions ProdA, ProdB, and Cons communicating via the sporadic interface comp
- ProdA sends periodically (cyclic provided interface step) via the comp request a tuple (owner, value), where owner=cA and value starts from 1 and increments at each activation
- ProdA sendB periodically (cyclic provided interface step) via the comp request a tuple (owner, value), where owner=cB and value starts from 1 and increments at each activation
- Cons receives the comp request and based on the sender value of the tuple cA or cB prints a corresponding message and the value


To compile the bip model the DataView.asn of the taste model must be copied under bip/ext-cpp and C code generated by the command:
asn1.exe -c -uPER -typePrefix asn1Scc DataView.asn

(This command is extracted from toolinst/bin/assert-builder-ocarina.py, lines 1747, 1749)

