#include "ExternExample34Fcns.hpp"

// Methods to print variables of predefined data types

// Methods for (queue of) Int

void write(const int i){
  cout<<i;
}

void writeln(const int i){
  cout<<i<<endl;
}


// Methods for (queue of) Float

void write(const double i){
  cout<<i;
}

void writeln(const double i){
  cout<<i<<endl;
}


// Methods for (queue of) String

void write(const char* s){
  cout<<s;
}

void writeln(const char* s){
  cout<<s<<endl;
}

// Methods for MyReal values

//void write(const MyReal i){
//  cout<<i;
//}

//void writeln(const MyReal i){
//  cout<<i<<endl;
//}

double get_double(const MyReal i){
  return i;
}

// Methods for asn1SccMyComp values

const MyComp get_cA(){
  return cA;
}

const MyComp get_cB(){
  return cB;
}

/*bool operator==(MyComp &s, MyComp &t){
  return(&s == &t);
}
*/

// Methods for asn1SccProd values
MyComp get_comp(const Prod &e){
  return e.comp;
}

MyReal get_value(const Prod &e){
  return e.value;
}

void set_comp(Prod &e, const MyComp &f){
  e.comp = f;
}

void set_value(Prod &e, const MyReal &f){
  e.value = f;
}

ostream &operator<<(ostream &os, Prod q){
  os<<get_comp(q)<<" "<<get_value(q);
  return os;
}

// Methods for (queue of) Prod

int get_size(const queueProd q){
  return q.size();
}

void push_back(queueProd &q, const Prod &p, const int &size){
  if (get_size(q) < size) {
    q.push(p);
    //cout<<"Queuing: "<<val<<endl;
  }
  else {
    //cout<<"Queue full!"<<endl;
  }
}

Prod pop_front(queueProd &q){
  Prod p;

  if (get_size(q) > 0) {
    p = q.front();
    q.pop();
    //cout<<"Popping: "<<val<<endl;
  }
  else {
    //cout<<"Queue empty!"<<endl;
  }

  return p;
}

ostream &operator<<(ostream &os, queueProd q){
  queueProd tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}

