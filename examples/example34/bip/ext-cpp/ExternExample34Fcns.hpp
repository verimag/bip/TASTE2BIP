#ifndef TE_HPP
#define TE_HPP

#include <iostream>
#include <queue>
#include <chrono>

#include "DataView.h"

using namespace std;

void write(const int i);
void writeln(const int i);
void write(const double i);
void writeln(const double i);
void write(const char* s);
void writeln(const char* s);


// Methods for MyReal values
//void write(const MyReal i);
//void writeln(const MyReal i);
double get_double(const MyReal i);

// Methods for asn1SccMyComp values
const MyComp get_cA();
const MyComp get_cB();
//bool operator==(asn1SccMyComp &s, asn1SccMyComp &t);

// Methods for asn1SccProd values
MyComp get_comp(const Prod &e);
MyReal get_value(const Prod &e);
void set_comp(Prod &e, const MyComp &f);
void set_value(Prod &e, const MyReal &f);
ostream &operator<<(ostream &os, Prod q);

// Queue and methods for asn1SccProd values
typedef queue<Prod> queueProd;

int get_size(const queueProd q);
void push_back(queueProd &q, const Prod &p, const int &size);
Prod pop_front(queueProd &q);
ostream &operator<<(ostream &os, queueProd q);

#endif
