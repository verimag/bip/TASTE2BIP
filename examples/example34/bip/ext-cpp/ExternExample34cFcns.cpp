#include "ExternExample34cFcns.hpp"

// Methods to print variables of predefined data types

// Methods for (queue of) Int

void write(const int i){
  cout<<i;
}

void writeln(const int i){
  cout<<i<<endl;
}


// Methods for (queue of) Float

void write(const double i){
  cout<<i;
}

void writeln(const double i){
  cout<<i<<endl;
}


// Methods for (queue of) String

void write(const char* s){
  cout<<s;
}

void writeln(const char* s){
  cout<<s<<endl;
}


// Methods for (queue of) Prod

ostream &operator<<(ostream &os, Prod q){
  os<<q.comp<<" "<<q.value;
  return os;
}

int get_size(const queueProd q){
  return q.size();
}

void push_back(queueProd &q, const int &comp, const double &value, const int &size){
  Prod p;
  p.comp = comp;
  p.value = value;
  if (get_size(q) < size) {
    q.push(p);
    //cout<<"Queuing: "<<val<<endl;
  }
  else {
    //cout<<"Queue full!"<<endl;
  }
}

void pop_front(queueProd &q, int &comp, double &value){
  Prod p;

  if (get_size(q) > 0) {
    p = q.front();
    q.pop();
    comp = p.comp;
    value = p.value;
    //cout<<"Popping: "<<val<<endl;
  }
  else {
    //cout<<"Queue empty!"<<endl;
  }
}

ostream &operator<<(ostream &os, queueProd q){
  queueProd tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}

