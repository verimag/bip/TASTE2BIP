#ifndef TE_HPP
#define TE_HPP

#include <iostream>
#include <queue>
#include <chrono>

using namespace std;

void write(const int i);
void writeln(const int i);
void write(const double i);
void writeln(const double i);
void write(const char* s);
void writeln(const char* s);

// Prod definition
typedef struct {
    int comp;
    double value;
} Prod;
ostream &operator<<(ostream &os, Prod q);

typedef queue<Prod> queueProd;

int get_size(const queueProd q);
void push_back(queueProd &q, const int &comp, const double &value, const int &size);
void pop_front(queueProd &q, int &comp, double &value);
ostream &operator<<(ostream &os, queueProd q);


#endif
