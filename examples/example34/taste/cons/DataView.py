#!/usr/bin/env python
# ASN.1 Data model
asn1Files = []
asn1Modules = []
exportedTypes = {}
exportedVariables = {}
importedModules = {}
types = {}
variables = {}
asn1Files.append("./dataview-uniq.asn")
asn1Modules.append("TASTE-BasicTypes")
exportedTypes["TASTE-BasicTypes"] = ["T-Int32", "T-UInt32", "T-Int8", "T-UInt8", "T-Boolean"]
exportedVariables["TASTE-BasicTypes"] = []
importedModules["TASTE-BasicTypes"] = []

types["T-Int32"] = type("T-Int32", (object,), {
    "Line": 6, "CharPositionInLine": 0, "type": type("T-Int32_type", (object,), {
        "Line": 6, "CharPositionInLine": 13, "kind": "IntegerType", "Min": "-2147483648", "Max": "2147483647"
    })
})

types["T-UInt32"] = type("T-UInt32", (object,), {
    "Line": 8, "CharPositionInLine": 0, "type": type("T-UInt32_type", (object,), {
        "Line": 8, "CharPositionInLine": 13, "kind": "IntegerType", "Min": "0", "Max": "4294967295"
    })
})

types["T-Int8"] = type("T-Int8", (object,), {
    "Line": 10, "CharPositionInLine": 0, "type": type("T-Int8_type", (object,), {
        "Line": 10, "CharPositionInLine": 11, "kind": "IntegerType", "Min": "-128", "Max": "127"
    })
})

types["T-UInt8"] = type("T-UInt8", (object,), {
    "Line": 12, "CharPositionInLine": 0, "type": type("T-UInt8_type", (object,), {
        "Line": 12, "CharPositionInLine": 12, "kind": "IntegerType", "Min": "0", "Max": "255"
    })
})

types["T-Boolean"] = type("T-Boolean", (object,), {
    "Line": 14, "CharPositionInLine": 0, "type": type("T-Boolean_type", (object,), {
        "Line": 14, "CharPositionInLine": 14, "kind": "BooleanType"
    })
})



asn1Modules.append("TASTE-Dataview")
exportedTypes["TASTE-Dataview"] = ["MyReal", "MyComp", "Prod", "T-Int32", "T-UInt32", "T-Int8", "T-UInt8", "T-Boolean"]
exportedVariables["TASTE-Dataview"] = []
importedModules["TASTE-Dataview"] = [{"TASTE-BasicTypes":{"ImportedTypes": ["T-Int32","T-UInt32","T-Int8","T-UInt8","T-Boolean"], "ImportedVariables": []}}]

types["MyReal"] = type("MyReal", (object,), {
    "Line": 25, "CharPositionInLine": 0, "type": type("MyReal_type", (object,), {
        "Line": 25, "CharPositionInLine": 16, "kind": "RealType", "Min": "0.00000000000000000000E+000", "Max": "1.00000000000000000000E+002"
    })
})

types["MyComp"] = type("MyComp", (object,), {
    "Line": 48, "CharPositionInLine": 0, "type": type("MyComp_type", (object,), {
        "Line": 48, "CharPositionInLine": 11, "kind": "EnumeratedType", "Extensible": "False", "ValuesAutoCalculated": "False", "EnumValues": {
            "cA": type("cA", (object,), {
                "IntValue": 0, "Line": 48, "CharPositionInLine": 24, "EnumID": "cA"
            }),
            "cB": type("cB", (object,), {
                "IntValue": 1, "Line": 48, "CharPositionInLine": 28, "EnumID": "cB"
            })
        }
    })
})

types["Prod"] = type("Prod", (object,), {
    "Line": 50, "CharPositionInLine": 0, "type": type("Prod_type", (object,), {
        "Line": 50, "CharPositionInLine": 9, "kind": "SequenceType", "Children": {
            "comp": type("comp", (object,), {
                "Optional": "False", "Line": 51, "CharPositionInLine": 2, "type": type("comp_type", (object,), {
                    "Line": 51, "CharPositionInLine": 7, "kind": "ReferenceType", "ReferencedTypeName": "MyComp"
                })
            }),
            "value": type("value", (object,), {
                "Optional": "False", "Line": 52, "CharPositionInLine": 2, "type": type("value_type", (object,), {
                    "Line": 52, "CharPositionInLine": 8, "kind": "ReferenceType", "ReferencedTypeName": "MyReal", "Min": "0", "Max": "100"
                })
            })
        }
    })
})


