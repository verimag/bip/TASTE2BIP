-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package proda is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "proda_step");
    --  Required interface "comp"
    procedure RI�comp(val: access asn1SccProd);
    pragma import(C, RI�comp, "proda_RI_comp");
end proda;