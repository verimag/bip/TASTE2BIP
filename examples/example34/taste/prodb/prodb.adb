-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body prodb is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        elem : aliased asn1SccProd;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 2;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when wait =>
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- elem ! comp := cB (11,13)
                        ctxt.elem.comp := asn1ScccB;
                        -- elem ! value := 1.0 (12,0)
                        ctxt.elem.value := 1.0;
                        -- NEXT_STATE Wait (14,18) at 271, 121
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('The value sent by ProdB is ', elem!value) (20,17)
                        Put("The value sent by ProdB is ");
                        Put(Long_Float'Image(ctxt.elem.value));
                        New_Line;
                        -- comp(elem) (22,19)
                        RI�comp(ctxt.elem'Access);
                        -- elem!value := elem!value + 1.0 (24,17)
                        ctxt.elem.value := (ctxt.elem.value + 1.0);
                        -- NEXT_STATE Wait (26,22) at 507, 285
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end prodb;