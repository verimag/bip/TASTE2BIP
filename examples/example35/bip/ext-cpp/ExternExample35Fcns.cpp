#include "ExternExample35Fcns.hpp"

void write(const bool i){
  cout<<i;
}

void writeln(const bool i){
  cout<<i<<endl;
}

void write(const int i){
  cout<<i;
}

void writeln(const int i){
  cout<<i<<endl;
}

void write(const double i){
  cout<<i;
}

void writeln(const double i){
  cout<<i<<endl;
}

void write(const char* s){
  cout<<s;
}

void writeln(const char* s){
  cout<<s<<endl;
}



// Methods for (queue of) Vector3d

int get_elem(const Vector3d &v, const int &pos){
  return v.arr[pos];
}

void set_elem(Vector3d &v, const int &pos, const int &val){
  v.arr[pos]=val;
  if (pos > v.nCount) v.nCount=pos;
}

ostream &operator<<(ostream &os, const Vector3d& q){
  for (int i=0; i<=q.nCount; i++){
    os<<q.arr[i]<<" ";
  }
  return os;
}


int get_size(const queueVector3d& q){
  return q.size();
}

void push_back(queueVector3d &q, Vector3d &val, const int &size){
  if (get_size(q) < size) {
    q.push(val);
    cout<<"Queuing: "<<val<<endl;
  }
  else {
    cout<<"Queue full!"<<endl;
  }
}

Vector3d pop_front(queueVector3d &q){
  Vector3d val;

  if (get_size(q) > 0) {
    val = q.front();
    q.pop();
    cout<<"Popping: "<<val<<endl;
  }
  else {
    cout<<"Queue empty!"<<endl;
  }

  return val;
}

ostream &operator<<(ostream &os, queueVector3d q){
  queueVector3d tmp(q); 
  while (!tmp.empty()) {
    os<<tmp.front()<<" ";
    tmp.pop();
  }
  return os;
}



