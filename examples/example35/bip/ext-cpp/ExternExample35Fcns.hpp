#ifndef TE_HPP
#define TE_HPP

#include <iostream>
#include <queue>
#include <chrono>
#include "DataView.h"

using namespace std;


void write(const bool i);
void writeln(const bool i);
void write(const int i);
void writeln(const int i);
void write(const double i);
void writeln(const double i);
void write(const char* s);
void writeln(const char* s);

// Queue and methods for String values

int get_elem(const Vector3d& v, const int& pos);
void set_elem(Vector3d& v, const int& pos, const int& val);
ostream &operator<<(ostream &os, const Vector3d& q);

typedef queue<Vector3d> queueVector3d;

int get_size(const queueVector3d& q);
void push_back(queueVector3d &q, Vector3d &val, const int &size);
Vector3d pop_front(queueVector3d &q);
ostream &operator<<(ostream &os, queueVector3d q);



#endif
