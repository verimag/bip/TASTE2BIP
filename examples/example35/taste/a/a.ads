-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package a is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "a_step");
    --  Required interface "sort"
    procedure RI�sort(v: access asn1SccVector3d);
    pragma import(C, RI�sort, "a_RI_sort");
end a;