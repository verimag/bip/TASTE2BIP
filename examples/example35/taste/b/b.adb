-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body b is
    type States is (wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        aux : aliased asn1SccT_Int32;
        v : aliased asn1SccVector3d;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 2;
    procedure runTransition(Id: Integer);
    procedure sort(v: access asn1SccVector3d) is
        begin
            case ctxt.state is
                when wait =>
                    ctxt.v := v.all;
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end sort;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (12,18) at 320, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- DECISION v(0) > v(2) (18,26)
                        -- ANSWER true (20,17)
                        if ((ctxt.v.Data(1) > ctxt.v.Data(3))) = true then
                            -- aux := v(0) (22,25)
                            ctxt.aux := Asn1Int(ctxt.v.Data(1));
                            -- v(0) := v(2) (23,0)
                            ctxt.v.Data(1) := Asn1Int(ctxt.v.Data(3));
                            -- v(2) := aux (24,0)
                            ctxt.v.Data(3) := Asn1Int(ctxt.aux);
                            -- writeln('Sorted vector', v(0),' ',v(1),' ',v(2)) (26,25)
                            Put("Sorted vector");
                            Put(Asn1Int'Image(ctxt.v.Data(1)));
                            Put(" ");
                            Put(Asn1Int'Image(ctxt.v.Data(2)));
                            Put(" ");
                            Put(Asn1Int'Image(ctxt.v.Data(3)));
                            New_Line;
                            -- NEXT_STATE Wait (28,30) at 325, 356
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                            -- ANSWER false (30,17)
                        elsif ((ctxt.v.Data(1) > ctxt.v.Data(3))) = false then
                            -- writeln('Sorted vector', v(0),' ',v(1),' ',v(2)) (32,25)
                            Put("Sorted vector");
                            Put(Asn1Int'Image(ctxt.v.Data(1)));
                            Put(" ");
                            Put(Asn1Int'Image(ctxt.v.Data(2)));
                            Put(" ");
                            Put(Asn1Int'Image(ctxt.v.Data(3)));
                            New_Line;
                            -- NEXT_STATE Wait (34,30) at 612, 295
                            trId := -1;
                            ctxt.state := Wait;
                            goto next_transition;
                        end if;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                null;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end b;