-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_Dataview;
use TASTE_Dataview;
with TASTE_BasicTypes;
use TASTE_BasicTypes;
with adaasn1rtl;
use adaasn1rtl;



package b is
    --  Provided interface "sort"
    procedure sort(v: access asn1SccVector3d);
    pragma Export(C, sort, "b_sort");
end b;