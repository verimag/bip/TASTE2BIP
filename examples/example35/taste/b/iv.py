#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['a'] = {
    'name_with_case' : 'A',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['a']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 20,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['step']['paramsInOrdered'] = []

functions['a']['interfaces']['step']['paramsOutOrdered'] = []

functions['a']['interfaces']['sort'] = {
    'port_name': 'sort',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'sort',
    'queue_size': 1
}

functions['a']['interfaces']['sort']['paramsInOrdered'] = ['v']

functions['a']['interfaces']['sort']['paramsOutOrdered'] = []

functions['a']['interfaces']['sort']['in']['v'] = {
    'type': 'Vector3d',
    'asn1_module': 'TASTE_Dataview',
    'basic_type': sequenceof,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'sort',
    'param_direction': param_in
}

functions['b'] = {
    'name_with_case' : 'B',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['b']['interfaces']['sort'] = {
    'port_name': 'sort',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['sort']['paramsInOrdered'] = ['v']

functions['b']['interfaces']['sort']['paramsOutOrdered'] = []

functions['b']['interfaces']['sort']['in']['v'] = {
    'type': 'Vector3d',
    'asn1_module': 'TASTE_Dataview',
    'basic_type': sequenceof,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'sort',
    'param_direction': param_in
}
