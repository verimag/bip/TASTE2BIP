/* User code: This file will not be overwritten by TASTE. */

#include "b.h"

void b_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
}

void b_PI_incr(const asn1SccT_Int32 *IN_x,
               asn1SccT_Int32 *OUT_y)
{
    /* Write your code here! */
    (*OUT_y) = (*IN_x) + 1;
}

void b_PI_mult(const asn1SccT_Int32 *IN_x,
               const asn1SccT_Int32 *IN_y,
               asn1SccT_Int32 *OUT_z)
{
    /* Write your code here! */
    (*OUT_z) = (*IN_x) * (*IN_y);
}

