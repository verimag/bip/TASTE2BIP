/* This file was generated automatically: DO NOT MODIFY IT ! */

/* Declaration of the functions that have to be provided by the user */

#ifndef __USER_CODE_H_b__
#define __USER_CODE_H_b__

#include "C_ASN1_Types.h"

#ifdef __cplusplus
extern "C" {
#endif

void b_startup();

void b_PI_incr(const asn1SccT_Int32 *,
               asn1SccT_Int32 *);

void b_PI_mult(const asn1SccT_Int32 *,
               const asn1SccT_Int32 *,
               asn1SccT_Int32 *);

#ifdef __cplusplus
}
#endif


#endif
