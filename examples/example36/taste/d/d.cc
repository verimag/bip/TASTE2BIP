/* User code: This file will not be overwritten by TASTE. */

#include "d.h"

#include <iostream>
#include <string>


void d_startup()
{
    /* Write your initialization code here,
       but do not make any call to a required interface. */
}

void d_PI_print(const asn1SccT_Int32 *IN_x)
{
    /* Write your code here! */
    std::cout<<"x="<<(*IN_x)<<std::endl;
}

