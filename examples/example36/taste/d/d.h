/* This file was generated automatically: DO NOT MODIFY IT ! */

/* Declaration of the functions that have to be provided by the user */

#ifndef __USER_CODE_H_d__
#define __USER_CODE_H_d__

#include "C_ASN1_Types.h"

#ifdef __cplusplus
extern "C" {
#endif

void d_startup();

void d_PI_print(const asn1SccT_Int32 *);

#ifdef __cplusplus
}
#endif


#endif
