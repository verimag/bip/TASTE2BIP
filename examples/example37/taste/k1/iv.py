#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['k1'] = {
    'name_with_case' : 'K1',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['k1']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'k1',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 1000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 50,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k1']['interfaces']['step']['paramsInOrdered'] = []

functions['k1']['interfaces']['step']['paramsOutOrdered'] = []

functions['k1']['interfaces']['comp1'] = {
    'port_name': 'comp1',
    'parent_fv': 'k1',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k3',
    'calling_threads': {},
    'distant_name': 'comp1',
    'queue_size': 1
}

functions['k1']['interfaces']['comp1']['paramsInOrdered'] = ['v']

functions['k1']['interfaces']['comp1']['paramsOutOrdered'] = []

functions['k1']['interfaces']['comp1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp1',
    'param_direction': param_in
}

functions['k2'] = {
    'name_with_case' : 'K2',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['k2']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'k2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 400,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 2,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k2']['interfaces']['step']['paramsInOrdered'] = []

functions['k2']['interfaces']['step']['paramsOutOrdered'] = []

functions['k2']['interfaces']['res'] = {
    'port_name': 'res',
    'parent_fv': 'k2',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 500,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k2']['interfaces']['res']['paramsInOrdered'] = ['v']

functions['k2']['interfaces']['res']['paramsOutOrdered'] = []

functions['k2']['interfaces']['res']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'res',
    'param_direction': param_in
}

functions['k2']['interfaces']['comp1'] = {
    'port_name': 'comp1',
    'parent_fv': 'k2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k3',
    'calling_threads': {},
    'distant_name': 'comp1',
    'queue_size': 1
}

functions['k2']['interfaces']['comp1']['paramsInOrdered'] = ['v']

functions['k2']['interfaces']['comp1']['paramsOutOrdered'] = []

functions['k2']['interfaces']['comp1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp1',
    'param_direction': param_in
}

functions['k2']['interfaces']['comp2'] = {
    'port_name': 'comp2',
    'parent_fv': 'k2',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k3',
    'calling_threads': {},
    'distant_name': 'comp2',
    'queue_size': 1
}

functions['k2']['interfaces']['comp2']['paramsInOrdered'] = ['v']

functions['k2']['interfaces']['comp2']['paramsOutOrdered'] = []

functions['k2']['interfaces']['comp2']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp2',
    'param_direction': param_in
}

functions['k3'] = {
    'name_with_case' : 'K3',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['k3']['interfaces']['comp1'] = {
    'port_name': 'comp1',
    'parent_fv': 'k3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 100,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 2
}

functions['k3']['interfaces']['comp1']['paramsInOrdered'] = ['v']

functions['k3']['interfaces']['comp1']['paramsOutOrdered'] = []

functions['k3']['interfaces']['comp1']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp1',
    'param_direction': param_in
}

functions['k3']['interfaces']['comp2'] = {
    'port_name': 'comp2',
    'parent_fv': 'k3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': variator,
    'period': 200,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 10,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k3']['interfaces']['comp2']['paramsInOrdered'] = ['v']

functions['k3']['interfaces']['comp2']['paramsOutOrdered'] = []

functions['k3']['interfaces']['comp2']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'comp2',
    'param_direction': param_in
}

functions['k3']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'k3',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 200,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 1,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['k3']['interfaces']['step']['paramsInOrdered'] = []

functions['k3']['interfaces']['step']['paramsOutOrdered'] = []

functions['k3']['interfaces']['res'] = {
    'port_name': 'res',
    'parent_fv': 'k3',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': sporadic,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'k2',
    'calling_threads': {},
    'distant_name': 'res',
    'queue_size': 1
}

functions['k3']['interfaces']['res']['paramsInOrdered'] = ['v']

functions['k3']['interfaces']['res']['paramsOutOrdered'] = []

functions['k3']['interfaces']['res']['in']['v'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example37/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'res',
    'param_direction': param_in
}
