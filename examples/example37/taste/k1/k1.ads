-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package k1 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "k1_step");
    --  Required interface "comp1"
    procedure RI�comp1(v: access asn1SccT_Int32);
    pragma import(C, RI�comp1, "k1_RI_comp1");
end k1;