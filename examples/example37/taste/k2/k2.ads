-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package k2 is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "k2_step");
    --  Provided interface "res"
    procedure res(v: access asn1SccT_Int32);
    pragma Export(C, res, "k2_res");
    --  Required interface "comp1"
    procedure RI�comp1(v: access asn1SccT_Int32);
    pragma import(C, RI�comp1, "k2_RI_comp1");
    --  Required interface "comp2"
    procedure RI�comp2(v: access asn1SccT_Int32);
    pragma import(C, RI�comp2, "k2_RI_comp2");
end k2;