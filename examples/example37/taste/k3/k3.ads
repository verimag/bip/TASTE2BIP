-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package k3 is
    --  Provided interface "comp1"
    procedure comp1(v: access asn1SccT_Int32);
    pragma Export(C, comp1, "k3_comp1");
    --  Provided interface "comp2"
    procedure comp2(v: access asn1SccT_Int32);
    pragma Export(C, comp2, "k3_comp2");
    --  Required interface "res"
    procedure RI�res(v: access asn1SccT_Int32);
    pragma import(C, RI�res, "k3_RI_res");
end k3;