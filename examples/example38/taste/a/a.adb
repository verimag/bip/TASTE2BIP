-- This file was generated automatically: DO NOT MODIFY IT !

with System.IO;
use System.IO;

with Ada.Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;

with Interfaces;
use Interfaces;

package body a is
    type States is (print, multiply, wait);
    type ctxt_Ty is
        record
        state : States;
        initDone : Boolean := False;
        y : aliased asn1SccT_Int32;
        x : aliased asn1SccT_Int32 := 0;
        m : aliased asn1SccT_Int32 := 2;
    end record;
    ctxt: aliased ctxt_Ty;
    CS_Only  : constant Integer := 5;
    procedure runTransition(Id: Integer);
    procedure step is
        begin
            case ctxt.state is
                when print =>
                    runTransition(CS_Only);
                when multiply =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(1);
                when others =>
                    runTransition(CS_Only);
            end case;
        end step;
        

    procedure sp is
        begin
            case ctxt.state is
                when print =>
                    runTransition(CS_Only);
                when multiply =>
                    runTransition(CS_Only);
                when wait =>
                    runTransition(2);
                when others =>
                    runTransition(CS_Only);
            end case;
        end sp;
        

    procedure runTransition(Id: Integer) is
        trId : Integer := Id;
        msgPending : aliased Asn1Boolean := True;
        begin
            while (trId /= -1) loop
                case trId is
                    when 0 =>
                        -- NEXT_STATE Wait (13,18) at 24, 60
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 1 =>
                        -- writeln('Increment x=',x) (19,17)
                        Put("Increment x=");
                        Put(Asn1Int'Image(ctxt.x));
                        New_Line;
                        -- incr(x,y) (21,17)
                        RI�incr(ctxt.x'Access, ctxt.y'Access);
                        -- NEXT_STATE Multiply (23,22) at 138, 229
                        trId := -1;
                        ctxt.state := Multiply;
                        goto next_transition;
                    when 2 =>
                        -- write('Print current ') (27,17)
                        Put("Print current ");
                        -- print(x) (29,17)
                        RI�print(ctxt.x'Access);
                        -- NEXT_STATE Wait (31,22) at 311, 229
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 3 =>
                        -- write('Print ') (38,17)
                        Put("Print ");
                        -- print(x) (40,17)
                        RI�print(ctxt.x'Access);
                        -- NEXT_STATE Wait (42,22) at 517, 460
                        trId := -1;
                        ctxt.state := Wait;
                        goto next_transition;
                    when 4 =>
                        -- writeln('Multiply x=',y,' by m=',m) (49,17)
                        Put("Multiply x=");
                        Put(Asn1Int'Image(ctxt.y));
                        Put(" by m=");
                        Put(Asn1Int'Image(ctxt.m));
                        New_Line;
                        -- mult(y,m,x) (51,17)
                        RI�mult(ctxt.y'Access, ctxt.m'Access, ctxt.x'Access);
                        -- NEXT_STATE Print (53,22) at 524, 241
                        trId := -1;
                        ctxt.state := Print;
                        goto next_transition;
                    when CS_Only =>
                        trId := -1;
                        goto next_transition;
                    when others =>
                        null;
                end case;
                <<next_transition>>
                --  Process continuous signals
                if ctxt.initDone then
                    Check_Queue(msgPending'access);
                end if;
                if not msgPending and trId = -1 and ctxt.state = print then
                    -- Priority 1
                    -- DECISION true (-1,-1)
                    -- ANSWER true (None,None)
                    if (true) = true then
                        trId := 3;
                    end if;
                end if;
                if not msgPending and trId = -1 and ctxt.state = multiply then
                    -- Priority 1
                    -- DECISION true (-1,-1)
                    -- ANSWER true (None,None)
                    if (true) = true then
                        trId := 4;
                    end if;
                end if;
            end loop;
        end runTransition;
        

    begin
        runTransition(0);
        ctxt.initDone := True;
end a;