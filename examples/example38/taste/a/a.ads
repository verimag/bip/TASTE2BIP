-- This file was generated automatically: DO NOT MODIFY IT !

with TASTE_BasicTypes;
use TASTE_BasicTypes;
with TASTE_Dataview;
use TASTE_Dataview;
with adaasn1rtl;
use adaasn1rtl;



package a is
    --  Provided interface "step"
    procedure step;
    pragma Export(C, step, "a_step");
    --  Provided interface "sp"
    procedure sp;
    pragma Export(C, sp, "a_sp");
    --  Sync required interface "incr"
    procedure RI�incr(x: access asn1SccT_Int32; y: access asn1SccT_Int32);
    pragma import(C, RI�incr, "a_RI_incr");
    --  Sync required interface "mult"
    procedure RI�mult(x: access asn1SccT_Int32; y: access asn1SccT_Int32; z: access asn1SccT_Int32);
    pragma import(C, RI�mult, "a_RI_mult");
    --  Sync required interface "print"
    procedure RI�print(x: access asn1SccT_Int32);
    pragma import(C, RI�print, "a_RI_print");
    procedure Check_Queue(res: access Asn1Boolean);
    pragma import(C, Check_Queue, "a_check_queue");
end a;