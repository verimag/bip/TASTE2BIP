#! /usr/bin/python

Ada, C, GUI, SIMULINK, VHDL, OG, RTDS, SYSTEM_C, SCADE6, VDM, CPP = range(11)
thread, passive, unknown = range(3)
PI, RI = range(2)
synch, asynch = range(2)
param_in, param_out = range(2)
UPER, NATIVE, ACN = range(3)
cyclic, sporadic, variator, protected, unprotected = range(5)
enumerated, sequenceof, sequence, set, setof, integer, boolean, real, choice, octetstring, string = range(11)
functions = {}

functions['a'] = {
    'name_with_case' : 'A',
    'runtime_nature': thread,
    'language': OG,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['a']['interfaces']['step'] = {
    'port_name': 'step',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 2000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 800,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['step']['paramsInOrdered'] = []

functions['a']['interfaces']['step']['paramsOutOrdered'] = []

functions['a']['interfaces']['sp'] = {
    'port_name': 'sp',
    'parent_fv': 'a',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': asynch,
    'rcm': cyclic,
    'period': 5000,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['a']['interfaces']['sp']['paramsInOrdered'] = []

functions['a']['interfaces']['sp']['paramsOutOrdered'] = []

functions['a']['interfaces']['mult'] = {
    'port_name': 'mult',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'b',
    'calling_threads': {},
    'distant_name': 'mult',
    'queue_size': 1
}

functions['a']['interfaces']['mult']['paramsInOrdered'] = ['x', 'y']

functions['a']['interfaces']['mult']['paramsOutOrdered'] = ['z']

functions['a']['interfaces']['mult']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'mult',
    'param_direction': param_in
}

functions['a']['interfaces']['mult']['in']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'mult',
    'param_direction': param_in
}

functions['a']['interfaces']['mult']['out']['z'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'mult',
    'param_direction': param_out
}

functions['a']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'a',
    'direction': RI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': '',
    'wcet_high': 0,
    'wcet_high_unit': '',
    'distant_fv': 'd',
    'calling_threads': {},
    'distant_name': 'print',
    'queue_size': 1
}

functions['a']['interfaces']['print']['paramsInOrdered'] = ['x']

functions['a']['interfaces']['print']['paramsOutOrdered'] = []

functions['a']['interfaces']['print']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}

functions['b'] = {
    'name_with_case' : 'B',
    'runtime_nature': passive,
    'language': CPP,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['b']['interfaces']['mult'] = {
    'port_name': 'mult',
    'parent_fv': 'b',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['b']['interfaces']['mult']['paramsInOrdered'] = ['x', 'y']

functions['b']['interfaces']['mult']['paramsOutOrdered'] = ['z']

functions['b']['interfaces']['mult']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'mult',
    'param_direction': param_in
}

functions['b']['interfaces']['mult']['in']['y'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'mult',
    'param_direction': param_in
}

functions['b']['interfaces']['mult']['out']['z'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'mult',
    'param_direction': param_out
}

functions['d'] = {
    'name_with_case' : 'D',
    'runtime_nature': passive,
    'language': CPP,
    'zipfile': '',
    'interfaces': {},
    'functional_states' : {}
}

functions['d']['interfaces']['print'] = {
    'port_name': 'print',
    'parent_fv': 'd',
    'direction': PI,
    'in': {},
    'out': {},
    'synchronism': synch,
    'rcm': protected,
    'period': 0,
    'wcet_low': 0,
    'wcet_low_unit': 'ms',
    'wcet_high': 100,
    'wcet_high_unit': 'ms',
    'distant_fv': '',
    'calling_threads': {},
    'distant_name': '',
    'queue_size': 1
}

functions['d']['interfaces']['print']['paramsInOrdered'] = ['x']

functions['d']['interfaces']['print']['paramsOutOrdered'] = []

functions['d']['interfaces']['print']['in']['x'] = {
    'type': 'T_Int32',
    'asn1_module': 'TASTE_BasicTypes',
    'basic_type': integer,
    'asn1_filename': '/home/taste/Documents/TASTE2BIP/examples/example38/taste/dataview-uniq.asn',
    'encoding': NATIVE,
    'interface': 'print',
    'param_direction': param_in
}
