isComponentType('interfaceview::FV::A','PUBLIC','PI_step','SUBPROGRAM','NIL','').
isComponentImplementation('interfaceview::FV::A','PUBLIC','PI_step','others','SUBPROGRAM','NIL','others','').
isFeature('ACCESS','interfaceview::IV','A','PI_step','PROVIDES','SUBPROGRAM','interfaceview::FV::A::PI_step.others','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::A','PI_step','NIL','NIL','Taste::Associated_Queue_Size','1','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_step','Taste::coordinates','"127242 66140"','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_step','Taste::RCMoperationKind','cyclic','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_step','Taste::RCMperiod','2000 ms','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_step','Taste::Deadline','1000 ms','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_step','Taste::InterfaceName','"step"','').
isProperty('NIL','=>','interfaceview::FV::A','PI_step','others','NIL','Compute_Execution_Time','0 ms .. 800 ms','').
isSubcomponent('interfaceview::IV','A','others','step_impl','SUBPROGRAM','interfaceview::FV::A::PI_step.others','NIL','NIL','').
isConnection('SUBPROGRAM ACCESS','interfaceview::IV','A','others','OpToPICnx_step_impl','step_impl','->','PI_step','NIL','').
isComponentType('interfaceview::FV::A','PUBLIC','PI_sp','SUBPROGRAM','NIL','').
isComponentImplementation('interfaceview::FV::A','PUBLIC','PI_sp','others','SUBPROGRAM','NIL','others','').
isFeature('ACCESS','interfaceview::IV','A','PI_sp','PROVIDES','SUBPROGRAM','interfaceview::FV::A::PI_sp.others','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::A','PI_sp','NIL','NIL','Taste::Associated_Queue_Size','1','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_sp','Taste::coordinates','"132754 66140"','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_sp','Taste::RCMoperationKind','cyclic','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_sp','Taste::RCMperiod','5000 ms','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_sp','Taste::Deadline','300 ms','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','PI_sp','Taste::InterfaceName','"sp"','').
isProperty('NIL','=>','interfaceview::FV::A','PI_sp','others','NIL','Compute_Execution_Time','0 ms .. 100 ms','').
isSubcomponent('interfaceview::IV','A','others','sp_impl','SUBPROGRAM','interfaceview::FV::A::PI_sp.others','NIL','NIL','').
isConnection('SUBPROGRAM ACCESS','interfaceview::IV','A','others','OpToPICnx_sp_impl','sp_impl','->','PI_sp','NIL','').
isConnection('SUBPROGRAM ACCESS','interfaceview::IV','interfaceview','others','B_PI_mult_A_RI_mult','B.PI_mult','->','A.RI_mult','NIL','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','others','B_PI_mult_A_RI_mult','Taste::coordinates','"128973 80313 128973 84328 123147 84328 123147 88344"','').
isComponentType('interfaceview::FV::A','PUBLIC','RI_mult','SUBPROGRAM','NIL','').
isComponentImplementation('interfaceview::FV::A','PUBLIC','RI_mult','others','SUBPROGRAM','NIL','others','').
isImportDeclaration('interfaceview::IV','PUBLIC','interfaceview::FV::B','').
isFeature('ACCESS','interfaceview::IV','A','RI_mult','REQUIRES','SUBPROGRAM','interfaceview::FV::B::PI_mult.others','NIL','NIL','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_mult','Taste::coordinates','"128973 80313"','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_mult','Taste::RCMoperationKind','any','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_mult','Taste::InterfaceName','"mult"','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_mult','Taste::labelInheritance','"true"','').
isFeature('PARAMETER','interfaceview::FV::A','RI_mult','x','IN','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::A','RI_mult','NIL','x','Taste::encoding','NATIVE','').
isFeature('PARAMETER','interfaceview::FV::A','RI_mult','y','IN','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::A','RI_mult','NIL','y','Taste::encoding','NATIVE','').
isFeature('PARAMETER','interfaceview::FV::A','RI_mult','z','OUT','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::A','RI_mult','NIL','z','Taste::encoding','NATIVE','').
isConnection('SUBPROGRAM ACCESS','interfaceview::IV','interfaceview','others','D_PI_print_A_RI_print','D.PI_print','->','A.RI_print','NIL','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','others','D_PI_print_A_RI_print','Taste::coordinates','"137319 77007 144407 77007 144407 88344"','').
isComponentType('interfaceview::FV::A','PUBLIC','RI_print','SUBPROGRAM','NIL','').
isComponentImplementation('interfaceview::FV::A','PUBLIC','RI_print','others','SUBPROGRAM','NIL','others','').
isImportDeclaration('interfaceview::IV','PUBLIC','interfaceview::FV::D','').
isFeature('ACCESS','interfaceview::IV','A','RI_print','REQUIRES','SUBPROGRAM','interfaceview::FV::D::PI_print.others','NIL','NIL','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_print','Taste::coordinates','"137319 77007"','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_print','Taste::RCMoperationKind','any','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_print','Taste::InterfaceName','"print"','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','RI_print','Taste::labelInheritance','"true"','').
isFeature('PARAMETER','interfaceview::FV::A','RI_print','x','IN','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::A','RI_print','NIL','x','Taste::encoding','NATIVE','').
isPackage('interfaceview::FV::A','PUBLIC','').
isComponentType('interfaceview::IV','PUBLIC','A','SYSTEM','NIL','').
isComponentImplementation('interfaceview::IV','PUBLIC','A','others','SYSTEM','NIL','others','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','NIL','Source_Language','(SDL)','').
isProperty('NIL','=>','interfaceview::IV','A','NIL','NIL','Taste::Active_Interfaces','any','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','others','A','Taste::coordinates','"123304 66140 137319 80313"','').
isSubcomponent('interfaceview::IV','interfaceview','others','A','SYSTEM','interfaceview::IV::A.others','NIL','NIL','').
isImportDeclaration('interfaceview::IV','PUBLIC','interfaceview::FV::A','').
isImportDeclaration('interfaceview::IV','PUBLIC','Taste','').
isImportDeclaration('interfaceview::FV::A','PUBLIC','Taste','').
isImportDeclaration('interfaceview::IV','PUBLIC','DataView','').
isImportDeclaration('interfaceview::FV::A','PUBLIC','DataView','').
isImportDeclaration('interfaceview::FV::A','PUBLIC','TASTE_IV_Properties','').
isImportDeclaration('interfaceview::IV','PUBLIC','TASTE_IV_Properties','').
isComponentType('interfaceview::FV::B','PUBLIC','PI_mult','SUBPROGRAM','NIL','').
isComponentImplementation('interfaceview::FV::B','PUBLIC','PI_mult','others','SUBPROGRAM','NIL','others','').
isFeature('ACCESS','interfaceview::IV','B','PI_mult','PROVIDES','SUBPROGRAM','interfaceview::FV::B::PI_mult.others','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::B','PI_mult','NIL','NIL','Taste::Associated_Queue_Size','1','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','PI_mult','Taste::coordinates','"123147 88344"','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','PI_mult','Taste::RCMoperationKind','protected','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','PI_mult','Taste::RCMperiod','0 ms','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','PI_mult','Taste::Deadline','200 ms','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','PI_mult','Taste::InterfaceName','"mult"','').
isFeature('PARAMETER','interfaceview::FV::B','PI_mult','x','IN','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::B','PI_mult','NIL','x','Taste::encoding','NATIVE','').
isFeature('PARAMETER','interfaceview::FV::B','PI_mult','y','IN','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::B','PI_mult','NIL','y','Taste::encoding','NATIVE','').
isFeature('PARAMETER','interfaceview::FV::B','PI_mult','z','OUT','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::B','PI_mult','NIL','z','Taste::encoding','NATIVE','').
isProperty('NIL','=>','interfaceview::FV::B','PI_mult','others','NIL','Compute_Execution_Time','0 ms .. 100 ms','').
isSubcomponent('interfaceview::IV','B','others','mult_impl','SUBPROGRAM','interfaceview::FV::B::PI_mult.others','NIL','NIL','').
isConnection('SUBPROGRAM ACCESS','interfaceview::IV','B','others','OpToPICnx_mult_impl','mult_impl','->','PI_mult','NIL','').
isPackage('interfaceview::FV::B','PUBLIC','').
isComponentType('interfaceview::IV','PUBLIC','B','SYSTEM','NIL','').
isComponentImplementation('interfaceview::IV','PUBLIC','B','others','SYSTEM','NIL','others','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','NIL','Source_Language','(CPP)','').
isProperty('NIL','=>','interfaceview::IV','B','NIL','NIL','Taste::Active_Interfaces','any','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','others','B','Taste::coordinates','"113383 88344 125981 99997"','').
isSubcomponent('interfaceview::IV','interfaceview','others','B','SYSTEM','interfaceview::IV::B.others','NIL','NIL','').
isImportDeclaration('interfaceview::FV::B','PUBLIC','Taste','').
isImportDeclaration('interfaceview::FV::B','PUBLIC','DataView','').
isImportDeclaration('interfaceview::FV::B','PUBLIC','TASTE_IV_Properties','').
isComponentType('interfaceview::FV::D','PUBLIC','PI_print','SUBPROGRAM','NIL','').
isComponentImplementation('interfaceview::FV::D','PUBLIC','PI_print','others','SUBPROGRAM','NIL','others','').
isFeature('ACCESS','interfaceview::IV','D','PI_print','PROVIDES','SUBPROGRAM','interfaceview::FV::D::PI_print.others','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::D','PI_print','NIL','NIL','Taste::Associated_Queue_Size','1','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','PI_print','Taste::coordinates','"144407 88344"','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','PI_print','Taste::RCMoperationKind','protected','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','PI_print','Taste::RCMperiod','0 ms','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','PI_print','Taste::Deadline','200 ms','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','PI_print','Taste::InterfaceName','"print"','').
isFeature('PARAMETER','interfaceview::FV::D','PI_print','x','IN','NIL','DataView::T_Int32','NIL','NIL','').
isProperty('NIL','=>','interfaceview::FV::D','PI_print','NIL','x','Taste::encoding','NATIVE','').
isProperty('NIL','=>','interfaceview::FV::D','PI_print','others','NIL','Compute_Execution_Time','0 ms .. 100 ms','').
isSubcomponent('interfaceview::IV','D','others','print_impl','SUBPROGRAM','interfaceview::FV::D::PI_print.others','NIL','NIL','').
isConnection('SUBPROGRAM ACCESS','interfaceview::IV','D','others','OpToPICnx_print_impl','print_impl','->','PI_print','NIL','').
isPackage('interfaceview::FV::D','PUBLIC','').
isComponentType('interfaceview::IV','PUBLIC','D','SYSTEM','NIL','').
isComponentImplementation('interfaceview::IV','PUBLIC','D','others','SYSTEM','NIL','others','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','NIL','Source_Language','(CPP)','').
isProperty('NIL','=>','interfaceview::IV','D','NIL','NIL','Taste::Active_Interfaces','any','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','others','D','Taste::coordinates','"136532 88344 148500 99683"','').
isSubcomponent('interfaceview::IV','interfaceview','others','D','SYSTEM','interfaceview::IV::D.others','NIL','NIL','').
isImportDeclaration('interfaceview::FV::D','PUBLIC','Taste','').
isImportDeclaration('interfaceview::FV::D','PUBLIC','DataView','').
isImportDeclaration('interfaceview::FV::D','PUBLIC','TASTE_IV_Properties','').
isProperty('_','_','_','_','_','_','LMP::Unparser_ID_Case','AsIs','').
isProperty('_','_','_','_','_','_','LMP::Unparser_Insert_Header','Yes','').
isPackage('interfaceview::IV','PUBLIC','').
isProperty('NIL','=>','interfaceview::IV','NIL','NIL','NIL','Taste::dataView','("DataView")','').
isProperty('NIL','=>','interfaceview::IV','NIL','NIL','NIL','Taste::dataViewPath','("DataView.aadl")','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','NIL','NIL','Taste::dataView','("DataView")','').
isProperty('NIL','=>','interfaceview::IV','interfaceview','NIL','NIL','Taste::dataViewPath','("DataView.aadl")','').
isVersion('AADL2.1','TASTE type interfaceview','','generated code: do not edit').
isProperty('NIL','=>','interfaceview::IV','NIL','NIL','NIL','Taste::coordinates','"0 0 297000 210000"','').
isProperty('NIL','=>','interfaceview::IV','NIL','NIL','NIL','Taste::version','"1.3"','').
isComponentType('interfaceview::IV','PUBLIC','interfaceview','SYSTEM','NIL','').
isComponentImplementation('interfaceview::IV','PUBLIC','interfaceview','others','SYSTEM','NIL','others','').

