\pdfminorversion=4
\documentclass[slidetop,10pt,mathserif]{beamer}

\usepackage[applemac]{inputenc}
\usepackage{xcolor}
\usepackage{tikz} 
\usetikzlibrary{shapes,arrows,shadows,positioning,calc,fit,backgrounds,decorations.pathreplacing,automata}
\usepackage{multirow}
\usepackage{makecell}
\usepackage{subfig}
\usepackage{progressbar}
%\DeclareGraphicsExtensions{.png,.pdf,.jpg}
  
\usetheme{CambridgeUS}
\usecolortheme{dolphin}
\setbeamertemplate{navigation symbols}{}
%\setbeamercovered{transparent}
\setbeamertemplate{headline}{}
\setbeamertemplate{footline}{}
\setbeamercolor{footline}{fg=black}
\setbeamerfont{footline}{size=\tiny, series=\bfseries}


\newcommand\Fontvi{\fontsize{8}{9}\selectfont}

\setbeamercolor{footlinecolor}{fg=black,bg=white}
\defbeamertemplate*{footline}{mytheme}
{
  \leavevmode%
  \hbox{%
    \begin{beamercolorbox}[wd=\paperwidth,ht=2.25ex,dp=1ex,right]{footlinecolor}%
      \hfill \insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
    \end{beamercolorbox}
  }%
  \vskip0pt%
}
\usebeamertemplate{mytheme}

\newcommand*\MyItem{%
  \item[\color{black} $\rightarrow$]}

\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\hide}[1]{}
\newcommand{\indnt}{\hspace{2ex}}

\definecolor{lightblue}{rgb}{0.74, 0.83, 0.9}
\definecolor{middleblue}{rgb}{0.19, 0.53, 0.74}

\definecolor{Magenta}{cmyk}{0,1,0,0}
\newcommand{\trans}[1]{\overset{#1}{\longrightarrow}}
\newtheorem{proposition}[theorem]{\translate{Proposition}}


\title{TASTE2BIP \\ - Current status -}
\author{Iulia Dragomir}
\institute[VERIMAG]{Universite Grenoble Alpes -- VERIMAG, France}
\date{}

\begin{document}


\makeatletter
\setbeamertemplate{title page}
{
  \vbox{}
  \vfill
  \begin{centering}
  	\vskip1em\par
    \begin{beamercolorbox}[sep=8pt,center]{title}
      \usebeamerfont{title}\inserttitle
    \end{beamercolorbox}
    \vskip1em\par
    \begin{beamercolorbox}[sep=8pt,center]{author}
      \usebeamerfont{author} {\insertauthor }
    \end{beamercolorbox}
    \vskip-0.5em\par % change here
    \begin{beamercolorbox}[sep=8pt,center]{institute}
      \usebeamerfont{institute}\insertinstitute
    \end{beamercolorbox}
    %\vskip-0.5em\par
    %\begin{beamercolorbox}[sep=8pt,center]{date}
    %  \usebeamerfont{date}\insertdate
    %\end{beamercolorbox}
    \vskip-0.3em\par
  \end{centering}
  \vfill
}
\makeatother

{
\begingroup
\setbeamertemplate{footline}{}

\begin{frame}
\label{frame:title}
\maketitle
\end{frame}
\endgroup
}


\pgfdeclarelayer{bg}    % declare background layer
\pgfsetlayers{bg,main}  % set the order of the layers (main is the standard layer)

\begin{frame}[t]{The TASTE Framework}
\label{frame:taste}
  \begin{itemize}
    \item Targets the model-based development of \blue{heterogeneous}, \blue{reactive}, \blue{discrete} \blue{embedded systems} %\medskip
    \item The \blue{language} is based on several \red{modeling formalisms} (ASN.1, AADL , SDL, Simulink, etc.) or \red{programming languages} (C, Ada) %\medskip
  \end{itemize} \vspace{-1.5ex}
  \begin{columns} \pause
  \begin{column}[T]{0.67\textwidth}
    \begin{itemize}
    \item A \blue{TASTE design} consists of:
      \begin{itemize}
	\item \red{Data view}
	\item \red{Hierarchical interface views} (software architecture and behavior)
	\item \red{Deployment view}
	\item \red{Concurrency view} computed from the above
      \end{itemize} %medskip
    \item<3> The \blue{tool-chain} allows for:
      \begin{itemize}
	\item \red{Well-formedness and typing analysis} of a design wrt the used modeling formalisms
	\item \red{Real-time scheduling analysis} (e.g., Cheddar)
	\item \red{Code generation} and \red{deployment} (in C/C++, Ada) wrt an execution platform
	\item \red{Simulation}, \red{debugging} and \red{testing}
      \end{itemize}
    \end{itemize}
    \end{column}
    \begin{column}[T]{0.3\textwidth} 
      \includegraphics[scale=0.4]{figures/iv} \\ \vspace{1ex}
      ~~~~~~~~~~\includegraphics[scale=0.45]{figures/behavior}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[t]{Shortcomings of TASTE}
  \begin{itemize}
    \item \blue{Large}, \blue{reactive}, \blue{component-based designs} in graphical \blue{semi-formal languages} \\
      $\rightarrow$ ambiguous or unspecified operational semantics \\
      $\rightarrow$ erroneous implementations due to different possible interpretations \medskip
    \item \blue{Assumptions} of a model are made that are \blue{not validated} \\
      $\rightarrow$ for example, the WCET is declared but its ``validity'' is not checked \medskip
    \item \blue{A posteriori validation} of the implementation \\
      $\rightarrow$ testing is incomplete (e.g., autonomous systems operating in unknown environment)
  \end{itemize} \bigskip \pause
  
  \centerline{ \red{We need a rigorous system design approach for TASTE!} }
\end{frame}


\begin{frame}[t]{Formal Semantics for TASTE}
\label{frame:semantics}
  \begin{itemize}
   \item TASTE interface view with behavior $\equiv$ \\Network of communicating timed automata extended with data \smallskip
   \item Software function in SDL $\equiv$ \\Timed automaton with typed variables \smallskip
   \item Cyclic interface $\equiv$ \\Timed automaton that sends a signal at periodic intervals \smallskip
   \item Sporadic interface $\equiv$ \\Timed automaton containing a queue storing requests and forwarding them to its ``owner'' when the latter is available \smallskip
   \item Sending/receiving signals $\equiv$ \\Regular parallel composition with synchronization on name matching events \smallskip
   \item Time-triggered actions $\equiv$ \\Time-guarded actions \smallskip
   \item All actions of a component's behavior are {\em eager}
  \end{itemize}

\end{frame}

\begin{frame}{Mapping TASTE Modeling Elements to BIP}
\label{frame:patterns}

\only<1>{\framesubtitle{System architecture}
	\centering
	\begin{tikzpicture}
		\node (iv) {\includegraphics[scale=0.5]{figures/ex_iv}};
	
		\node[right=10ex of iv] (diag) {
			\scalebox{0.6}{
			\begin{tikzpicture}
				\node[draw,minimum height=5ex,align=center,inner sep=2ex] (step) {prod\_step : \\ \underline{TASTE\_PI\_Cyclic(1000,1000)}};
				\node[draw,minimum height=5ex,align=center,inner sep=2ex,below=10ex of step] (prod) {prod : \\ \underline{Prod}};
				\node[draw,minimum height=5ex,align=center,inner sep=2ex,below=10ex of prod] (comp) {cons\_comp : \\ \underline{TASTE\_PI\_Sporadic\_Int(200,200,2)}};
				\node[draw,minimum height=5ex,align=center,inner sep=2ex,below=10ex of comp] (cons) {cons : \\ \underline{Cons}};
				\node[draw,fit={(step)(prod)([xshift=-1ex]comp.west)([xshift=1ex]comp.east)(cons)},label={[label distance=3pt]70:System}] (syst) {};
				
				\draw[-] ([xshift=1ex]step.south) -- ([xshift=1ex]step.south |- prod.north) node[midway,align=left] {(sig\_out,\\ step\_in)};
				\draw[-] ([xshift=-1ex]prod.north) -- ([xshift=-1ex]prod.north |- step.south) node[midway,left,align=right] {(step\_return,\\ sig\_return)};
				\draw[-] (prod.south) -- (comp.north) node[midway,align=left] {(comp\_out(v),\\ sig\_out(v))};
				\draw[-] ([xshift=1ex]comp.south) -- ([xshift=1ex]comp.south |- cons.north) node[midway,align=left] {(sig\_in(v),\\ comp\_in(v))};
				\draw[-] ([xshift=-1ex]cons.north) -- ([xshift=-1ex]cons.north |- comp.south) node[midway,left,align=right] {(comp\_return,\\ sig\_return)};
			\end{tikzpicture}}
		};
		
		\draw[-latex',thick] ([xshift=-8ex,yshift=10ex]iv.east) -- ([xshift=4ex,yshift=17ex]diag.west);
		\draw[-latex',thick] ([xshift=-2ex,yshift=6ex]iv.east) -- ([xshift=10ex,yshift=6ex]diag.west);
		\draw[-latex',thick] ([xshift=-8ex,yshift=-3ex]iv.east) -- ([xshift=2ex,yshift=-6ex]diag.west);
		\draw[-latex',thick] ([xshift=-2ex,yshift=-7ex]iv.east) -- ([xshift=10ex,yshift=-17ex]diag.west);
	\end{tikzpicture}
}


\only<2-3>{\framesubtitle{Cyclic and sporadic interfaces}
	\begin{columns}
		\begin{column}[T]{0.5\textwidth}
			TASTE\_PI\_Cyclic(period, deadline): \bigskip 
			  
			\scalebox{0.4}{
			\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3.8cm,semithick]
  				\node[initial,state,circle split] (l0)               {$l_0$ \nodepart{lower} \textcolor{white}{$t \leq \mathsf{deadline}$}};
  				\node[state,circle split]         (l1) [right=15ex of l0] {$l_1$ \nodepart{lower} $t \leq \mathsf{deadline}$};
  				\node[state,circle split]         (l2) [below of=l1] {$l_2$ \nodepart{lower} $t \leq \mathsf{deadline}$};
    
  				\path (l0) edge             node[align=center] {${sig\_out}\ /$\\$t=0^{\epsilon}$}    (l1)
        				 (l1) edge [bend right,left] node {${sig\_return}^{\epsilon}$} (l2)
        				 (l2) edge [bend right,right] node[align=right] {$[t == \mathsf{period}]\ /$\\ ${sig\_out};$ \\ $t=0^{\epsilon}$}(l1);   

        		\node[minimum height=5ex, below=50ex of l2] (aux) {};
			\end{tikzpicture}
  			} 
		\end{column} 
		\begin{column}[T]{0.5\textwidth} 		
			\only<3>{
			TASTE\_PI\_Sporadic\_ParamType(miat, deadline, size): \bigskip 
			
			\scalebox{0.4}{
			\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=4cm,semithick]
				\node[state,circle split] (l0)               {$l_0$ \nodepart{lower} \textcolor{white}{$t \leq deadline$}};
				\node[state,circle split]         (l1) [below of=l0] {$l_1$ \nodepart{lower} $t \leq \mathsf{deadline}$};
				\node[state,circle split]         (l2) [below= 8ex of l1] {$l_2$ \nodepart{lower} $t \leq \mathsf{deadline}$};
				\node (l3) [left=9ex of l0] {start};
    
				\draw[-latex'] (l3.east) --++ (4ex,0) node[anchor=south,align=center] {val=0} -- (l0.west);     
				\path (l0) edge [loop above] node[right,align=left,xshift=2ex] {$sig\_out(val);$\\$push\_back(q, val, \mathsf{size})^{\epsilon}$} (l0)
					     (l0) edge node[align=left,xshift=2ex] {$[get\_size(q) > 0]\ /$\\$sig\_in(pop\_front(q));$\\$t=0^{\epsilon}$} (l1)
					     (l1) edge node[xshift=1ex] {$sig\_return^{\epsilon}$} (l2)
						 (l1) edge [loop right,in=0,out=20,min distance=10mm,align=left] node {$sig\_out(val);$\\$push\_back(q, val, \mathsf{size})^{\epsilon}$} (l1)
						 (l2) edge [bend left=30] node[align=right] {$[t = \mathsf{miat}]\ /$\\$t=0^{\epsilon}$} (l0)
						 (l2) edge [loop below] node[right,pos=0.2,align=left] {$sig\_out(val);$\\$push\_back(q, val, \mathsf{size})^{\epsilon}$}(l2);
			\end{tikzpicture}
			}}
		\end{column} 
	\end{columns}	
}

\only<4->{
	\framesubtitle{SDL state machines}
	Prod:
	\begin{tikzpicture}
		\node (sm) {\includegraphics[scale=0.4]{figures/prod_sm}};
		
		\node[right=3ex of sm] (ta) {
			\scalebox{0.5}{
			\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
				\node[state,inner sep=1ex] (l1)                    {$wait$};
				\node (l0) [left=9ex of l1]   {start};
				\node[state,inner sep=1ex] (l2) [right=15ex of l1] {$l_1$};
				\node[state,inner sep=1ex] (l3) [below=5ex of l2] {$l_2$};
				\node[state,inner sep=1ex] (l4) [below=5ex of l3] {$l_3$};
				\node[state,inner sep=1ex] (l5) [left=15.8ex of l4] {$l_4$};

				\draw[-latex'] (l0.east) --++ (4ex,0) node[anchor=south,align=center] {val=0} -- (l1.west);  
    
				\path (l1) edge             node[align=center] {${step\_in}^{\epsilon}$}    (l2)
						(l2) edge [right] node[align=left] {$printf('Sent\ value: \%d', val)^{\epsilon}$} (l3)
						(l3) edge [right] node[align=left] {${comp\_out}^{\epsilon}$} (l4)
						(l4) edge node[align=center] {$val = val + 1^\epsilon$} (l5)
						(l5) edge [left] node[align=right,xshift=-1ex] {${step\_return}^{\epsilon}$}(l1);   
				
			\end{tikzpicture}
			}};
			\node<4>[minimum height=1ex, below=50ex of sm] (aux) {};
	\end{tikzpicture}
	
	\only<5->{
	Cons:
	\begin{tikzpicture}
		\node (sm) {\includegraphics[scale=0.4]{figures/cons_sm}};
		
		\node[right=3ex of sm] (ta) {	
			\scalebox{0.5}{
			\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,semithick]
				\node[state,inner sep=1ex] (l1)                    {$wait$};
				\node (l0) [left=9ex of l1]   {start};
				\node[state,inner sep=1ex] (l2) [right=15ex of l1] {$l_1$};
				\node[state,inner sep=1ex] (l3) [below left= 7ex and 7ex of l2] {$l_2$};

				\draw[-latex'] (l0.east) --++ (4ex,0) node[anchor=south,align=center] {val=0} -- (l1.west);  
    
				\path (l1) edge node[align=center] {$comp\_in^\epsilon$}    (l2)
						(l2) edge node[align=left] {$printf('Received\ value:$\\$\%d', val)^{\epsilon}$} (l3)
						(l3) edge node[align=right] {$comp\_return^{\epsilon}$} (l1);
			\end{tikzpicture}}		
		};
	\end{tikzpicture}
	}
}
\end{frame}

\begin{frame}[t]{Implementation}
\label{frame:implem}
	\begin{itemize}
		\item With the help of Ellidiss \smallskip
		\item In Prolog -- already available parser for IV and easy to integrate in TASTE \smallskip
		\item BIP patterns for each TASTE-SDL modeling element are defined
		\item First prototype available -- generation of the system architecture modulo parameterization \smallskip
	\end{itemize}	  
\end{frame}

\begin{frame}[t]{Ongoing and Future Work}
\label{frame:fwork}
 	\begin{enumerate}
 		\item Pattern validation on several examples, e.g., component with multiple sporadic PIs, SDL decision element, SDL timer, etc. \smallskip
 		\item Pattern definitions for external code handling ASN.1 data types \smallskip
 		\item Definition of a ``more structured'' architecture, e.g., compound of a TASTE component and all its PIs \smallskip
 		\item Definition of the semantics of hierarchical views and possibly new patterns \smallskip
 		\item Pattern definition for TASTE components in C++ \smallskip
 		\item Validation and implementation of the above iteratively
 	\end{enumerate}
\end{frame}



\end{document}
