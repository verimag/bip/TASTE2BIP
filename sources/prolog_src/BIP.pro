/* H2020 ESROCOS/ERGO Projects
   Company: Ellidiss Technologies and Verimag Laboratory
   Licence: CeCILL-B*/


/* ---------------------------------------*/
insertBIPHeader :- 
    write('//-------------------------------------'), nl, 
    write('// TASTE to BIP model transformation '), nl, 
    write('// UGA/Verimag and Ellidiss Technologies '), nl, 
    write('// ESROCOS/ERGO projects - April 2018  - v.0.9'), nl, 
    write('//-------------------------------------'), nl, nl.

/* ---------------------------------------
 Main function*/
insertBIPPackage(Label,File) :- 
    splitName(File,_,F),
    write('@cpp(src="ext-cpp/Extern'), write(F),
    write('Fcns.cpp'),
    fail.
insertBIPPackage(Label,File) :- 
    getFunctionLanguage(Pkg,Type,'(C)'),
    toLower(Type,Type2),
    write(','), write(Type2), write('/'), write(Type2), write('.c'),
    fail.
insertBIPPackage(Label,File) :- 
    splitName(File,_,F),
    write('",include="Extern'), write(F), write('Fcns.hpp'),
    fail.
insertBIPPackage(Label,File) :- 
    getFunctionLanguage(Pkg,Type,'(C)'),
    toLower(Type,Type2),
    write(','), write(Type2), write('.h'),
    fail.
insertBIPPackage(Label,File) :- 
    write('")'), nl,
    splitName(File,_,F),
    write('package '), write(F), nl, nl,
    insertBIPConstants(Label),
    insertBIPExterns(Label),
    insertBIPPortTypes(Label),
    insertBIPConnectorTypes(Label),
    insertBIPAtomTypes(Label),
    insertBIPCompoundTypes(Label),
    fail.
insertBIPPackage(Label,File) :- 
    write('end'), nl.

/* ---------------------------------------
 Function for SDL constants in BIP*/
insertBIPConstants(IV) :-
    write('  const data bool TRUE = true'), nl,
    write('  const data bool FALSE = false'), nl,
    nl.

/* ---------------------------------------
 Functions for BIP external declarations*/
insertBIPExterns(IV) :- 
    write('  // Extern declarations'), nl,  
    insertBIPExternsFcnType(IV, 'Bool'),
    insertBIPExternsFcnType(IV, 'Int'),
    insertBIPExternsFcnType(IV, 'Float'),
    insertBIPExternsFcnType(IV, 'String'),
    insertBIPExternCFcn.

insertBIPExternsFcnType(IV,Type) :- 
    toLower(Type,D),
    write('  extern data type queue'), write(Type), nl,
    write('  extern function int get_size(const queue'), write(Type), write(')'), nl,
    write('  extern function push_back(queue'), write(Type), write(', '), write(D), write(', int)'), nl,
    write('  extern function '), write(D), write(' pop_front(queue'), write(Type), write(')'), nl,
    write('  extern function write(const '), write(D), write(')'), nl,
    write('  extern function writeln(const '), write(D), write(')'), nl,
    nl.

insertBIPExternCFcn :- 
    write('  // Integration of the C code'), nl,
    getFunctionLanguage(Pkg,Type,'(C)'), 
    write('  extern function '), write(Type), write('_startup_wrap()'),nl,
    isTastePI(Type,PIName), insertBIPExternPICFcn(Pkg,Type,PIName).
insertBIPExternCFcn :- 
    nl,
    getFunctionLanguage(Pkg,Type,'(CPP)'), 
    write('  extern function '), write(Type), write('_startup_wrap()'),nl,
    isTastePI(Type,PIName), insertBIPExternPICFcn(Pkg,Type,PIName).
insertBIPExternCFcn :- 
    nl, true.

insertBIPExternPICFcn(Pkg,Type,PIName) :- 
    write('  extern function float '), write(Type), write('_'), write(PIName), write('_wrap('),
    cpt2Rst,
    concat('PI_',PIName,PIName2),
    isFeature('PARAMETER',Pkg,PIName2,Label,Dir,'NIL',C,_,_,_),
    ( ( cpt2Get(G), G=1 ) -> write(', ');cpt2Inc),
    ( Dir = 'IN' -> write('const ');true),
    splitReference(C,ParamPkg,ParamType,ParamImpl), 
    getBIPType(ParamType,BIPType), toLower(BIPType,BIPType2), write(BIPType2),
    fail.
insertBIPExternPICFcn(Pkg,Type,PIName) :- 
    write(')'), nl,
    fail.

/* ---------------------------------------
 Functions for the definition of BIP port types (parameterized for BIP predefined
 types)*/
insertBIPPortTypes(IV) :- 
    write('  // Port types'), nl, 
    write('  port type Port()'), nl, nl,
    write('  port type portNil()'), nl,
    write('  port type queuePortNil(int current)'), nl, 
    insertBIPPortParamType(IV, 'Bool'), 
    insertBIPPortParamType(IV, 'Int'),
    insertBIPPortParamType(IV, 'Float'),
    insertBIPPortParamType(IV, 'String'),
    nl.

insertBIPPortParamType(IV,Type) :- 
    toLower(Type,D),
    write('  port type port'), write(Type), write('('), write(D), write(' v)'), nl,
    write('  port type queuePort'), write(Type), write('(queue'), write(Type), write(' q)'), nl.

/* ---------------------------------------
 Functions for the definition of BIP connector types (parameterized for
 BIP predefined types)*/
insertBIPConnectorTypes(IV) :- 
    write('  // Connector types'), nl,
    write('  connector type Sync(Port s, Port r)'), nl, 
    write('    define s r'), nl, 
    write('  end'), nl, 
    nl,
    insertBIPConnectorParamNil(IV),
    insertBIPConnectorParamType(IV, 'Bool'),
    insertBIPConnectorParamType(IV, 'Int'),
    insertBIPConnectorParamType(IV, 'Float'),
    insertBIPConnectorParamType(IV, 'String'),
    nl.

insertBIPConnectorParamNil(IV) :- 
    write('  connector type SendRecNil(portNil s, portNil r)'), nl, 
    write('    define s r'), nl, 
    write('  end'), nl, 
    nl, 
    write('  connector type SendRecQueueNil(queuePortNil s, portNil r)'), nl, 
    write('    define s r'), nl, 
    write('    on s r'), nl, 
    write('      down { if (s.current > 0) then s.current = s.current - 1;'),
    write(' else writeln("Queue empty!"); fi }'), nl, 
    write('  end'), nl, 
    nl.

insertBIPConnectorParamType(IV,Type) :- 
    write('  connector type SendRec'), write(Type), write('(port'), write(Type), write(' s, port'), write(Type), write(' r)'), nl, 
    write('    define s r'), nl, 
    write('    on s r'), nl, 
    write('      down { r.v = s.v; }'), nl, 
    write('  end'), nl, 
    nl, 
    write('  connector type SendRecQueue'), write(Type), write('(queuePort'), write(Type), write(' s, port'), write(Type), write(' r)'), nl, 
    write('    define s r'), nl, 
    write('    on s r'), nl, 
    write('      down { r.v = pop_front(s.q); }'), nl, 
    write('  end'), nl, 
    nl.

/* ---------------------------------------
 Functions for BIP atomic component types declarations*/
insertBIPAtomTypes(IV) :- 
    insertBIPCyclicType(IV), 
    insertBIPSporadicType(IV), 
    insertBIPProtectedType,
    write('  // TASTE terminal functions'), nl, 
    isTasteFunction(F,P,T,I,'terminal'), getFunctionLanguage(P,T,L), 
    ( L='(SDL)' -> insertBIPFunctionType(IV,F,P,T,I) ; true), 
    ( L='(C)' -> insertBIPCFunctionType(IV,F,P,T,I) ; true), 
    ( L='(CPP)' -> insertBIPCFunctionType(IV,F,P,T,I) ; true),
    nl, fail.
insertBIPAtomTypes(IV) :- 
    true.

/* ---------------------------------------
 Function for BIP declaration of cyclic interfaces*/
insertBIPCyclicType(IV) :- 
    write('  // Generic TASTE cyclic interface type'), nl, 
    write('  atom type TASTE_PI_Cyclic(int period, int deadline)'), nl, 
    write('    clock t unit millisecond'), nl, 
    nl, 	
    write('    export port Port sig_out()'), nl, 
    write('    export port Port sig_return()'), nl, 
    nl, 	
    write('    place l0, l1, l2'), nl, 
    nl, 	
    write('    initial to l0'), nl, 
    nl,
    write('    on sig_out'), nl, 
    write('      from l0 to l1'), nl, 
    write('      eager'), nl, 
    write('      do { t = 0; }'), nl, 
    nl, 
    write('    on sig_return'), nl, 
    write('      from l1 to l2'), nl, 
    write('      eager'), nl, 
    nl, 
    write('    on sig_out'), nl, 
    write('      from l2 to l1'), nl, 
    write('      provided (t == period)'), nl, 
    write('      do { t = 0; }'), nl, 
    nl, 
    write('    invariant inv1 at l1 provided (t <= deadline)'), nl, 
    write('    invariant inv2 at l2 provided (t <= period)'), nl, 
    write('  end'), nl, 
    nl.

/* ---------------------------------------
 Function for BIP declaration of sporadic interfaces (parameterized for
 BIP predefined types)*/
insertBIPSporadicType(IV) :- 
    write('  // Generic TASTE sporadic interfaces'), nl, 
    nl,
    insertBIPSporadicParamNil(IV),
    nl, 
    insertBIPSporadicParamType(IV, 'Bool'),
    insertBIPSporadicParamType(IV, 'Int'),
    insertBIPSporadicParamType(IV, 'Float'),
    insertBIPSporadicParamType(IV, 'String').

insertBIPSporadicParamNil(IV) :- 
    write('  // Sporadic interface type with no parameter'), nl,
    write('  atom type TASTE_PI_Sporadic_Nil(int miat, int deadline, int size)'), nl,
    write('    clock t unit millisecond'), nl,
    write('    data int current'), nl,
    nl,
    write('    export port portNil sig_out()'), nl,
    write('    export port Port sig_return()'), nl,
    write('    export port queuePortNil sig_in(current)'), nl,
    nl,
    write('    port Port silent()'), nl,
    nl,
    write('    place l0, l1, l2'), nl,
    nl,
    write('    initial to l0'), nl,
    write('      do { current = 0; }'), nl,
    nl,
    write('    on sig_in'), nl,
    write('      from l0 to l1'), nl,
    write('      provided(current>0)'), nl,
    write('      eager'), nl,
    write('      do { t = 0; }'), nl,
    nl,
    write('    on sig_out'), nl, 
    write('      from l0 to l0'), nl,
    write('      eager'), nl,
    write('      do { if (current < size) then current = current + 1;'),
    write(' else writeln("Queue full!"); fi }'), nl,
    nl, 
    write('    on sig_return'), nl,
    write('      from l1 to l2'), nl,
    write('      eager'), nl,
    nl,
    write('    on sig_out'), nl,
    write('      from l1 to l1'), nl,
    write('      eager'), nl,
    write('      do { if (current < size) then current = current + 1;'),
    write(' else writeln("Queue full!"); fi }'), nl,
    nl,
    write('    on silent'), nl,
    write('      from l2 to l0'), nl,
    write('      provided (t == miat)'), nl,
    write('      eager'), nl,
    write('      do { t = 0; }'), nl,
    nl,
    write('    on sig_out'), nl,
    write('      from l2 to l2'), nl,
    write('      eager'), nl,
    write('      do { if (current < size) then current = current + 1;'),
    write(' else writeln("Queue full!"); fi }'), nl,
    nl,
    write('    invariant inv1 at l1 provided (t <= deadline)'), nl,
    write('    invariant inv2 at l2 provided (t <= miat)'), nl,
    write('  end'), nl,
    nl.

insertBIPSporadicParamType(IV,Type) :- 
    toLower(Type, D),
    write('  // Sporadic interface type with parameter of '), write(D), write(' type'), nl, 
    write('  atom type TASTE_PI_Sporadic_'), write(Type), write('(int miat, int deadline, int size)'), nl, 
    write('    clock t unit millisecond'), nl, 
    write('    data '), write(D), write(' val'), nl, 
    write('    data queue'), write(Type), write(' q'), nl, 
    nl, 
    write('    export port port'), write(Type), write(' sig_out(val)'), nl, 
    write('    export port Port sig_return()'), nl, 
    write('    export port queuePort'), write(Type), write(' sig_in(q)'), nl, 
    nl, 
    write('    port Port silent()'), nl, 
    nl, 
    write('    place l0, l1, l2'), nl, 
    nl, 
    write('    initial to l0'), nl, 
    write('      do { val = '), insertInitialValue(Type), write('; }'), nl, 
    nl, 
    write('    on sig_in'), nl, 
    write('      from l0 to l1'), nl, 
    write('      provided(get_size(q)>0)'), nl, 
    write('      eager'), nl, 
    write('      do { t = 0; }'), nl, 
    nl, 
    write('    on sig_out'), nl, 
    write('      from l0 to l0'), nl, 
    write('      eager'), nl, 
    write('      do { push_back(q, val, size); }'), nl, 
    nl, 
    write('    on sig_return'), nl, 
    write('      from l1 to l2'), nl, 
    write('      eager'), nl, 
    nl, 
    write('    on sig_out'), nl, 
    write('      from l1 to l1'), nl, 
    write('      eager'), nl, 
    write('      do { push_back(q, val, size); }'), nl, 
    nl, 
    write('    on silent'), nl, 
    write('      from l2 to l0'), nl, 
    write('      provided (t == miat)'), nl, 
    write('      eager'), nl, 
    write('      do { t = 0; }'), nl, 
    nl, 
    write('    on sig_out'), nl, 
    write('      from l2 to l2'), nl, 
    write('      eager'), nl, 
    write('      do { push_back(q, val, size); }'), nl, 
    nl, 	
    write('    invariant inv1 at l1 provided (t <= deadline)'), nl, 
    write('    invariant inv2 at l2 provided (t <= miat)'), nl, 
    write('  end'), nl, 
    nl.

insertBIPProtectedType :- 
    write('  // Generic TASTE protected interface type'), nl,
    write('  atom type TASTE_PI_Protected()'), nl,
    write('    export port Port lock()'), nl,
    write('    export port Port release()'), nl,
    nl,
    write('    place l0, l1'), nl,
    nl,
    write('    initial to l0'), nl,
    nl,
    write('    on lock'), nl,
    write('      from l0 to l1'), nl,
    write('      eager'), nl,
    nl,
    write('    on release'), nl,
    write('      from l1 to l0'), nl,
    write('      eager'), nl,
    write('  end'), nl, nl.

insertInitialValue(Type) :- 
    ( Type = 'Bool' -> write('false') ;   
    	Type = 'String' -> write('""') ;   
    	write('0') ).

/* ---------------------------------------
 Function for BIP declaration of atomic TASTE functions*/
insertBIPFunctionType(IV,Label,Pkg,Type,Impl) :- 
    write('  atom type '), write(Label), write('('),
    fail.
insertBIPFunctionType(IV,Label,Pkg,Type,Impl) :- 
    cpt2Rst, 
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    ( ( cpt2Get(N), N=0 ) -> cpt2Inc ; write(', ') ),
    write('int '), write(RIName), write('_deadline, int '), write(RIName), write('_wcet'),
    fail.
insertBIPFunctionType(IV,Label,Pkg,Type,Impl) :- 
    write(')'), nl, 
    toLower(Label,Process),
    insertBIPData(Pkg,Process,Type), 
    insertBIPPort(Pkg,Type), 
    insertBIPBehavior(Label), 
    write('  end'), nl.

insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    cpt1Rst,
    write('  atom type '), write(Label), write('()'), nl,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_), 
    getBIPType(T,BIPType), BIPType\='Nil', 
    toLower(BIPType, X), 
    write('    data '), write(X), write(' '), 
    write(L), write('_'), write(P), nl,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    write('    clock '), write(Label), write('_comp_wait unit millisecond'), nl,
    write('    data float '), write(Label), write('_comp_time'), nl,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    isTastePI(Pkg,Type,L,_,_,'cyclic',_,_,_,_), 
    write('    export port Port '), write(L), write('_in()'), nl, 
    write('    export port Port '), write(L), write('_return()'), nl, 
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_), 
    getBIPType(T,BIPType), BIPType\='Nil', 
    write('    export port port'), write(BIPType), write(' '), write(L),
    write('_in('), write(L), write('_'), write(P), write(')'), nl, 
    write('    export port Port '), write(L), write('_return()'), nl, 
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_), 
    getBIPType(T,BIPType), memezTra(BIPType,'Nil'), 
    write('    export port port'), write(BIPType), write(' '), write(L), write('_in()'), nl, 
    write('    export port Port '), write(L), write('_return()'), nl, 
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    nl, write('    port Port silent()'), nl, nl,
    write('    place Wait'), cpt1Inc,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    cpt2Rst, cpt2Inc, 
    ( isTastePI(Pkg,Type,L,P,T,'cyclic',_,_,_,_); isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_)),
    cpt2Get(N),
    write(', l'), write(N), N2 is N+1,
    write(', l'), write(N2), N3 is N+2,
    write(', l'), write(N3),
    cpt2Inc, cpt2Inc, cpt2Inc,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    nl, nl, write('    initial to Wait'), nl,
    write('      do { '), write(Label), write('_startup_wrap(); }'), nl,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    ( isTastePI(Pkg,Type,L,P,T,'cyclic',_,_,_,_); isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_)),
    cpt1Get(N),
    isTastePI(Type,PIName),
    concat(Type,'_',PIName,L),
    N2 is N+1, N3 is N+2,
    nl,
    write('    on '), write(Label), write('_'), write(PIName), write('_in'), nl,
    write('      from Wait to l'), write(N), nl,
    write('      eager'), nl, nl,
    write('    on silent'), nl,
    write('      from l'), write(N), write(' to l'), write(N2), nl,
    write('      eager'), nl,
    write('      do { '), write(Label), write('_comp_wait = 0; '), write(Label), 
    write('_comp_time = '), write(Label), write('_'), write(PIName), write('_wrap('), 
    ( memezTra(P,'NIL') ->
        true;
        ( write(Label), write('_'), write(PIName), write('_'), write(P) )
    ), 
    write('); }'), nl, nl,
    write('    on silent'), nl,
    write('      from l'), write(N2), write(' to l'), write(N3), nl,
    write('      provided ('), write(Label), write('_comp_wait == '), write(Label), write('_comp_time)'), nl,
    write('      eager'), nl, nl,
    write('    on '), write(Label), write('_'), write(PIName), write('_return'), nl,
    write('      from l'), write(N3), write(' to Wait'), nl,
    write('      eager'), nl,
    cpt1Inc, cpt1Inc, cpt1Inc,
    fail.
insertBIPCFunctionType(IV,Label,Pkg,Type,Impl) :- 
    write('  end'), nl.

insertBIPData(Pkg,Process,Type) :- 
    insertBIPProcessData(Process),
    fail.
insertBIPData(Pkg,Process,Type) :- 
    isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_), 
    getBIPType(T,BIPType), BIPType\='Nil', 
    toLower(BIPType, X), 
    write('    data '), write(X), write(' '), 
    write(L), write('_'), write(P), nl,
    fail.
insertBIPData(Pkg,Process,Type) :- 
    isTasteRI(Type,RIName), 
    not(isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName)),
    concat(Type,'_',RIName,L),
    isTasteRI(Pkg,Type,L,P,T), 
    getBIPType(T,BIPType), BIPType\='Nil', 
    toLower(BIPType, X), 
    write('    data '), write(X), write(' '), 
    write(L), write('_'), write(P), nl,
    fail.
insertBIPData(Pkg,Process,Type) :- 
    isSDLProcedure(Process,ProcedureId,_),
    insertBIPProcedureData(Process,ProcedureId),
    fail.
insertBIPData(Pkg,Process,Type) :- 
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName), 
    not(alreadyDoneProtected2(Pkg,Type)),
    write('    clock '), write(Type), write('_comp_wait unit millisecond'), nl,
    write('    data float '), write(Type), write('_comp_time'), nl,
    write('    clock '), write(Type), write('_comp_t unit millisecond'), nl, 
    $assert(alreadyDoneProtected2(Pkg,Type)),
    fail.
insertBIPData(Pkg,Process,Type) :- 
    true.

insertBIPProcessData(Process) :- 
    isSDLVariable(Process,'NIL',P,T,_,_), 
    toLower(T,T2), T2\='timer',
    getBIPType(T,BIPType),
    toLower(BIPType, X),
    write('    data '), write(X), write(' '), write(P), nl,
    fail.
insertBIPProcessData(Process) :- 
    isSDLVariable(Process,'NIL',P,T,_,_),
    toLower(T,T2), memezTra(T2,'timer'),
    write('    clock '), write(P), write(' unit millisecond'), nl,
    write('    data int '), write(P), write('_val'), nl,
    fail.

insertBIPProcedureData(Process,ProcedureId) :- 
    isSDLVariable(Process,ProcedureId,P,T,_,_),
    toLower(T,T2), T2\='timer',
    getBIPType(T,BIPType),
    toLower(BIPType, X),
    write('    data '), write(X), write(' '), write(ProcedureId), write('_'), write(P), nl,
    fail.
insertBIPProcedureData(Process,ProcedureId) :- 
    isSDLVariable(Process,ProcedureId,P,T,_,_),
    toLower(T,T2), memezTra(T2,'timer'),
    write('    clock '), write(P), write(' unit millisecond'), nl,
    write('    data int '), write(ProcedureId), write('_'), write(P), write('_val'), nl,
    fail.
insertBIPProcedureData(Process,ProcedureId) :- 
    isSDLProcedureParam(Process,ProcedureId,P,_,T,_),
    toLower(T,T2), T2\='timer',
    getBIPType(T,BIPType),
    toLower(BIPType, X),
    write('    data '), write(X), write(' '), write(ProcedureId), write('_'), write(P), nl,
    fail.
insertBIPProcedureData(Process,ProcedureId) :- 
    isSDLProcedureParam(Process,ProcedureId,P,_,T,_),
    toLower(T,T2), memezTra(T2,'timer'),
    write('    clock '), write(P), write(' unit millisecond'), nl,
    write('    data int '), write(ProcedureId), write('_'), write(P), write('_val'), nl,
    fail.

insertBIPPort(Pkg,Type) :- 
    isTastePI(Pkg,Type,L,_,_,'cyclic',_,_,_,_), 
    write('    export port Port '), write(L), write('_in()'), nl, 
    write('    export port Port '), write(L), write('_return()'), nl, 
    fail.
insertBIPPort(Pkg,Type) :- 
    isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_), 
    getBIPType(T,BIPType), BIPType\='Nil', 
    write('    export port port'), write(BIPType), write(' '), write(L),
    write('_in('), write(L), write('_'), write(P), write(')'), nl, 
    write('    export port Port '), write(L), write('_return()'), nl, 
    fail.
insertBIPPort(Pkg,Type) :- 
    isTastePI(Pkg,Type,L,P,T,'sporadic',_,_,_,_), 
    getBIPType(T,BIPType), memezTra(BIPType,'Nil'), 
    write('    export port port'), write(BIPType), write(' '), write(L), write('_in()'), nl, 
    write('    export port Port '), write(L), write('_return()'), nl, 
    fail.
insertBIPPort(Pkg,Type) :- 
    isTasteRI(Type,RIName), 
    not(isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName)),
    concat(Type,'_',RIName,L),
    isTasteRI(Pkg,Type,L,P,T), 
    getBIPType(T,BIPType), BIPType\='Nil', 
    write('    export port port'), write(BIPType), write(' '), write(L),
    write('_out('), write(L), write('_'), write(P), write(')'), nl, 
    fail.
insertBIPPort(Pkg,Type) :- 
    isTasteRI(Type,RIName), 
    not(isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName)),
    concat(Type,'_',RIName,L),
    isTasteRI(Pkg,Type,L,P,T), 
    getBIPType(T,BIPType), memezTra(BIPType,'Nil'), 
    write('    export port port'), write(BIPType), write(' '), write(L), write('_out()'), nl, 
    fail.
insertBIPPort(Pkg,Type) :- 
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    write('    export port Port '), write(Type), write('_'), write(RIName), write('_lock()'), nl,
    write('    export port Port '), write(Type), write('_'), write(RIName), write('_release()'), nl,
    fail.
insertBIPPort(Pkg,Type) :- 
    nl, write('    port Port silent()'), nl.

insertBIPBehavior(Process) :- 
    toLower(Process,P), nl,
    insertBIPPlaces(P),
    fail.
insertBIPBehavior(Process) :- 
    toLower(Process,P), cpt1Rst,
    initPlaceCpt(P),
    fail.
insertBIPBehavior(Process) :- 
    toLower(Process,P),
    insertBIPTransitions(P,'NIL','NIL'), 
    fail.
insertBIPBehavior(Process) :- 
    nl.

/* ---------------------------------------
 Function for BIP declaration of system (interface view structure)*/
insertBIPCompoundTypes(IV) :- 
    write('  // Compound types'), nl, 
    write('  compound type '), write('Syst'), write('()'), nl, 
    isTasteFunction(F,P,T,I,'terminal'), insertBIPFunctionComp(IV,F,P,T,I), 
    fail.
insertBIPCompoundTypes(IV) :- 
    nl, 
    insertBIPConnectors(IV), 
    fail.
insertBIPCompoundTypes(IV) :- 
    write('  end'), nl.

insertBIPFunctionComp(IV,Label,Pkg,Type,Impl) :- 
    write('    component '), write(Label), sp, write(Label), write('(').
insertBIPFunctionComp(IV,Label,Pkg,Type,Impl) :- 
    cpt2Rst, 
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    ( ( cpt2Get(N), N=0 ) -> cpt2Inc ; write(',') ),
    concat('PI','_',PIName,PIName2),
    getPIDeadline(Pkg2,PICompo,PIName2,Deadline),
    write(Deadline), write(','),
    getPIWcet(_,_,_,Pkg2,PIName2,_,Wcet),
    write(Wcet),
    fail.
insertBIPFunctionComp(IV,Label,Pkg,Type,Impl) :- 
    write(')'), nl, 
    isTastePI(Pkg,Type,L,_,T,K,P,D,_,Q), insertBIPPIComp(L,T,K,P,D,Q), 
    nl.
insertBIPFunctionComp(IV,Label,Pkg,Type,Impl) :- 
    isConnectedToAProtected(Pkg2,RICompo,RIName,Pkg,Type,PIName),
    not(alreadyDoneProtected(Type,PIName)),
    write('    component TASTE_PI_Protected '), write(Type), write('_'), write(PIName), write('()'), nl, 
    $assert(alreadyDoneProtected(Type,PIName)).

insertBIPPIComp(Label,Type,Kind,Period,Deadline,QSize) :- 
    Kind = 'cyclic', !, 
    write('    component TASTE_PI_Cyclic '), write(Label), 
    write('('), write(Period), write(','), write(Deadline), write(')').
insertBIPPIComp(Label,Type,Kind,Period,Deadline,QSize) :- 
    Kind = 'sporadic', !, 
    getBIPType(Type, BT), 
    write('    component TASTE_PI_Sporadic_'), write(BT), write(' '), write(Label),
    write('('), write(Period), write(','), write(Deadline), write(','), write(QSize), write(')').

insertBIPConnectors(IV) :- 
    cpt1Rst, 
    isTasteFunction(F,P,T,_,'terminal'), 
    isTastePI(P,T,L,_,_,'cyclic',_,_,_,_), 
    insertBIPConnector('Sync',L,'sig','_out',F,L,'_in'), 
    insertBIPConnector('Sync',F,L,'_return',L,'sig','_return'), 
    fail.
insertBIPConnectors(IV) :- 
    isTasteFunction(F,P,T,_,'terminal'), 
    isTastePI(P,T,L,_,_,'sporadic',_,_,_,_), 
    insertBIPConnector('Sync',F,L,'_return',L,'sig','_return'), 
    fail.
insertBIPConnectors(IV) :- 
    isConnectedToAProtected(Pkg2,RICompo,RIName,Pkg,PICompo,PIName),
    concat(PICompo,'_',PIName,L),
    concat(RICompo,'_',RIName,L2),
    write('    connector Sync '), write(RICompo), write('2'), write(L), write('_lock('),
    write(RICompo), write('.'), write(L2), write('_lock,'), write(L), write('.lock)'), nl,
    write('    connector Sync '), write(RICompo), write('2'), write(L), write('_release('),
    write(RICompo), write('.'), write(L2), write('_release,'), write(L), write('.release)'), nl,
    fail.
insertBIPConnectors(IV) :- 
    isTasteFunction(D,P,T,_,'terminal'), 
    isTastePI(P,_,V,_,BT,'sporadic',_,_,_,_), 
    cpt2Rst, 
    isTasteConnection(IV,S,A,D,B), 
    concat(S,'_',A,U), 
    concat(D,'_',B,V), 
    getBIPType(BT, BT1),
    concat('SendRec',BT1,CT1),
    concat('SendRecQueue',BT1,CT2), 
    insertBIPConnector(CT1,S,U,'_out',V,'sig','_out'), 
    cpt2Get(N), N=0, insertBIPConnector(CT2,V,'sig','_in',D,V,'_in'),
    cpt2Inc,
    fail.
insertBIPConnectors(IV) :- 
    true.

insertBIPConnector(Type,SComp,SPort,SSuffix,DComp,DPort,DSuffix) :- 
    cpt1Get(N), 
    write('    connector '), write(Type), write(' '), write(SComp), write('2'), write(DComp), write('('), %! write(' C'), write(N), write('('),
    write(SComp), dot, write(SPort), write(SSuffix), write(','), 
    write(DComp), dot, write(DPort), write(DSuffix), write(')'), nl, 
    cpt1Inc.

/* ---------------------------------------*/
insertBIPFooter :- 
    $told.

