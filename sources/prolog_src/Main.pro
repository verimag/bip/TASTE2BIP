/* H2020 ESROCOS/ERGO Projects
   Company: Ellidiss Technologies and Verimag Laboratory
   Licence: CeCILL-B */


go :- 
    $getenv('XMLFILE',Out), 
    ( $tell(Out) -> 
        true;
        errorMessage('Cannot open output file: ',Out,1) ), 
    genBIP, 
    pathName(Out,Directory,File),
    concat(Directory,'/ext-cpp/ExternInterfaceViewFcns.cpp',CppFile),
    $tell(CppFile),
    genCpp,
    $told,
    concat(Directory,'/ext-cpp/ExternInterfaceViewFcns.hpp',HppFile),
    $tell(HppFile),
    genHpp,
    $told,
    fail.
go :- 
    true.

errorMessage(Text1,Text2,Line) :- 
    print('Taste2BIP ERROR: ',Text1,Text2,'.').

genBIP :- 
    insertBIPHeader, 
    isTasteIV(I,F), insertBIPPackage(I,F),
    insertBIPFooter.

genHpp :- 
    write('#ifndef TE_HPP'), nl,
    write('#define TE_HPP'), nl,
    nl,
    write('#include <iostream>'), nl,
    write('#include <queue>'), nl,
    write('#include <chrono>'),nl,
    write('#include "C_ASN1_Types.h"'), nl,
    fail.
genHpp :- 
    getFunctionLanguage(Pkg,Type,'(C)'), 
    toLower(Type,Type2),
    write('#include "'), write(Type2), write('.h"'), nl,
    fail.
genHpp :- 
    getFunctionLanguage(Pkg,Type,'(CPP)'), 
    toLower(Type,Type2),
    write('#include "'), write(Type2), write('.h"'), nl,
    fail.
genHpp :- 
    nl,
    write('using namespace std;'), nl,
    nl,
    genCppQueueDef('Bool'), nl,
    genCppQueueDef('Int'), nl,
    genCppQueueDef('Float'), nl,
    genCppQueueDef('String'),
    write('void write(const char* s);'), nl,
    write('void writeln(const char* s);'), nl,
    nl,
    fail.
genHpp :- 
    getFunctionLanguage(Pkg,Type,'(C)'), 
    genCStartupFncSig(Type), write(';'), nl,
    fail.
genHpp :- 
    getFunctionLanguage(Pkg,Type,'(CPP)'), 
    genCStartupFncSig(Type), write(';'), nl,
    fail.
genHpp :- 
    getFunctionLanguage(Pkg,Type,'(C)'), 
    isTastePI(Type,PIName), 
    genCProtectedFncSig(Pkg,Type,PIName), write(';'), nl,
    fail.
genHpp :- 
    getFunctionLanguage(Pkg,Type,'(CPP)'), 
    isTastePI(Type,PIName), 
    genCProtectedFncSig(Pkg,Type,PIName), write(';'), nl,
    fail.
genHpp :- 
    nl, write('#endif'), nl.

genCppQueueDef(Type) :- 
    getCppType(Type, T), 
    write('// Queue and methods for '), write(Type), write(' values'), nl,
    write('typedef queue<'), write(T), write('> queue'), write(Type), write(';'), nl,
    nl,
    write('int get_size(const queue'), write(Type), write(' q);'), nl,
    write('void push_back(queue'), write(Type), write(' &q, '), write(T), write(' &val, const int &size);'), nl,
    write(T), write(' pop_front(queue'), write(Type), write(' &q);'), nl,
    write('ostream &operator<<(ostream &os, queue'), write(Type), write(' q);'), nl,
    write('void write(const '), write(T), write(' i);'), nl,
    write('void writeln(const '), write(T), write(' i);'), nl.

genCpp :- 
    write('#include "ExternInterfaceViewFcns.hpp"'), nl,
    nl,
    genCppQueueImplem('Bool'), nl,
    genCppQueueImplem('Int'), nl,
    genCppQueueImplem('Float'), nl,
    genCppQueueImplem('String'),
    write('void write(const char* s){'), nl,
    write('  cout<<s;'), nl,
    write('}'), nl,
    nl,
    write('void writeln(const char* s){'), nl,
    write('  cout<<s<<endl;'), nl,
    write('}'), nl,
    nl,
    fail.
genCpp :- 
    getFunctionLanguage(Pkg,Type,'(C)'), 
    genCStartupFncSig(Type), write('{'), nl,
    genCStartupFncBody(Type),
    write('}'), nl, nl,
    fail.
genCpp :- 
    getFunctionLanguage(Pkg,Type,'(CPP)'), 
    genCStartupFncSig(Type), write('{'), nl,
    genCStartupFncBody(Type),
    write('}'), nl, nl,
    fail.
genCpp :- 
    getFunctionLanguage(Pkg,Type,'(C)'), 
    isTastePI(Type,PIName), 
    genCProtectedFncSig(Pkg,Type,PIName), write('{'), nl,
    genCProtectedFncBody(Pkg,Type,PIName),
    write('}'), nl, nl,
    fail.
genCpp :- 
    getFunctionLanguage(Pkg,Type,'(CPP)'), 
    isTastePI(Type,PIName), 
    genCProtectedFncSig(Pkg,Type,PIName), write('{'), nl,
    genCProtectedFncBody(Pkg,Type,PIName),
    write('}'), nl, nl,
    fail.
genCpp :- 
    true.

genCppQueueImplem(Type) :- 
    getCppType(Type, T), 
    write('// Methods for (queue of) '), write(Type), nl,
    nl,
    write('int get_size(const queue'), write(Type), write(' q){'), nl,
    write('  return q.size();'), nl,
    write('}'), nl,
    nl,
    write('void push_back(queue'), write(Type), write(' &q, '), write(T), write(' &val, const int &size){'), nl,
    write('  if (get_size(q) < size) {'), nl,
    write('    q.push(val);'), nl,
    write('    cout<<"Queuing: "<<val<<endl;'), nl,
    write('  }'), nl,
    write('  else {'), nl,
    write('    cout<<"Queue full!"<<endl;'), nl,
    write('  }'), nl,
    write('}'), nl,
    nl,
    write(T), write(' pop_front(queue'), write(Type), write(' &q){'), nl,
    write('  '), write(T), write(' val;'), nl,
    nl,
    write('  if (get_size(q) > 0) {'), nl,
    write('    val = q.front();'), nl,
    write('    q.pop();'), nl,
    write('    cout<<"Popping: "<<val<<endl;'), nl,
    write('  }'), nl,
    write('  else {'), nl,
    write('    val = '), insertInitialValue(Type), write(';'), nl,
    write('    cout<<"Queue empty!"<<endl;'), nl,
    write('  }'), nl,
    nl,
    write('  return val;'), nl,
    write('}'), nl,
    nl,
    write('ostream &operator<<(ostream &os, queue'), write(Type), write(' q){'), nl,
    write('  queue'), write(Type), write(' tmp(q); '), nl,
    write('  while (!tmp.empty()) {'), nl,
    write('    os<<tmp.front()<<" ";'), nl,
    write('    tmp.pop();'), nl,
    write('  }'), nl,
    write('  return os;'), nl,
    write('}'), nl,
    nl,
    write('void write(const '), write(T), write(' i){'), nl,
    write('  cout<<i;'), nl,
    write('}'), nl,
    nl,
    write('void writeln(const '), write(T), write(' i){'), nl,
    write('  cout<<i<<endl;'), nl,
    write('}'), nl,
    nl.

getCppType(InType,CppType) :- 
    ( InType='Float' -> CppType='double' ; toLower(InType, CppType) ).

genCStartupFncSig(Type) :- 
    write('void '), write(Type), write('_startup_wrap()').

genCStartupFncBody(Type) :- 
    toLower(Type,Type2),
    write('  '), write(Type2), write('_startup();'), nl.

genCProtectedFncSig(Pkg,Type,PIName) :- 
    write('double '), write(Type),write('_'),write(PIName), write('_wrap('),
    cpt2Rst,
    concat('PI_',PIName,PIName2),
    isFeature('PARAMETER',Pkg,PIName2,Label,Dir,'NIL',C,_,_,_),
    ( ( cpt2Get(G), G=1 ) -> write(',');cpt2Inc),
    ( Dir = 'IN' -> write('const ');true),
    splitReference(C,ParamPkg,ParamType,ParamImpl), 
    getBIPType(ParamType,BIPType), toLower(BIPType,BIPType2), write(BIPType2),
    write(' &'), write(Label),
    fail.
genCProtectedFncSig(Pkg,Type,PIName) :- 
    write(')').

genCProtectedFncBody(Pkg,Type,PIName) :- 
    concat('PI_',PIName,PIName2),
    isFeature('PARAMETER',Pkg,PIName2,Label,Dir,'NIL',C,_,_,_),
    splitReference(C,ParamPkg,ParamType,ParamImpl), 
    write('  asn1Scc'), write(ParamType), write(' f_'), write(Label), write(' = (asn1Scc'), 
    write(ParamType), write(') '), write(Label), write(';'), nl,
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    write('  auto time_start = std::chrono::high_resolution_clock::now();'), nl,
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    toLower(Type,Type2),
    write('  '), write(Type2), write('_PI_'), write(PIName), write('('),
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    concat('PI_',PIName,PIName2), cpt2Rst,
    isFeature('PARAMETER',Pkg,PIName2,Label,Dir,'NIL',C,_,_,_),
    ( ( cpt2Get(N), N=0 ) -> cpt2Inc ; write(', ') ),
    splitReference(C,ParamPkg,ParamType,ParamImpl), 
    write('&f_'), write(Label),
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    write(');'), nl,
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    write('  auto time_end = std::chrono::high_resolution_clock::now();'), nl,
    write('  std::chrono::duration<double> time = time_end - time_start;'), nl,
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    concat('PI_',PIName,PIName2),
    isFeature('PARAMETER',Pkg,PIName2,Label,Dir,'NIL',C,_,_,_),
    Dir='OUT',
    splitReference(C,ParamPkg,ParamType,ParamImpl), 
    write('  '), write(Label), write(' = ('), 
    getBIPType(ParamType,BIPType), toLower(BIPType,BIPType2), write(BIPType2),
    write(') f_'), write(Label), write(';'), nl, 
    fail.
genCProtectedFncBody(Pkg,Type,PIName) :- 
    write('  return (time.count() * 1000);'), nl.

