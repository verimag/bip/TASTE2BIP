/* H2020 ESROCOS/ERGO Projects
   Company: Ellidiss Technologies and Verimag Laboratory
   Licence: CeCILL-B*/


insertBIPPlaces(Process) :- 
    cpt1Rst, 
    isBIPPlace(Process,P), cpt1Get(I), ( I=1 -> write('    place ') ; write(', ') ), write(P), 
    fail.
insertBIPPlaces(Process) :- 
    cpt1Get(I), 
    $globalset(lastPlaceNb(I)), 
    nl, nl.

insertBIPTransitions(Process,ProcedureId,ActionId) :- 
    insertBIPInitial(Process,ProcedureId,ActionId), 
    fail.
insertBIPTransitions(Process,ProcedureId,ActionId) :- 
    insertBIPProvInt(Process,ProcedureId,'START','NIL'),
    fail.
insertBIPTransitions(Process,ProcedureId,ActionId) :- 
    isSDLState(Process,ProcedureId,State,_),
    insertState(Process,ProcedureId,State),
    fail.
insertBIPTransitions(Process,ProcedureId,ActionId) :- 
    
true.

insertBIPNextTransition :- 
    cpt1Get(I), intToStr(I,F), cpt1Inc, 
    cpt1Get(J), lastPlaceNb(M), ( J < M -> intToStr(J,T); T = '0' ), 
    write('      from l'), write(F), write(' to l'), write(T), nl.

insertBIPNextTransition(SrcState,TgtState) :- 
    SrcState\='', TgtState\='',
    write('      from '), write(SrcState), write(' to '), write(TgtState), nl.
insertBIPNextTransition(SrcState,TgtState) :- 
    SrcState='', TgtState\='',
    cpt1Get(I), intToStr(I,F),  
    write('      from l'), write(F), write(' to '), write(TgtState), nl,
    cpt1Inc.
insertBIPNextTransition(SrcState,TgtState) :- 
    SrcState\='', TgtState='',
    cpt1Get(J), lastPlaceNb(M), ( J < M -> intToStr(J,T); T = '0' ), 
    write('      from '), write(SrcState), write(' to l'), write(T), nl.
insertBIPNextTransition(SrcState,TgtState) :- 
    SrcState='', TgtState='' ,
    cpt1Get(I), intToStr(I,F), cpt1Inc, 
    cpt1Get(J), lastPlaceNb(M), ( J < M -> intToStr(J,T); T = '0' ), 
    write('      from l'), write(F), write(' to l'), write(T), nl.

insertBIPInitial(Process,ProcedureId,ActionId) :- 
    isSDLNextState(Process,ProcedureId,'START',_,I,_),
    write('    initial to '), write(I), nl, 
    write('      do { '),
    fail.
insertBIPInitial(Process,ProcedureId,ActionId) :- 
    not(isSDLNextState(Process,ProcedureId,'START',_,I,_)),
    nl, write('    internal'), nl, 
    insertBIPNextTransition('',''),
    write('      do { '), 
    fail.
insertBIPInitial(Process,ProcedureId,ActionId) :- 
    isSDLVariable(Process,ProcedureId,D,T,V,_), V\='NIL',
    toLower(V,NV),
    ( ProcedureId\='NIL' -> ( write(ProcedureId), write('_') ) ; true ),
    write(D), write(' = '), write(NV), write('; '), 
    fail.
insertBIPInitial(Process,ProcedureId,ActionId) :- 
    cpt2Rst, cpt2Inc,
    isSDLProcedureParam(Process,ProcedureId,D,Kind,_,_), 
    ( ( Kind='IN' ; Kind='IN_OUT' ) -> 
        ( cpt2Get(N), isSDLActionParam(Process,_,_,_,ActionId,N,P,_), 
          write(ProcedureId), write('_'), write(D), write(' = '), write(P), write('; ') );
        true
    ),
    cpt2Inc.
insertBIPInitial(Process,ProcedureId,ActionId) :- 
    write('}'), nl.

insertState(Process,ProcedureId,State) :- 
    nl, write('    // Definition of state '), write(State), nl, 
    fail.
insertState(Process,ProcedureId,State) :- 
    isSDLInput(Process,ProcedureId,State,Input,_),
    insertBIPInput(Input,State,Process,ProcedureId),
    fail.
insertState(Process,ProcedureId,State) :- 
    isSDLContinuousSignal(Process,ProcedureId,State,Signal,Expression,_), 
    insertBIPContinuousSignal(Process,ProcedureId,State,Signal,Expression),
    fail.
insertState(Process,ProcedureId,State) :- 
    isTastePI(T,Input), 
    toLower(T,Process),
    isSDLState(Process,ProcedureId,State,_), 
    not( isInput(Process,State,Input) ),
    insertBIPCompletuteTransitions(Process,Input,State),
    fail.
insertState(Process,ProcedureId,State) :- 
    true.

insertBIPInput(Input) :- 
    isTastePI(_,T,L,P,_,_,_,_,_,_), 
    concat(T,'_',Input,N), memezTra(N,L), 
    nl, write('    on '), write(L), write('_in'), nl, 
    insertBIPNextTransition, 
    write('      eager'), nl.

insertBIPInput(Input,State,Process,ProcedureId) :- 
    isTastePI(_,T,L,P,_,_,_,_,_,_), 
    toLower(T,Process),
    concat(T,'_',Input,N), memezTra(N,L), 
    nl, write('    on '), write(L), write('_in'), nl, 
    insertBIPNextTransition(State,''),
    write('      eager'), nl,
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    isTastePI(_,T,L,P,_,_,_,_,_,_), 
    toLower(T,Process),
    isSDLInputParam(Process,ProcedureId,State,Input,Id,V,_),
    concat(T,'_',Input,N), memezTra(N,L), 
    nl, write('    internal'), nl, 
    insertBIPNextTransition('',''),
    write('      do { '), write(V), write(' = '), 
    write(L), write('_'), write(P), write('; }'), nl,
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    isSDLVariable(Process,ProcedureId,Input,T,_,_),
    toLower(T,Type), Type='timer',
    nl, write('    on silent'), nl,
    insertBIPNextTransition(State,''),
    write('      provided ('), write(Input), write(' == '), write(Input), write('_val)'), nl,
    write('      eager'), nl,
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    insertBIPProvInt(Process,ProcedureId,State,Input),
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    isSDLVariable(Process,ProcedureId,Input,T,_,_),
    toLower(T,Type), memezTra(Type,'timer'), 
    insertBIPReturnTimer(Input,State,Process,Input),
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    insertBIPReturn(Input,'sporadic',State,Process,Input),
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    insertBIPReturn(Input,'cyclic',State,Process,Input),
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    invariant(RIComp,N,RIName), memezTra(Process,RIComp), nl,
    write('    invariant inv'), write(N), write(' at l'), write(N), write(' provided ('), 
    write(RIComp), write('_comp_t <= '), write(RIName), write('_deadline)'), nl,
    N2 is N+1,
    write('    invariant inv'), write(N2), write(' at l'), write(N2), write(' provided ('), 
    write(RIComp), write('_comp_t <= '), write(RIName), write('_deadline)'), nl,
    N3 is N+2,
    write('    invariant inv'), write(N3), write(' at l'), write(N3), write(' provided ('), 
    write(RIComp), write('_comp_wait <= '), write(RIName), write('_wcet && '), write(RIComp), write('_comp_t <= '), 
    write(RIName), write('_deadline)'), nl,
    N4 is N+3, 
    write('    invariant inv'), write(N4), write(' at l'), write(N4), write(' provided ('), 
    write(RIComp), write('_comp_t <= '), write(RIName), write('_deadline)'), nl,
    fail.
insertBIPInput(Input,State,Process,ProcedureId) :- 
    true.

insertBIPContinuousSignal(Process,ProcedureId,State,Signal,Expression) :- 
    nl, write('    on silent'),
    nl, insertBIPNextTransition(State,''),
    write('      provided ('), write(Expression), write(')'),
    nl, write('      eager'),
    insertBIPProvInt(Process,ProcedureId,State,Signal),
    fail.
insertBIPContinuousSignal(Process,ProcedureId,State,Signal,Expression) :- 
    isSDLNextState(Process,ProcedureId,State,Signal,S,_),
    nl, write('    internal'),
    nl, insertBIPNextTransition('',S).

insertBIPProvInt(Process,ProcedureId,State,Input) :- 
    isSDLAction(Process,ProcedureId,State,Input,Input,ActionId,ActionKind,Label,_), 
    insertBIPAction(Process,ProcedureId,State,Input,Input,ActionId,ActionKind,Label),
    fail.

insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    memezTra(ActionKind,'CALL'), 
    not(isSDLProcedure(Process,Label,_)),
    ( ( isTasteRI(Type,RIName), memezTra(Type,Process), memezTra(RIName,Label) ) ->
          not(isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName));true
    ),
    insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label),
    fail.
insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    memezTra(ActionKind,'CALL'), 
    not(isSDLProcedure(Process,Label,_)),
    isTasteRI(Type,RIName), memezTra(Type,Process), memezTra(RIName,Label),
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    isTasteFunction(Func,_,_,_,_), memezTra(Func,Process),
    insertBIPCallProtected(Type,RIName,PICompo,PIName,ActionId),
    fail.
insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    memezTra(ActionKind,'CALL'), 
    isSDLProcedure(Process,Label,_),
    insertBIPProcedure(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label),
    fail.
insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    memezTra(ActionKind,'TASK'), 
    insertBIPTask(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label),
    fail.
insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    memezTra(ActionKind,'OUTPUT'), 
    insertBIPOutput(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label),
    fail.
insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    memezTra(ActionKind,'DECISION'), 
    cpt1Get(I), intToStr(I,F), concat('l',F,EnterState), cpt1Inc, 
    getDecisionExitState(ActionId,ExitState),
    insertBIPDecision(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label,EnterState,ExitState),
    fail.
insertBIPAction(Process,ProcedureId,State,Input,ParentActionId,ActionId,ActionKind,Label) :- 
    true.

/*Deprecated function*/
insertBIPOutput(Output) :- 
    isTasteRI(_,T,L,P,_), 
    concat(T,'_',Output,N), memezTra(N,L), 
    nl, write('    on '), write(L), write('_out'), nl, 
    insertBIPNextTransition, 
    write('      eager'), nl.

insertBIPOutput(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    isTasteRI(_,T,L,P,X), 
    toLower(T,Process),
    getBIPType(X, BT), memezTra(BT, 'Nil'), 
    concat(T,'_',Label,N), memezTra(N,L), 
    nl, write('    on '), write(L), write('_out'), nl, 
    insertBIPNextTransition, 
    write('      eager'), nl,
    fail.
insertBIPOutput(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    isTasteRI(_,T,L,P,X), 
    toLower(T,Process),
    getBIPType(X, BT), BT\='Nil',
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,V,_),
    concat(T,'_',Label,N), memezTra(N,L), 
    nl, write('    internal'), nl,
    insertBIPNextTransition('',''), 
    write('      do { '), write(L), write('_'), write(P), 
    write(' = '), write(V), write('; }'), nl,
    nl, 
    write('    on '), write(L), write('_out'), nl, 
    insertBIPNextTransition('',''), 
    write('      eager'), nl,
    fail.

insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    nl, write('    on silent'), nl, 
    insertBIPNextTransition('',''), 
    write('      eager'), nl.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    L\='reset_timer',
    L\='set_timer',
    L\='writeln',
    L\='write',
    write('      do { '), write(Call), write('('), 
    ( isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,P,_) -> writeBIP(P); fail ), 
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,Q,_), P \= Q, 
    write(','), writeBIP(Q), 
    fail.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'set_timer'),
    write('      do { '), 
    ( isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,P,_) -> !; fail),
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,Q,_), P \= Q, 
    write(Q), write(' = 0; '), write(Q), write('_val = '), writeBIP(P),
    fail.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'reset_timer'),
    write('      do { '), 
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,P,_), 
    write(P), write(' = 0; '), write(P), write('_val = 0'), 
    fail.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'writeln'),
    write('      do { '), 
    getActionParameter(Process,State,Input,ActionId,Parameters),
    insertBIPWriteln(Parameters),
    fail.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'write'),
    write('      do { '), 
    getActionParameter(Process,State,Input,ActionId,Parameters),
    insertBIPWrite(Parameters),
    fail.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    L\='reset_timer',
    L\='set_timer',
    L\='writeln',
    L\='write',
    write('); }'), nl.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'set_timer'),
    write('; }'), nl.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'reset_timer'),
    write('; }'), nl.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'writeln'),
    write('; }'), nl.
insertBIPCall(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    toLower(Label, L),
    memezTra(L,'write'),
    write('; }'), nl.

insertBIPCallProtected(RIComp,RIName,PIComp,PIName,CallId) :- 
    nl, write('    internal'), nl, 
    insertBIPNextTransition('',''), 
    write('      do { '), write(RIComp), write('_comp_t = 0; }'), nl,
    cpt1Get(N), $assert(invariant(RIComp,N,RIName)),
    fail.
insertBIPCallProtected(RIComp,RIName,PIComp,PIName,CallId) :- 
    nl, write('    on '), write(RIComp), write('_'), write(RIName), write('_lock'),nl, 
    insertBIPNextTransition('',''), 
    write('      eager'), nl,
    fail.
insertBIPCallProtected(RIComp,RIName,PIComp,PIName,CallId) :- 
    nl, write('    on silent'), nl,
    insertBIPNextTransition('',''), 
    write('      eager'), nl,
    write('      do { '), write(RIComp), write('_comp_wait = 0; '),
    write(RIComp), write('_comp_time = '), write(PIComp), write('_'), write(PIName), write('_wrap('),
    isSDLActionParam(_,_,_,_,CallId,I,P,_), 
    ( I\=1 -> write(',');true ), write(P),
    fail.
insertBIPCallProtected(RIComp,RIName,PIComp,PIName,CallId) :- 
    write('); }'), nl, nl,
    fail.
insertBIPCallProtected(RIComp,RIName,PIComp,PIName,CallId) :- 
    write('    on silent'), nl,
    insertBIPNextTransition('',''), 
    write('      provided ('), write(RIComp), write('_comp_wait == '), write(RIComp), write('_comp_time)'), nl,
    write('      eager'), nl, nl,
    fail.
insertBIPCallProtected(RIComp,RIName,PIComp,PIName,CallId) :- 
    write('    on '), write(RIComp), write('_'), write(RIName), write('_release'), nl,
    insertBIPNextTransition('',''), 
    write('      eager'), nl,
    write('      do { '), write(RIComp), write('_comp_t = 0; }'), nl.

insertBIPProcedure(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    insertBIPTransitions(Process,Label,ActionId), 
    fail.
insertBIPProcedure(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    nl, write('    internal'), nl,
    insertBIPNextTransition('',''),
    write('      do { '),
    fail.
insertBIPProcedure(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    cpt2Rst, cpt2Inc,
    isSDLProcedureParam(Process,Label,V,Kind,_,_), 
    ( ( Kind='OUT' ; Kind='IN_OUT' ) -> 
        ( cpt2Get(N), isSDLActionParam(Process,_,_,_,ActionId,N,P,_), 
          write(P), write(' = '), write(Label), write('_'), write(V), write('; ') );
        true
    ),
    cpt2Inc.
insertBIPProcedure(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    write('}'), nl,
    fail.

insertBIPTask(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    nl, write('    on silent'), nl, 
    insertBIPNextTransition('',''), 
    write('      eager'), nl,
    write('      do { '),
    fail.
insertBIPTask(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,P,_),
    write(P), write('; '),
    fail.
insertBIPTask(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label) :- 
    write('}'), nl.

insertBIPDecision(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label,EnterState,ReturnState) :-  
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,Question,_),
    isSDLAction(Process,ProcedureId,State,Input,ActionId,AnswerId,'ANSWER',Answer,_),
    insertBIPAnswer(Process,ProcedureId,State,Input,AnswerId,Question,Answer,EnterState,ReturnState).
insertBIPDecision(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label,EnterState,ReturnState) :-  
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,_,Question,_),
    isSDLAction(Process,ProcedureId,State,Input,ActionId,ElseId,'ELSE','',_),
    insertBIPElse(Process,ProcedureId,State,Input,ActionId,ElseId,Question,EnterState,ReturnState).
insertBIPDecision(Process,ProcedureId,State,Input,ParentActionId,ActionId,Label,EnterState,ReturnState) :- 
    ReturnState\='',
    nl, write('    internal'), nl,
    insertBIPNextTransition(ReturnState,'').

insertBIPAnswer(Process,ProcedureId,State,Input,AnswerId,Question,Answer,EnterState,ReturnState) :- 
    toLower(Answer, A),
    nl, write('    on silent'), nl, 
    insertBIPNextTransition(EnterState,''), 
    write('      provided (('), write(Question), write(') == '), write(A), write(')'), nl,
    write('      eager'), nl,
    isSDLAction(Process,ProcedureId,State,Input,AnswerId,ChildActionId,ChildActionKind,ChildLabel,_),
    insertBIPAction(Process,ProcedureId,State,Input,AnswerId,ChildActionId,ChildActionKind,ChildLabel),
    fail.
insertBIPAnswer(Process,ProcedureId,State,Input,AnswerId,Question,Answer,EnterState,ReturnState) :- 
    insertBIPAnswerEnd(Process,ProcedureId,State,Input,AnswerId,ReturnState).

insertBIPAnswerEnd(Process,ProcedureId,State,Input,AnswerId,ReturnState) :- 
    isSDLNextState(Process,ProcedureId,State,AnswerId,NextState,_),
    not(isSDLVariable(Process,ProcedureId,Input,'TIMER',_,_)),
    isTastePI(T,L), toLower(T,Process),
    memezTra(Input,L),
    nl, write('    on '), write(T), write('_'), write(L), write('_return'), nl, 
    insertBIPNextTransition('',NextState),
    write('      eager'), nl.
insertBIPAnswerEnd(Process,ProcedureId,State,Input,AnswerId,ReturnState) :- 
    isSDLNextState(Process,ProcedureId,State,AnswerId,NextState,_),
    isSDLVariable(Process,ProcedureId,Input,'TIMER',_,_),
    nl, write('    on silent'), nl, 
    insertBIPNextTransition('',NextState),
    write('      eager'), nl.
insertBIPAnswerEnd(Process,ProcedureId,State,Input,AnswerId,ReturnState) :- 
    not(isSDLNextState(Process,ProcedureId,State,AnswerId,NextState,_)),
    nl, write('    internal'), nl,
    insertBIPNextTransition('',ReturnState).

insertBIPElse(Process,ProcedureId,State,Input,ActionId,ElseId,Question,EnterState,ReturnState) :- 
    nl, write('    on silent'), nl, 
    insertBIPNextTransition(EnterState,''), 
    insertBIPElseCondition(Process,ProcedureId,State,Input,ActionId,ElseId,Question),
    write('      eager'), nl,
    isSDLAction(Process,ProcedureId,State,Input,ElseId,ChildActionId,ChildActionKind,ChildLabel,_),
    insertBIPAction(Process,ProcedureId,State,Input,ElseId,ChildActionId,ChildActionKind,ChildLabel),
    fail.
insertBIPElse(Process,ProcedureId,State,Input,ActionId,ElseId,Question,EnterState,ReturnState) :- 
    insertBIPAnswerEnd(Process,ProcedureId,State,Input,ElseId,ReturnState).

insertBIPElseCondition(Process,ProcedureId,State,Input,ActionId,ElseId,Question) :- 
    $globalset(bipElseCondition('')),
    write('      provided (!('),
    fail.
insertBIPElseCondition(Process,ProcedureId,State,Input,ActionId,ElseId,Question) :- 
    isSDLAction(Process,ProcedureId,State,Input,ActionId,_,'ANSWER',Answer,_),
    bipElseCondition(L),  write(L),
    write('(('),write(Question), write(') == '), write(Answer), write(')'),
    $globalset(bipElseCondition(' || ')),
    fail.
insertBIPElseCondition(Process,ProcedureId,State,Input,ActionId,ElseId,Question) :- 
    $globalset(bipElseCondition('')),
    write('))'), nl.

insertBIPReturn(Input,Kind) :- 
    isTastePI(_,T,L,_,_,Kind,_,_,_,_), 
    concat(T,'_',Input,N), memezTra(N,L), 
    nl, write('    on '), write(L), write('_return'), nl, 
    insertBIPNextTransition, 
    write('      eager'), nl.

insertBIPReturn(Input,Kind,State,Process,ParentActionId) :- 
    isTastePI(_,T,L,_,_,Kind,_,_,_,_), 
    toLower(T,Process),
    concat(T,'_',Input,N), memezTra(N,L), 
    isSDLNextState(Process,'NIL',State,ParentActionId,S,_), !,
    nl, write('    on '), write(L), write('_return'), nl, 
    insertBIPNextTransition('',S), 
    write('      eager'), nl.

insertBIPReturnTimer(Input,State,Process,ParentActionId) :- 
    isSDLVariable(Process,'NIL',Input,T,_,_),
    toLower(T,Type), memezTra(Type,'timer'),
    isSDLNextState(Process,'NIL',State,ParentActionId,S,_), !,
    nl, write('    internal'), nl, 
    insertBIPNextTransition('',S), 
    write('').

isBIPPlace(Process,Place) :- 
    isBIPProcessPlace(Process,Place).
isBIPPlace(Process,Place) :- 
    isSDLProcedure(Process,ProcedureId,_),
    isSDLAction(Process,'NIL',_,_,_,_,'CALL',ProcedureId,_),
    isBIPProcedureCallPlace(Process,ProcedureId,Place).

isBIPProcessPlace(Process,Place) :- 
    isSDLState(Process,'NIL',State,_), 
    cpt1Get(I), intToStr(I,N), cpt1Inc,
    Place=State.
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',_,_,_,_,'OUTPUT',_,_), 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',State,Input,_,ActionId,'OUTPUT',_,_), 
    isSDLActionParam(Process,'NIL',State,Input,ActionId,1,_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',State,Input,Input,_,'DECISION',_,_),
    isSDLNextState(Process,'NIL',State,Input,_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',State,Input,Input,_,'DECISION',_,_),
    isSDLNextState(Process,'NIL',State,Input,_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLInput(Process,'NIL',S1,Input,L1), 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLInput(Process,'NIL',State,Input,_),  
    isSDLInputParam(Process,'NIL',State,Input,_,_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLContinuousSignal(Process,'NIL',State,Signal,_,_), 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',_,_,_,_,T,_,_),
    T\='OUTPUT', T\='DECISION',
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isTastePI(T,Input), 
    toLower(T,Process),
    isSDLState(Process,'NIL',State,_), 
    not( isInput(Process,State,Input) ),
    cpt1Get(I), intToStr(I,N), cpt1Inc,
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',_,_,_,_,'CALL',Label,_), 
    not(isSDLProcedure(Process,Label,_)),
    isTasteRI(Type,RIName), memezTra(Type,Process), memezTra(RIName,Label),
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    isTasteFunction(Func,_,_,_,_), memezTra(Func,Process),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',_,_,_,_,'CALL',Label,_), 
    not(isSDLProcedure(Process,Label,_)),
    isTasteRI(Type,RIName), memezTra(Type,Process), memezTra(RIName,Label),
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    isTasteFunction(Func,_,_,_,_), memezTra(Func,Process),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',_,_,_,_,'CALL',Label,_), 
    not(isSDLProcedure(Process,Label,_)),
    isTasteRI(Type,RIName), memezTra(Type,Process), memezTra(RIName,Label),
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    isTasteFunction(Func,_,_,_,_), memezTra(Func,Process),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcessPlace(Process,Place) :- 
    isSDLAction(Process,'NIL',_,_,_,_,'CALL',Label,_), 
    not(isSDLProcedure(Process,Label,_)),
    isTasteRI(Type,RIName), memezTra(Type,Process), memezTra(RIName,Label),
    isConnectedToAProtected(Pkg,Type,RIName,Pkg2,PICompo,PIName),
    isTasteFunction(Func,_,_,_,_), memezTra(Func,Process),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).

isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLState(Process,ProcedureId,State,_), 
    cpt1Get(I), intToStr(I,N), cpt1Inc,
    Place=State.
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLAction(Process,ProcedureId,_,_,_,_,'OUTPUT',_,_), 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLAction(Process,ProcedureId,State,Input,_,ActionId,'OUTPUT',_,_), 
    isSDLActionParam(Process,ProcedureId,State,Input,ActionId,1,_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLAction(Process,ProcedureId,State,Input,Input,_,'DECISION',_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLAction(Process,ProcedureId,State,Input,Input,_,'DECISION',_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLInput(Process,ProcedureId,S1,Input,L1), 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLInput(Process,ProcedureId,State,Input,_),  
    isSDLInputParam(Process,ProcedureId,State,Input,_,_,_),
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLContinuousSignal(Process,ProcedureId,State,Signal,_,_), 
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLAction(Process,ProcedureId,_,_,_,_,T,_,_),
    T\='OUTPUT', T\='DECISION',
    cpt1Get(I), intToStr(I,N), cpt1Inc, 
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isTastePI(T,Input), 
    toLower(T,Process),
    isSDLState(Process,ProcedureId,State,_), 
    not( isInput(Process,State,Input) ),
    cpt1Get(I), intToStr(I,N), cpt1Inc,
    concat('l',N,Place).
isBIPProcedureCallPlace(Process,ProcedureId,Place) :- 
    isSDLProcedure(Process,ProcedureId2,_),
    isSDLAction(Process,ProcedureId,_,_,_,_,'CALL',ProcedureId2,_),
    isBIPProcedureCallPlace(Process,ProcedureId2,Place).

isBIPNextInput(Process,Pred,Succ,Input) :- 
    isSDLNextState(Process,'NIL',Pred,_,Succ,_), 
    isSDLInput(Process,'NIL',Succ,Input,_).

/*to replace single quotes into double quotes*/
writeBIP(Value) :- 
    $name(Value,[39,39|L]), /* Value starts with '' */
    $reverse(L,[39,39|R]), /* Value ends with '' */
    $reverse(R,S), $name(V,S), 
    write('"'), write(V), write('"'), !.
writeBIP(Value) :- 
    write(Value).

writeBIPTask(Task) :- 
    $name(Task,[58,61|L]), /* Value starts with := */
    $name(V,L), 
    write('='), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[60,61|L]), /* Value starts with <= */
    $name(V,L), 
    write('<='), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[62,61|L]), /* Value starts with >= */
    $name(V,L), 
    write('>='), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[61|L]), /* Value starts with = */
    $name(V,L), 
    write('=='), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[77,79,68|L]), /* Value starts with MOD */
    $name(V,L), 
    write('%'), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[65,78,68|L]), /* Value starts with AND */
    $name(V,L), 
    write('&&'), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[79,82|L]), /* Value starts with OR */
    $name(V,L), 
    write('||'), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[78,79,84|L]), /* Value starts with NOT */
    $name(V,L), 
    write('!'), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[A|L]), L\=[],
    $name(V,L), $name(A2,[A]),
    write(A2), !, writeBIPTask(V).
writeBIPTask(Task) :- 
    $name(Task,[A|L]), L=[],
    $name(A2,[A]),
    write(A2).

initPlaceCpt(Process) :- 
    isSDLState(Process,'NIL',S,_), cpt1Inc.

insertBIPCompletuteTransitions(Process,Input,State) :- 
    isTastePI(T,L), toLower(T,Process),
    memezTra(Input,L),
    nl, write('    on '), write(T), write('_'), write(L), write('_in'), nl, 
    insertBIPNextTransition(State,''),
    write('      eager'), nl, nl,
    write('    on '), write(T), write('_'), write(L), write('_return'), nl, 
    insertBIPNextTransition('',State), 
    write('      eager'), nl.

getActionParameter(Process,State,Input,ActionId,Parameters) :- 
    $findall(P,isSDLActionParam(Process,'NIL',State,Input,ActionId,_,P,_),PS),
    Parameters=PS.

insertBIPWriteln(Parameters) :- 
    Parameters\=[],
    Parameters=[L|P] , P\=[],
    write('write('), writeBIP(L), write('); '), 
    insertBIPWriteln(P).
insertBIPWriteln(Parameters) :- 
    Parameters\=[],
    Parameters=[L|P] ,$length(P,0),
    write('writeln('), writeBIP(L), write(')').

insertBIPWrite(Parameters) :- 
    Parameters\=[],
    Parameters=[L|P] , P\=[],
    write('write('), writeBIP(L), write('); '), 
    insertBIPWrite(P).
insertBIPWrite(Parameters) :- 
    Parameters\=[],
    Parameters=[L|P] ,$length(P,0),
    write('write('), writeBIP(L), write(')').

getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_int8'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_uint8'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_int16'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_uint16'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_int32'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_uint32'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_int64'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_uint64'), BIPType='Int'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'t_boolean'), BIPType='Bool'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    memezTra(T,'nil'), BIPType='Nil'.
getBIPType(InType,BIPType) :- 
    toLower(InType,T), 
    not(memezTra(T,'t_int8')), not(memezTra(T,'t_uint8')), 
    not(memezTra(T,'t_int16')),  not(memezTra(T,'t_uint16')),
    not(memezTra(T,'t_int32')), not(memezTra(T,'t_uint32')), 
    not(memezTra(T,'t_int64')),  not(memezTra(T,'t_uint64')),
    not(memezTra(T,'t_boolean')), not(memezTra(T,'nil')),
    BIPType=InType.

getDecisionExitState(DecisionId,ExitState) :- 
    isSDLAction(_,_,_,_,DecisionId,ChildId,'ANSWER',_,_),
    not(isSDLNextState(_,_,_,ChildId,_,_)),!,
    cpt1Get(I2), intToStr(I2,F2), concat('l',F2,ExitState), cpt1Inc.
getDecisionExitState(DecisionId,ExitState) :- 
    ExitState='', true.

isInput(Process,State,PI) :- isSDLInput(Process,'NIL',State,Input,_), memezTra(Input,PI).

