/* H2020 ESROCOS/ERGO Projects
   Company: Ellidiss Technologies and Verimag Laboratory
   Licence: CeCILL-B*/


isTasteIV(Label,File) :- 
    getProject(P,N,File),  /* get main package info */
    isComponentImplementation(P,_,Label,_,'SYSTEM',_,_,_), 
    memezTra(Label,N).

isTasteFunction(Label,Pkg,Type,Impl,Kind) :- 
    isSubcomponent(_,_,_,Label,'SYSTEM',C,_,_,_), 
    splitReference(C,Pkg,Type,Impl), 
    ( isSubcomponent(Pkg,Type,Impl,_,'SYSTEM',_,_,_,_) -> 
      Kind = 'non-terminal';
      Kind = 'terminal' ).

isTastePI(PPkg,PType,Label,Param,Type,Kind,Period,Deadline,Wcet,QSize) :- 
    isFeature('ACCESS',PPkg,PType,F,'PROVIDES','SUBPROGRAM',C,_,_,_), 
    getPILabel(PPkg,PType,F,L), 
    /* prefix to solve lack of namespace in BIP */
    concat(PType,'_',L,Label), 
    getPIKind(PPkg,PType,F,Kind), 
    getPIPeriod(PPkg,PType,F,Period), 
    getPIDeadline(PPkg,PType,F,Deadline), 
    splitReference(C,P,T,I), 
    getPIWcet(PPkg,PType,F,P,T,I,Wcet), 
    getPIRIParam(P,T,Param,'IN',_,Type,_), 
    getPIQueueSize(P,T,QSize).

isTastePI(PType,Label) :- isFeature('ACCESS',PPkg,PType,F,'PROVIDES','SUBPROGRAM',C,_,_,_), 
    getPILabel(PPkg,PType,F,Label).

isTasteRI(PPkg,PType,Label,Param,Type) :- 
    isFeature('ACCESS',PPkg,PType,F,'REQUIRES','SUBPROGRAM',C,_,_,_), 
    splitReference(C,P,T,_), 
    getRILabel(PPkg,PType,F,L), 
    /* prefix to solve lack of namespace in BIP */
    concat(PType,'_',L,Label), 
    getPIRIParam(P,T,Param,'IN',_,Type,_).

isTasteRI(PType,Label) :- isFeature('ACCESS',PPkg,PType,F,'REQUIRES','SUBPROGRAM',C,_,_,_), 
    getPILabel(PPkg,PType,F,Label).

isTasteConnection(IV,SComp,SPort,DComp,DPort) :- 
    isConnection('ACCESS',_,IV,_,_,S,_,D,_,_), 
    /* Source and Destination are reversed in ACCESS connections */
    splitName(D,A,SComp), removePrefix(A,SPort), 
    splitName(S,B,DComp), removePrefix(B,DPort).

getProject(Package,Project,File) :- 
    isPackage(Package,'PUBLIC',_),
    $name(Package,L1), 
    ( $reverse(L1,[86,73,58,58|R]) /* VI:: */;
      $reverse(L1,[118,105,58,58|R]) /* vi:: */ ), 
    $reverse(R,L2), 
    $name(Project,L2), 
    ( $getenv('XMLFILE',F) -> splitPathName(F,_,File); File = Project ).

getPILabel(PPkg,PType,PI,Label) :- 
    /* if InterfaceName property exists, use its value as label 
       use isProperty to keep the label case */
    isProperty(_,_,PPkg,PType,'NIL',PI,P,V,_), 
    memezTra(P,'Taste::InterfaceName'), 
    stripString(V,Label), !.
getPILabel(PPkg,PType,PI,Label) :- 
    /* otherwise, use the AADL feature identifier as label */
    Label = PI.

getPIKind(PPkg,PType,PI,Kind) :- 
    isProperty(_,_,PPkg,PType,'NIL',PI,P,V,_), 
    memezTra(P,'Taste::RCMoperationKind'), 
    stripString(V,K), toLower(K,Kind), !.
getPIKind(PPkg,PType,PI,Kind) :- 
    Kind = 'unprotected'.

getPIPeriod(PPkg,PType,PI,Period) :- 
    toUpper(PI,P), 
    isPropertyOpt(_,_,PPkg,PType,'NIL',P,'TASTE::RCMPERIOD',R,_), 
    stripString(R,S), splitTime(S,Period,_), !.
getPIPeriod(PPkg,PType,PI,Period) :- 
    Period = '0'.

getPIDeadline(PPkg,PType,PI,Deadline) :- 
    toUpper(PI,P), 
    isPropertyOpt(_,_,PPkg,PType,'NIL',P,'TASTE::DEADLINE',D,_), 
    stripString(D,S), splitTime(S,Deadline,_), !.
getPIDeadline(PPkg,PType,PI,Deadline) :- 
    Deadline = '0'.

getPIWcet(PPkg,PType,PI,Pkg,Type,Impl,Wcet) :- 
    /* look at the subprogram component implementation */
    isPropertyOpt(_,_,Pkg,Type,Impl,'NIL','COMPUTE_EXECUTION_TIME',W,_), 
    W \= '', getMinMax(W,_,M), splitTime(M,Wcet,_), !.
getPIWcet(PPkg,PType,PI,Pkg,Type,Impl,Wcet) :- 
    /* otherwise look at the subprogram component type */
    isPropertyOpt(_,_,Pkg,Type,'NIL','NIL','COMPUTE_EXECUTION_TIME',W,_), 
    W \= '', getMinMax(W,_,M), splitTime(M,Wcet,_), !.
getPIWcet(PPkg,PType,PI,Pkg,Type,Impl,Wcet) :- 
    /* otherwise look at the subprogram subcomponent */
    concat(PI,'_impl',S), toUpper(S,Z), 
    isPropertyOpt(_,_,PPkg,PType,'others',Z,'COMPUTE_EXECUTION_TIME',W,_), 
    W \= '', getMinMax(W,_,M), splitTime(M,Wcet,_), !.
getPIWcet(PPkg,PType,PI,Pkg,Type,Impl,Wcet) :- 
    /* upwards compatibility (remove PI_ prefix) */
    $name(PI,[80,73,95|L]), $name(N,L), 
    concat(N,'_impl',S), toUpper(S,Z), 
    isPropertyOpt(_,_,PPkg,PType,'others',Z,'COMPUTE_EXECUTION_TIME',W,_), 
    W \= '', getMinMax(W,_,M), splitTime(M,Wcet,_), !.
getPIWcet(PPkg,PType,PI,Pkg,Type,Impl,Wcet) :- 
    Wcet = '0'.

getPIQueueSize(Pkg,Type,Size) :- 
    isPropertyOpt(_,_,Pkg,Type,'NIL','NIL','TASTE::ASSOCIATED_QUEUE_SIZE',Size,_), !.
getPIQueueSize(Pkg,Type,Size) :- 
    Size = '1'.

/*(cf. REQ_TASTE_115)*/
getRILabel(PPkg,PType,RI,Label) :- 
    /* if InterfaceName property exists, use its value as label 
       use isProperty to keep the label case */
    isProperty(_,_,PPkg,PType,'NIL',RI,P,V,_), 
    memezTra(P,'Taste::InterfaceName'), 
    stripString(V,Label), !.
getRILabel(PPkg,PType,RI,Label) :- 
    /* otherwise, use the AADL feature identifier as label */
    Label = RI.

getPIRIParam(PPkg,PType,Label,Dir,Pkg,Type,Impl) :- 
    isFeature('PARAMETER',PPkg,PType,Label,Dir,'NIL',C,_,_,_), !,  
    splitReference(C,Pkg,Type,Impl).
getPIRIParam(PPkg,PType,Label,Dir,Pkg,Type,Impl) :- 
    Label = 'NIL', 
    Pkg = 'NIL', 
    Type = 'NIL', 
    Impl = 'NIL'.

removePrefix(Long,Short) :- 
    /* case PI_ */
    $name(Long,[80,73,95|L]), $name(Short,L), !.
removePrefix(Long,Short) :- 
    /* case RI_ */
    $name(Long,[82,73,95|L]), $name(Short,L), !.
removePrefix(Long,Short) :- 
    /* otherwise */
    Short = Long.

splitPathName(Path,Directory,File) :- 
    $name(Path,L1), $reverse(L1,R1), $name(Htap,R1), 
    $conlength(Path,L), 
    $subdelim(1,'/',Elif,0,Htap,P), 
    N is L - P, 
    $substring(1,N,Yrotcerid,P,Htap,L), 
    $name(Elif,R2), $reverse(R2,L2), $name(File,L2), 
    $name(Yrotcerid,R3), $reverse(R3,L3), $name(Directory,L3).

isConnectedToAProtected(RIPkg,RICompo,RIName,PIPkg,PICompo,PIName) :- 
    /*isTasteRI(RICompo,RIName),*/
    
isFeature('ACCESS',RIPkg,RICompo,F,'REQUIRES','SUBPROGRAM',_,_,_,_), 
    getRILabel(RIPkg,RICompo,F,RIName),
    isTasteConnection(IV,RICompo,RIName,PICompo,PIName),
    isFeature('ACCESS',PIPkg,PICompo,F2,'PROVIDES','SUBPROGRAM',_,_,_,_), 
    getPILabel(PIPkg,PICompo,F2,PIName),
    getPIKind(PIPkg,PICompo,F2,Kind),
    Kind='protected'.

getFunctionLanguage(Pkg,Type,Language) :- isProperty(_,_,Pkg,Type,_,_,'Source_Language',Language,_).

