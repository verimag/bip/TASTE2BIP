package provide taste2bip 0.1 

lappend auto_path .
namespace eval taste2bip {
    
    # Graphical name of the operation
    proc getLabel {} {
        return "Translate IV into BIP"
    }
    
    # Name of the application this script can be used with
    # shall be either InterfaceView or DeploymentView
    proc getApplication {} {
        return "InterfaceView"
    }
    
    # Names of  the object this script can be used on
    # FIXME - it should be active all the time
    proc getApplyTo {} {
        return [list "alwayson" ]
    }
    
    # List of way to manage output in the Framework
    # Could be an empty list or one or both of 'dialogBox' and 'statusBar'
    proc getOutputManagement {} {
		## Ticket mantis 0000625
        return [list dialogBoxOnError ]
    }
    
    proc taste2bip { args } {
		return [taste2bip_internal ]
    }
    
    #  the line "exec {*}[auto_execok $::installationPath/config/externalTools/test.bat]"
    #  ask the current OS which software is to be used to open the file test.bat
    #  to launch using the absolute path, read the template2.tcl_
    
    # synchronous call
    proc taste2bip_internal { } { 
        set ivFilename [ lindex [::Object::getAttribute "interfaceview" "filenames" ] 0]
        #~ set dvFilename [ lindex [::Object::getAttribute "deploymentview" "filenames" ] 0]
        #~ set dtFilename [lindex [Object::getAttribute "dataview" "filenames" ] 0 ]
        #~ set cvFilename "[LogManager::getLogDirectory]/concurrencyview.aadl"
        
        set ivFactsFile "[file rootname $ivFilename].sbp"
        set ivBipFile "[file rootname $ivFilename].bip"
        set ivDirname [file dirname $ivFilename]
        
        file mkdir "$ivDirname/ext-cpp"
        
        set diagramID [Object::getAttribute "interfaceview" "diagramID"]
        set rootID [Object::getAttribute $diagramID "rootID"]
        
        set SDLFiles {}
        foreach func [ HierarchicalObject::getTerminalChildrenByClass $rootID FunctionObject ] {
			if { [Object::getAttribute $func "Source_Language"] == "SDL" } {
				set funcName [string tolower [Object::getAttribute $func "label"] ]
				if { [file exists "$ivDirname/$funcName/${funcName}.pr" ] } {
					lappend SDLFiles "$ivDirname/$funcName/${funcName}.pr"
				}
			}
		}
		
		if { [llength $SDLFiles] > 0 } {
			File::concat $SDLFiles "[LogManager::getLogDirectory]/concatenatedSDL.pr"
			set originalPath [pwd]
			cd [LogManager::getLogDirectory]
			#~ replace -bt by -b to remove debug traces
			set error [catch {exec $::binPath/sdlrev -tbp concatenatedSDL.pr } errorMessage]
			if { $error != 0 } {
				set msg "Error in parsing concatenated SDL file [LogManager::getLogDirectory]/concatenatedSDL.pr:\n"
				foreach errorLine [split $errorMessage "\n"] {
					append msg "> [string trim $errorLine]\n"
				}
				return [list -1 [list $msg ] ]
			} 
			
			#~ remove following 3 lines to remove debug traces
			set chan [ open "[LogManager::getLogDirectory]/concatenatedSDL.pro" {WRONLY TRUNC CREAT}] 
			puts $chan "$errorMessage"
			close $chan
			
			file copy -force "[LogManager::getLogDirectory]/Predicates.sbp" "[LogManager::getLogDirectory]/concatenatedSDL.sbp"
			file delete -force "Predicates.sbp"
			cd $originalPath
		}
		
		AADLInspectorTools::getFactsFromAADL $ivFilename "${ivFactsFile}_"
		if { [llength $SDLFiles] > 0 } {
			File::concat [list "${ivFactsFile}_" "[LogManager::getLogDirectory]/concatenatedSDL.sbp" ] $ivFactsFile "binary"
		} else {
			file copy -force "${ivFactsFile}_" $ivFactsFile
		}
		AADLInspectorTools::generateXMLFromFacts $ivFactsFile "$::commonPluginPath/taste2bip.sbp" $ivBipFile
		
		if { [auto_execok gedit] == "" } {
			set errNumb -1
			set msg [list "Error in execution of \"gedit ${ivBipFile}: unknown file gedit" ]
		} else {
			set errNumb [catch { exec -ignorestderr {*}[auto_execok gedit] $ivBipFile } ]
			if { $errNumb == 0 } {
			   set msg "Everything went fine!"
			} else {
			   set msg "Some errors were reported - check the console"
			}
		}
		
		return [list $errNumb $msg]
    } 
}
