#include "TasteExample.hpp"

int get_size(const queueInt q){
  return q.size();
}

void push_back(queueInt &q, int &val, const int &size){
  if (get_size(q) < size){
    q.push(val);
    std::cout<<"Queuing: "<<val <<std::endl;
  }
  else{
  	std::cout<<"Queue full! Element "<<val<<" lost"<<std::endl;
  }
}

int pop_front(queueInt &q){
  int res = q.front();
  q.pop();
  std::cout<<"Popping: "<<res <<std::endl;
  return(res);
}

std::ostream &operator<<(std::ostream &os, queueInt q){
  queueInt tmp(q); 
  while (!tmp.empty()) {
    os << tmp.front() << " ";
    tmp.pop();
  }
  return os;
}
