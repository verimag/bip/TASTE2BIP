#ifndef TE_HPP
#define TE_HPP

#include <iostream>
#include <queue>

typedef std::queue<int> queueInt ;


int get_size(const queueInt q);
void push_back(queueInt &q, int &val, const int &size);
int pop_front(queueInt &q);
std::ostream &operator<<(std::ostream &os, queueInt q);

#endif
